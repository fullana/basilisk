#define JVIEW 1
#define ADAPT 1

#if JVIEW
#include "display.h"
#endif

#include "navier-stokes/centered.h"
#include "contact.h"
#include "vof.h"
#include "tension.h"

#define DDB 1

scalar f[], * interfaces = {f};

vector h[];
double theta0 = 30;

#if DDB == 1
double lambda = 0.1;
double lambda2 = 0.1;
u.t[bottom] = DDBb(0.,lambda,lambda2);
u.n[bottom]  = dirichlet(0);
#endif

h.t[bottom] = contact_angle (theta0*pi/180.);



int main()
{
  size (2);

  const face vector muc[] = {.1,.1};
  mu = muc;

  f.height = h;

  f.sigma = 1.;

  #if DDB == 1
  lmbda = lambda;
  lmbda2 = lambda2;	
  run();
  #else
  run();
  #endif
}

event init (t = 0)
{
  fraction (f, - (sq(x) + sq(y) - sq(0.5)));
}

#if ADAPT == 1
event adapt (i++){
    adapt_wavelet ({f,u}, (double[]){0.00001,0.001,0.001,0.001},  8);
}
#endif

event end (t = 10)
{
  output_facets (f, stdout);
  
  scalar kappa[];
  curvature (f, kappa);
  stats s = statsf (kappa);
  double R = s.volume/s.sum, V = 2.*statsf(f).sum;
  fprintf (stderr, "%d %g %.5g %.3g\n", N, theta0, R/sqrt(V/pi), s.stddev);
}