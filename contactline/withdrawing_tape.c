#define JVIEW 1
#define SLIP 0
#define GNBC 1

#if JVIEW
#include "display.h"
#endif
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "contact.h"

#define rho_1 1.
#define rho_2 0.2

#define g -1.25

#define sig 1.
#define V_s sqrt(Ca)
#define mu_1 (V_s)
#define mu_2 (V_s)

#define l_c 1.0
#define L (10*l_c)
#define h_0 (3.5*l_c)

#define Re (rho_1*V_s*l_c/mu_1)

#define TCHAR (l_c/V_s)
#define TEND (8.*TCHAR)

#define u_err (2e-3)
#define f_err 0.

int LEVEL = 9;
double Ca = 0.03, eps = 0.01;

#define GRID (L/pow(2,LEVEL))

vector h[];

#if GNBC
double bell(double y, double eps){
	return ((1. - pow(tanh(y/eps), 2))/eps);
}
double Peskin(double y){
	if (fabs(y) < 2.*GRID){
		return (1./(4.*GRID))*(1 + cos((pi*y)/(2.*GRID)));
	}
	return 0.;
}
double Young_stress(double y, double eps, double theta, double theta_eq){
	return bell(y, eps)*(cos(theta_eq) - cos(theta));
}
scalar gnbc[];
double dynamic_angle = 70.*pi/180., angle_eq = 70.*pi/180.;
h.t[left] = contact_angle(dynamic_angle);
#if SLIP
u.t[left] = robin(gnbc[], eps);
#else
u.t[left] = dirichlet(gnbc[]);
#endif
#else
double angle_eq = 90.*pi/180.;
h.t[left] = contact_angle(angle_eq);
#if SLIP
u.t[left] = robin(V_s, eps);
#else
u.t[left] = dirichlet(V_s);
#endif
#endif

u.n[bottom]  = dirichlet(0.);
u.t[bottom] = dirichlet(0.);

u.n[right]  = neumann(0.);
u.t[right] = neumann(0.);

u.n[top]  = neumann(0.);
u.t[top] = neumann(0.);

u.n[left]  = dirichlet(0.);

int main ()
{
  origin(0., 0.);
  init_grid (128);

  f.sigma = sig;
  f.height = h;  
  rho1 = rho_1; rho2 = rho_2;
  mu1 = mu_1; mu2 = mu_2;

	#if !JVIEW
	for (LEVEL = 7; LEVEL <= 9; LEVEL += 1){
		for (Ca = 0.01; Ca <= 0.06; Ca += 0.01){
			for (angle_eq = 70.*pi/180.; angle_eq <= 111.*pi/180.; angle_eq += 20.*pi/180.){
				lmbda = eps;
				size (L);
				run();
			}
		}
	}
	#else 
	lmbda = eps;
  	size (L); 
	run();
	#endif
}

event init (t = 0) {
	fraction (f, -(y-h_0));
	double delta = L/pow(2,LEVEL);
	fprintf(ferr, "Re = %g  Ca = %g  GNBC = %d  SLIP = %d  LEVEL = %d\n", Re, Ca, GNBC, SLIP, LEVEL);
	fprintf(ferr, "Delta = %g  eps = %g  eps/Delta = %g  angle_eq = %lf\n", delta, eps, eps/delta, 180.*angle_eq/pi);
	fprintf(ferr, "--------------------------------------------------\n");
	boundary ({f});
}

event acceleration(i++){
	face vector av = a;
	foreach_face(y)
    av.y[] += g;
}

event adapt(i++){
	adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err},  LEVEL);
}

event extract (i++){
	char name[80];
	#if JVIEW
	sprintf (name, "JVIEW_NOGNBC");
	#else
	sprintf (name, "GNBC%d_SLIP%d_Ca%g_LEVEL%d_eps%g_angle%g", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi);
	#endif
	static FILE * fp = fopen(name, "w");
	double angle_extracted = 0., angle_above = 0., angle_extrapolated = 0., position = 0., maxYS = 0.;
	double curv = 0., Ca_loc = 0.;
	scalar kappa[];

	curvature (f,kappa);

	foreach(reduction(max:angle_extrapolated) reduction(max:angle_above)){
		if ((x <= (2.*Delta)) & (x > (Delta)) & (h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
				coord n = {0};
				n = interface_normal (point,f);
				angle_above = atan2(n.y, n.x);
				double tmp = pow(((h.y[1,0] - h.y[])/1.0), 2);
				angle_extrapolated = angle_above + 1.5*Delta*kappa[]*sqrt(1 + tmp)/sin(angle_above);
		}
	}

	foreach_boundary(left reduction(max:position) reduction(max:angle_extracted) reduction(max:curv) reduction(max:Ca_loc)){
		if ((h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			position = y + Delta*height(h.y[]);
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.y, n.x);
			curv = kappa[];
			Ca_loc = mu_1*u.y[]/sig;
		}
	}

	#if GNBC
	foreach_boundary(left reduction(max:maxYS)){
		double relative_position = y - position;
		double tmp = 0.;
		if (angle_extracted > (angle_eq + 2.*pi/180.))
			tmp = eps*(sig/mu_1)*Peskin(relative_position)*(cos(angle_eq + 2.*pi/180.) - cos(angle_extracted));
		if (angle_extracted < (angle_eq - 2.*pi/180.))
			tmp = -eps*(sig/mu_1)*Peskin(relative_position)*(cos(angle_eq - 2.*pi/180.) - cos(angle_extracted));
		// gnbc[] = V_s;
		// gnbc[] = V_s + (sig/mu_1)*Young_stress(relative_position, eps, angle_extracted, angle_eq);
		// double tmp = eps*(sig/mu_1)*Peskin(relative_position)*(cos(angle_eq) - cos(angle_extracted)); //eps*(sig/mu_1)*Young_stress(relative_position, eps, angle_extracted, angle_eq);
		// gnbc[] = V_s + max(0., tmp);
		gnbc[] = V_s + tmp;
		// gnbc[] = V_s + eps*(sig/mu_1)*Peskin(relative_position)*(cos(angle_eq) - cos(angle_extracted));
		// gnbc[] = V_s + V_s*Young_stress(relative_position, eps, angle_extracted, angle_eq);
		if (fabs(maxYS) < fabs(tmp))
			maxYS = tmp;
	}
	dynamic_angle = angle_extrapolated;
	#endif

	// Stops if the interface exits the domain
	foreach_boundary(top){
		if (f[] != 0)
			return 1.;
	}

	fflush(fp);
	fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf %lf\n", t/TCHAR, (position-h_0)/l_c, 180.*angle_above/pi, 180.*angle_extrapolated/pi, 180.*angle_extracted/pi, Ca_loc, curv, maxYS);
	
}

event final (t = TEND){
	char name[80], name2[80], name3[80], name4[80];
	#if JVIEW
	sprintf (name, "GNBC%d_SLIP%d_pressure_JVIEW_%d", GNBC, SLIP, tid());
	sprintf (name2, "GNBC%d_SLIP%d_curvature_JVIEW_%d", GNBC, SLIP, tid());
	sprintf (name3, "GNBC%d_SLIP%d_facets_JVIEW_%d", GNBC, SLIP, tid());
	sprintf (name4, "GNBC%d_SLIP%d_dudy_JVIEW_%d", GNBC, SLIP, tid());
	#else
	sprintf (name, "GNBC%d_SLIP%d_pressure_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	sprintf (name2, "GNBC%d_SLIP%d_curvature_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	sprintf (name3, "GNBC%d_SLIP%d_facets_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	sprintf (name4, "GNBC%d_SLIP%d_dudy_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	#endif
	FILE * fp = fopen(name, "w");
	FILE * fp2 = fopen(name2, "w");
	FILE * fp3 = fopen(name3, "w");
	FILE * fp4 = fopen(name4, "w");
	scalar kappa[];
	double position = 0.;

	curvature (f,kappa);

	foreach_boundary(left reduction(max:position)){
		if ((h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			position = y + Delta*height(h.y[]);
		}
		double dudy = (u.y[1,0] - u.y[])/Delta;
		fprintf(fp4,"%lf %lf\n", y, dudy);
	}

	foreach(){
		fprintf(fp,"%lf %lf %lf\n", x, y, p[]);
		if ((h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			double ycl = y + Delta*height(h.y[]);
			double r = sqrt(pow(x,2) + pow((ycl - position), 2));
			coord n = {0};
			n = interface_normal (point,f);
			double theta = atan2(n.y, n.x);
			fprintf(fp2,"%lf %lf %lf\n", r, kappa[], 180.*theta/pi);
		}
	}
	foreach_boundary(left){
			fprintf(stdout,"%lf %lf\n", y, u.y[]);
		}
	output_facets (f, fp3);

	fclose(fp);
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
}