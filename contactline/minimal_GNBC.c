#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"
//#include "navier-stokes/perfs.h"

#define _height_SI 100.e-9
#define _widthliq_SI 150.e-9
#define muliq_SI 8.77e-4
#define mugas_SI muliq_SI/1000.
#define SIGMA_SI 5.8e-2 
#define rholiq_SI 986.
#define rhogas_SI rholiq_SI/1000.
#define u_plates_SI 100./12.   

#define MU_unit (rholiq_SI*u_plates_SI*_height_SI)
#define SIGMA_unit (rholiq_SI*_height_SI*u_plates_SI*u_plates_SI)
#define _height 1.
#define muliq (muliq_SI/MU_unit) // = 1/Re_l
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit) // =1/We_l 
#define rholiq 1.
#define rhogas rhogas_SI/rholiq_SI
#define u_plates 1.

#define TEND 1.
#define _width 5*_height
#define _widthliq _widthliq_SI/_height_SI
#define TOUT (TEND/10.)
#define TCHAR 1.
#define sliplength 1e-2
#define brisbane 1./(pow(2.,maxlevel))

int maxlevel = 6;
int count= 0;
double uemax = 0.01, rec = 80.85, adv = 95.;
double middle;
double pos_11, pos_12, pos_21, pos_22;
double ystress_11, ystress_12, ystress_21, ystress_22 = 0.;
double opp, adj, argtan = 0.;

scalar ftemp[], fd[];
vector h[];

u.n[top]  = dirichlet(0);
u.t[top] = robin(u_plates/2,sliplength);

u.n[bottom]  = dirichlet(0);
u.t[bottom] = robin(-u_plates/2,sliplength);

//h.t[top] = contact_angle ((x > -middle) ? rec*pi/180. : adv*pi/180.);
//h.t[bottom] = contact_angle ((x < middle) ? rec*pi/180. : adv*pi/180.);

int main (int argc, char * argv[])
{
  init_grid (1<<maxlevel);
  origin(-0.5,-0.5); 
  size (_height);

  refine (level < maxlevel);
  DT = 1e-3;
  periodic (right); 
  f.sigma = SIGMA;
  f.height = h;
    
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;

  run();
}

event init (t = 0) {
  if (!restore (file = "restart")) { 
	fraction (ftemp, (x-0.25)*(x+0.25));
	foreach(){
		f[] = 1. - ftemp[];
	}
  }
  boundary ({f});
  dump("dump-initial");
}


event adapt (i++) {
  adapt_wavelet ({f,u}, (double[]){0.001,uemax,uemax,uemax},  maxlevel);
}

event imposeangle (i = 20; i++){
	int yp = 1;
	double leftbot1x = 0.;
	double leftbot1y = 0.;
	double leftbot2x = 0.;
	double leftbot2y = 0.;
	double ux1, ux2, uxloc, gammap;
	double testf1, testf2;
	vector h3[];
    heights (f, h3);
    foreach() {
		if ((y > (-0.5 + brisbane)) & ((x + Delta*height(h3.x[])) < leftbot1x) & ((x + Delta*height(h3.x[]))>-0.5)){
			leftbot1x = x + Delta*height(h3.x[]);
		    leftbot1y = y;
			//point = locate(x + brisbane,y);
			//ux1 = u.x[];
			//testf1[] = f[];
			//point = locate(x,y + sliplength);
			//ux2 = u.x[];
		}
		if ((y > (-0.5 + 2*brisbane)) & ((x + Delta*height(h3.x[])) < leftbot2x ) & ((x + Delta*height(h3.x[]))>-0.5 ) ){
			leftbot2x = x + Delta*height(h3.x[]);
			leftbot2y = y;
			//point = locate(x,y + 3.*brisbane);
			//ux2 = u.x[];
			//testf2[] = f[];
		}
	}
	//foreach(){
		//fd[] = 0.;
	//}
	
	opp = fabs(leftbot1x - leftbot2x) ;
	adj = fabs(leftbot1y - leftbot2y) ;

	ystress_22 = (cos(pi/2. - atan(opp/adj))-cos(rec*pi/180.));

	middle = leftbot1x + 0.25;
	//gammap = (sliplength*fabs(ux1 - ux2))/(ystress_22 * (2+yp)*brisbane);
	
	foreach(){
		point = locate(leftbot1x,leftbot1y);
		//while((f[0,yp] > 1e-06)  (yp < 8)){
			//yp += 1;
			//}
		//fd[0,-1] = 1.;
		//fd[0,yp] = 1.;
		ux1 = u.x[0,-1];
		ux2 = u.x[0,yp];
		uxloc = u.x[];
		//testf1 = f[0,-1];
		//testf2 = f[0,yp];
		gammap = (u.x[0,-1] - (((2.*Delta*(0.5))/(Delta+2.*(sliplength)))+ ((2.*(sliplength)-Delta)/(2.*(sliplength)+Delta))*u.x[0,0]))*(1./ystress_22);
	}
	/*
	foreach(){
		point = locate(leftbot1x + brisbane,leftbot1y);
			if (f[] > 1e-6){
				ux1 = u.x[];
				testf1 = f[];
			}
	}
	foreach(){
		point = locate(leftbot1x + brisbane,leftbot1y + sliplength/2.);
			if (f[] < 1.- 1e-6){
				ux2 = u.x[];
				testf2 = f[];
			}	
	}*/
	/*
	opp = fabs(leftbot1x - leftbot2x) ;
	adj = fabs(leftbot1y - leftbot2y) ;

	ystress_22 = (cos(pi/2. - atan(opp/adj))-cos(rec*pi/180.));
	/*
	foreach(){
		if ((x < (leftbot1x + Delta)) & (x > (leftbot1x - Delta)) & (y > (-0.5)) & (y < (-0.5 + 2*Delta)))
			u.x[] = ystress_22;
	}
	middle = leftbot1x + 0.25;
	gammap = (sliplength*fabs(ux1 - ux2))/(ystress_22 * (2+yp)*brisbane);
	*/
	
	//fprintf(stderr,"%d %lf %lf %lf %lf %lf %lf %lf\n",i, t, 90.-atan(opp/adj)*180./pi, ux1, ux2,fabs(ux1 - ux2),ystress_22, gammap);
	fprintf(stderr,"%d %lf %lf %lf %lf %lf\n",i, t, 90.-atan(opp/adj)*180./pi, ux1, uxloc, ux2);
	fprintf(stdout,"%d %lf %lf %lf %lf %lf\n",i, t, 90.-atan(opp/adj)*180./pi, fabs(ux1 - ux2),ystress_22, gammap);
	
}

event snapshot (t = TEND){
  char name[80];
  sprintf (name, "snap-%g", t);
  scalar pid[];
  scalar omega[];
  vorticity (u, omega);
  foreach(){
      pid[] = fmod(pid()*(npe() + 37), npe());
      omega[] *= TCHAR;
    }
  boundary ({pid,omega});
  dump (name);
}

event movie (t += TEND/120.) {
  clear();
  box();
  squares ("u.x");
  draw_vof("f",lc = {1.,0.,1.} , lw = 2);
  save ("movie.mp4"); 
}