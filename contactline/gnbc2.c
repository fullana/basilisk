/**
Droplet in sheared flow with velocity slip. 
 // */
#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"

/**
We define parameters in the SI (kg m s) unit system. 
*/

#define _height_SI 50.e-9    
#define _width_SI 250.e-9   //running on 5 processors will bring the height
                                // down to 50nm   
#define _widthliq_SI 75.e-9
#define muliq_SI 8.77e-4
#define mugas_SI muliq_SI/1000.
#define SIGMA_SI 5.78e-2 
#define rholiq_SI 986.
#define rhogas_SI rholiq_SI/1000.
#define u_plates_SI 1.6667   //using dimensionless number for u_plates = 1.



/**
No we convert parameters in the unit system of the simulation:
L = _height  T = _height/U  M = rho * _height^3  MU = M L-1 T-1 = rho * _height * U
SIGMA = M T^-2 = rho _height U^2 
*/

#define _width 1.
#define _height (_width/npe())    
#define MU_unit (rholiq_SI*u_plates_SI*_height_SI)
#define SIGMA_unit (rholiq_SI*_height_SI*u_plates_SI*u_plates_SI)
#define T_unit ((_height_SI*u_plates)/(_height*u_plates_SI))
#define L_unit (_height_SI/_height)

#define muliq (muliq_SI/MU_unit) // = 1/Re_l
#define mugas (mugas_SI/MU_unit) 
// use uppercase otherwise the preprocessor will also overwrite the sigma in f.sigma:
#define SIGMA (SIGMA_SI/SIGMA_unit) // =1/We_l 
#define rholiq 1.
#define rhogas rhogas_SI/rholiq_SI
#define u_plates 1.
#define _widthliq (_widthliq_SI/L_unit) 

/**  The following parameters are defined directly in terms of the dimensionless parameters */

// #define TEND 0.5
#define TEND_SI 25.e-9
#define TOUT_SI 0.4e-9
#define TEND (TEND_SI/T_unit)
#define TOUT (TOUT_SI/T_unit)
#define TCHAR 1.

/**
The default maximum level of refinement is 6 and the error threshold
on velocity is 0.1. */

int maxlevel = 6;
double uemax = 0.1;
double lambda = 8e-2;   //defining slip length
double rec = 94., adv = 103.;
double minx = -0.5, maxx = 0.;
double clb1, clt1, clb2, clt2;
double t_b, t_t, tmp = 0.00625;
double sm = SIGMA/muliq;
vector h2[];

u.n[top]  = dirichlet(0);
//u.t[top]  = robin((u_plates), lambda);
u.n[bottom]  = dirichlet(0);
//u.t[bottom]  = robin(-(u_plates), lambda);
h2.t[bottom] = contact_angle(x<0. ? (180.-rec)*pi/180. : (180.-adv)*pi/180.);
h2.t[top] = contact_angle(x>0. ? (180.-adv)*pi/180. : (180.-rec)*pi/180.);

//u.t[bottom] = (((x < (clb1 + tmp)) & (x > (clb1 - tmp))) ? robin((-u_plates + phi(x,tmp,clb1)*ygs(-sm,rec,t_b)),lambda) : ((x < (-clt1 + tmp)) & (x > (-clt1 - tmp))) ? robin((-u_plates + phi(x,tmp,-clt1)*ygs(-sm,adv,t_t)),lambda) : robin(-u_plates,lambda));

//u.t[top] = (((x < (clt1 + tmp)) & (x > (clt1 - tmp))) ? robin((u_plates + phi(x,tmp,clt1)*ygs(sm,adv,t_t)),lambda) : ((x < (-clb1 + tmp)) & (x > (-clb1 - tmp))) ? robin((u_plates + phi(x,tmp,-clb1)*ygs(sm,rec,t_b)),lambda) : robin(u_plates,lambda));

u.t[bottom] = robin(-u_plates,lambda);
u.t[top] = robin(u_plates,lambda);
#if dimension > 2
printf("error")
exit(5)
#endif
double ygs(double a, double theta_e, double theta_d){
		double b = (a)*(cos((theta_d)*pi/180.)-cos((theta_e)*pi/180.));
			return b;
}

double phi(double a, double b, double pos){
		double c = pow(((1.+cos((pi*(a - (pos)))/b))/2),2);
			return c;
}
/**
The program can take two optional command-line arguments: the maximum
level and the error threshold on velocity. */

int main (int argc, char * argv[])
{
  if (argc > 1)
    maxlevel = atoi (argv[1]);
  if (argc > 2)
    uemax = atof (argv[2]);

  /**
  The initial domain is discretised with $512^2$ grid points. We set the domain size. */
  
  init_grid (256);
  // init_grid (64);   //for testing purposes
  origin(-0.5,-0.5); 
  size (_width);
  // dimensions (nx = 4);    // simulating a long channel when running on 4 processors
  // refine ( level < maxlevel);

  /**  We use periodic condition on the left/right */
  lambdaxu = lambda;
  periodic (right); 
  /**
  We set the density and viscosity of each phase as well as the
  surface tension coefficient and start the simulation. */
  f.sigma = SIGMA;
  f.height = h2;  
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;

  run();
}

/**
## Initial conditions */

event init (t = 0) {
  if (!restore (file = "restart")) {

    // refine (y < (_height/npe() - _height/2.) && level < 8);
    /**
       We initialise the auxilliary volume fraction field as rectangle of extent L_x=_width/2 */

    fraction (f, (x-_widthliq/2.)*(x+_widthliq/2.));

    /* Output dimensionless numbers with a consistency check */

    //fprintf(ferr,"#Re \t%e\t%e\n",1./muliq,u_plates_SI*_height_SI*rholiq_SI/muliq_SI);
    //fprintf(ferr,"#Ca \t%e\t%e\n",2*muliq/SIGMA,muliq_SI*u_plates_SI/SIGMA_SI);
    //fprintf(ferr,"#La \t%e\n",SIGMA*rholiq*_height/sq(muliq));
    //fprintf(ferr,"#We \t%e\n",rhogas*u_plates*u_plates*_height/SIGMA);
    //fprintf(ferr,"#Processors: \t%d\n",npe());
  }
  boundary ({f});

  dump("dump-initial");
}

event testheight (i++){

vector h2[];
scalar pid[], ff[], c[];
   foreach() {
    pid[] = fmod(pid()*(npe() + 37), npe());
    ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
  }
boundary ({pid,ff});
heights(ff,h2);
	foreach(reduction(max:maxx) reduction(min:minx)) {
		if ((x > -0.5) & (x < 0.)){
			if (y < (-0.5 + Delta)){
			if (h2.x[] != nodata){
				clb1 = x + Delta*height(h2.x[]);
			}
			}
			if (y > (-0.3 - Delta)){
			if (h2.x[] != nodata){
				clt1 = x + Delta*height(h2.x[]);
			}
			}
			if ((y < (-0.5 + 2.*Delta)) & (y > (-0.5 + Delta))){
			if (h2.x[] != nodata){
				clb2 = x + Delta*height(h2.x[]);
			}
			}
			if ((y > (-0.3 - 2.*Delta)) & (y < (-0.3 - Delta))){
			if (h2.x[] != nodata){
				clt2 = x + Delta*height(h2.x[]);
				//tmp = Delta; 
			}
			}	
		}
	}
t_b = 180. - 180.*atan(tmp/(fabs(clb1-clb2)+1e-15))/pi;
t_t = 180. - 180.*atan(tmp/(fabs(clt1-clt2)+1e-15))/pi;
fprintf(stdout, "%g %g %g %g %g %g\n", t, t_b, t_t, fabs(clb1-clt1), ygs(1.,rec,t_b),ygs(1.,adv,t_t));
}
event snapshot (t += TOUT) {

	  char name[100];
	    sprintf (name, "facet_t-%05.0fps_%d.dat",t*T_unit*1.e+12,pid());

	      FILE * fp = fopen (name, "w");
	        scalar pid[], ff[];
		  foreach() {

			      pid[] = fmod(pid()*(npe() + 37), npe());
			          ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
				    }
		    boundary ({pid,ff});

		      output_facets (ff, fp);

		        fclose (fp);

}
event velo (t = TEND) {
  char name[100];
  sprintf (name, "velo%d",pid());
  FILE * fp4 = fopen (name, "w");
  scalar pid[], ff[];
  foreach() {
    pid[] = fmod(pid()*(npe() + 37), npe());
    ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
  }
  boundary ({pid,ff});
  foreach_boundary(bottom){
	fprintf (fp4, "%g %g \n", x, u.x[]);
  }
}
