#include "view.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"

#define MU 0.25
#define SIGMA 7.32
#define radius0 0.5
#define lambda 1.0/256.0
#define theta_s 60.
vector h[];
double dtmp;


//double theta_d = 60.;
int j;

u.n[bottom]  = dirichlet(0);
//u.t[bottom]  = dirichlet(0);
u.t[bottom]  = robin(0,0.0);
/*
u.n[left]  = dirichlet(0);
u.t[left]  = dirichlet(0);
*/
u.n[top]  = dirichlet(0);
u.t[top]  = dirichlet(0);

u.n[right]  = dirichlet(0);
u.t[right]  = dirichlet(0);

h.t[bottom] = contact_angle ((180.-theta_s)*pi/180);

int main() {
  	N = 64;
      L0 = 1.;
      origin (-L0/2, -L0/2.);
  
      DT = 1e-5;
	  
      f.sigma = SIGMA;
      f.height = h;
    
      lmbda = lambda;	
      rho1 = 1.; rho2 = 1.;
      mu1 = MU; mu2 = MU;  
    	run();
  
}

event init (t = 0) {
  if (!restore (file = "restart")) { 
	fraction (f, sq((x+0.5))+sq((y+0.5))-sq((0.5)););
 	}
	char name[80];
	sprintf (name, "init_snap-%d", N);
    dump(name);
  output_facets (f, stderr)
}

event impose (i++){

	char name[80];
	sprintf (name, "log_stat1-%d", N);
	static FILE * fp = fopen(name, "w");
	double radius = 0.0;
	double Ca = 0.0;
	foreach_boundary(bottom){
		if ((x > 0.) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			radius = x + Delta*height(h.x[]);
			Ca = MU*u.x[]/SIGMA;;
		}
	}
	//theta_d = acos(cos(theta_s)+5.63*Ca*log(0.02/lambda));
	//theta_d = theta_s;
	fprintf(fp,"%lf %lf %lf\n",t, fabs(0.5+radius), Ca);
    fflush(fp);
	
}
event snapshot (t = 0){
	double tmp, tmp2;
	char name[80];
	char name2[80];
	sprintf (name2, "dudy_stat1-%d", N);
	static FILE * fp2 = fopen(name2, "w");
	foreach_boundary(bottom){
		point = locate(x,y);
		tmp = u.x[];
		point = locate(x,y+Delta);
		tmp2 = fabs(u.x[]-tmp);
		fprintf(fp2,"%lf %lf %lf\n",x, tmp2, tmp);
		//fflush(fp2);	
	}
	//fclose(fp2);
	sprintf (name, "stat1-%d", N);
    dump(name);
}
