//#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "contact.h"
#include "vof.h"
#include "tension.h"
#include "display.h"

#define RADIUS (0.25)
#define DIAMETER (2.*RADIUS)
#define MU sqrt(DIAMETER/LAPLACE)
#define TMAX 100.*(sq(DIAMETER)/MU)

scalar f[], * interfaces = {f};
double LAPLACE = 200.;
double Dshift;
double Ct;
int LEVEL = 5;
double Ntemp;
int Ntempi;
scalar omega[];

vector h[];
double theta0 = 90.;
h.t[bottom] = contact_angle (theta0*pi/180.);

int main()
{ 
// for (LEVEL = 5; LEVEL <= 7; LEVEL += 1){

  	// for (LAPLACE = 200.; LAPLACE <= 1700.; LAPLACE*=2.){

		size (1);
		origin (-0.5, -0.5);
		N = pow(2.,LEVEL);
		//N = Ntempi;
		// periodic (right);
		// periodic (top);
		// stokes = true;
		const face vector muc[] = {MU,MU};
		mu = muc;
	
		f.height = h;
	  
		f.sigma = 1.;
		// for (Ct = 0.; Ct <= 1.; Ct += 1.){
			// for (Dshift = 0.; Dshift <= (1./N)/2.; Dshift += (1./N)/4.){
				Ct = 1.;
				Dshift = 0.;
				run();
			// }
		// }
  		
	// }
// }
}

/**
The initial drop is a quarter of a circle. */

event init (t = 0)
{
  fraction (f, -(sq(x + Dshift) + sq(y + (Ct/2.)) - sq(RADIUS)));
  boundary ({f});
  vorticity(u,omega);
  }
  

// event adapt (i++) {
//   adapt_wavelet ({f,u}, (double[]){1e-6,1e-3,1e-3,1e-3}, LEVEL);
// }

event logfile (i++; t <= TMAX)
{
	// char name[80];
	// sprintf (name, "CONTACT%g_LAPLACE%g_SHIFT%g_LEVEL%d",Ct,LAPLACE,(Dshift/(1./N)),LEVEL);
	// static FILE * fp = fopen(name, "w");
	// scalar un[];
	// foreach()
	//     un[] = norm(u);
  	// fprintf (fp, "%g %g\n", MU*t/sq(DIAMETER), normf(un).max*MU);
	// fflush(fp);
	vorticity(u,omega);
}


// event end ()
// {
//   //output_facets (f, stdout);
//   char name3[80];
//   sprintf (name3, "log-%g-%g-%d-%g",Ct,LAPLACE,LEVEL,(Dshift/(2./N)));
//   static FILE * fp2 = fopen(name3, "w");
//   scalar kappa[];
//   curvature (f, kappa);
//   stats s = statsf (kappa);
//   double R = s.volume/s.sum, V = 2.*statsf(f).sum;
//   fprintf (fp2, "%d %g %.5g %.3g\n", LEVEL, theta0, R/sqrt(V/pi), s.stddev);
//   fclose(fp2);
// }

/**
We compare $R/R_0$ to the analytical expression, with $R_0=\sqrt{V/\pi}$.

~~~gnuplot
reset
set xlabel 'Contact angle (degrees)'
set ylabel 'R/R_0'
set arrow from 15,1 to 165,1 nohead dt 2
set xtics 15,15,165
plot 1./sqrt(x/180. - sin(x*pi/180.)*cos(x*pi/180.)/pi) t 'analytical', \
  'log' u 2:3 pt 7 t 'numerical'
~~~

## See also

* [Similar test with
   Gerris](http://gerris.dalembert.upmc.fr/gerris/tests/tests/sessile.html)
*/
