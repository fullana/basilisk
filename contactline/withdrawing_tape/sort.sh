sort curvature_Ca0.1_LEVEL7_eps0.15625_angle80_* > curvature_Ca0.1_LEVEL7_eps0.15625_angle80_all
sort curvature_Ca0.1_LEVEL7_eps0.3125_angle80_* > curvature_Ca0.1_LEVEL7_eps0.3125_angle80_all
sort curvature_Ca0.1_LEVEL7_eps0.625_angle80_* > curvature_Ca0.1_LEVEL7_eps0.625_angle80_all
sort curvature_Ca0.1_LEVEL8_eps0.15625_angle80_* > curvature_Ca0.1_LEVEL8_eps0.15625_angle80_all
sort curvature_Ca0.1_LEVEL8_eps0.3125_angle80_* > curvature_Ca0.1_LEVEL8_eps0.3125_angle80_all
sort curvature_Ca0.1_LEVEL8_eps0.625_angle80_* > curvature_Ca0.1_LEVEL8_eps0.625_angle80_all
sort curvature_Ca0.1_LEVEL9_eps0.15625_angle80_* > curvature_Ca0.1_LEVEL9_eps0.15625_angle80_all
sort curvature_Ca0.1_LEVEL9_eps0.3125_angle80_* > curvature_Ca0.1_LEVEL9_eps0.3125_angle80_all
sort curvature_Ca0.1_LEVEL9_eps0.625_angle80_* > curvature_Ca0.1_LEVEL9_eps0.625_angle80_all

sort SLIP1_curvature_Ca0.1_LEVEL8_eps0.078125_angle80_* > SLIP1_curvature_Ca0.1_LEVEL8_eps0.078125_angle80_all
sort SLIP1_curvature_Ca0.1_LEVEL9_eps0.078125_angle80_* > SLIP1_curvature_Ca0.1_LEVEL9_eps0.078125_angle80_all
sort SLIP1_curvature_Ca0.1_LEVEL10_eps0.078125_angle80_* > SLIP1_curvature_Ca0.1_LEVEL10_eps0.078125_angle80_all
sort SLIP1_curvature_Ca0.1_LEVEL10_eps0.15625_angle80_* > SLIP1_curvature_Ca0.1_LEVEL10_eps0.15625_angle80_all
sort SLIP1_curvature_Ca0.1_LEVEL10_eps0.3125_angle80_* > SLIP1_curvature_Ca0.1_LEVEL10_eps0.3125_angle80_all

sort curvature_Ca0.1_LEVEL7_eps0.15625_angle90_* > curvature_Ca0.1_LEVEL7_eps0.15625_angle90_all
sort curvature_Ca0.1_LEVEL7_eps0.3125_angle90_* > curvature_Ca0.1_LEVEL7_eps0.3125_angle90_all
sort curvature_Ca0.1_LEVEL7_eps0.625_angle90_* > curvature_Ca0.1_LEVEL7_eps0.625_angle90_all
sort curvature_Ca0.1_LEVEL8_eps0.15625_angle90_* > curvature_Ca0.1_LEVEL8_eps0.15625_angle90_all
sort curvature_Ca0.1_LEVEL8_eps0.3125_angle90_* > curvature_Ca0.1_LEVEL8_eps0.3125_angle90_all
sort curvature_Ca0.1_LEVEL8_eps0.625_angle90_* > curvature_Ca0.1_LEVEL8_eps0.625_angle90_all
sort curvature_Ca0.1_LEVEL9_eps0.15625_angle90_* > curvature_Ca0.1_LEVEL9_eps0.15625_angle90_all
sort curvature_Ca0.1_LEVEL9_eps0.3125_angle90_* > curvature_Ca0.1_LEVEL9_eps0.3125_angle90_all
sort curvature_Ca0.1_LEVEL9_eps0.625_angle90_* > curvature_Ca0.1_LEVEL9_eps0.625_angle90_all

sort SLIP1_curvature_Ca0.1_LEVEL8_eps0.078125_angle90_* > SLIP1_curvature_Ca0.1_LEVEL8_eps0.078125_angle90_all
sort SLIP1_curvature_Ca0.1_LEVEL9_eps0.078125_angle90_* > SLIP1_curvature_Ca0.1_LEVEL9_eps0.078125_angle90_all
sort SLIP1_curvature_Ca0.1_LEVEL10_eps0.078125_angle90_* > SLIP1_curvature_Ca0.1_LEVEL10_eps0.078125_angle90_all
sort SLIP1_curvature_Ca0.1_LEVEL10_eps0.15625_angle90_* > SLIP1_curvature_Ca0.1_LEVEL10_eps0.15625_angle90_all
sort SLIP1_curvature_Ca0.1_LEVEL10_eps0.3125_angle90_* > SLIP1_curvature_Ca0.1_LEVEL10_eps0.3125_angle90_all

sort facets_Ca0.1_LEVEL7_eps0.15625_angle80_* > facets_Ca0.1_LEVEL7_eps0.15625_angle80_all
sort facets_Ca0.1_LEVEL7_eps0.3125_angle80_* > facets_Ca0.1_LEVEL7_eps0.3125_angle80_all
sort facets_Ca0.1_LEVEL7_eps0.625_angle80_* > facets_Ca0.1_LEVEL7_eps0.625_angle80_all
sort facets_Ca0.1_LEVEL8_eps0.15625_angle80_* > facets_Ca0.1_LEVEL8_eps0.15625_angle80_all
sort facets_Ca0.1_LEVEL8_eps0.3125_angle80_* > facets_Ca0.1_LEVEL8_eps0.3125_angle80_all
sort facets_Ca0.1_LEVEL8_eps0.625_angle80_* > facets_Ca0.1_LEVEL8_eps0.625_angle80_all
sort facets_Ca0.1_LEVEL9_eps0.15625_angle80_* > facets_Ca0.1_LEVEL9_eps0.15625_angle80_all
sort facets_Ca0.1_LEVEL9_eps0.3125_angle80_* > facets_Ca0.1_LEVEL9_eps0.3125_angle80_all
sort facets_Ca0.1_LEVEL9_eps0.625_angle80_* > facets_Ca0.1_LEVEL9_eps0.625_angle80_all

sort SLIP1_facets_Ca0.1_LEVEL8_eps0.078125_angle80_* > SLIP1_facets_Ca0.1_LEVEL8_eps0.078125_angle80_all
sort SLIP1_facets_Ca0.1_LEVEL9_eps0.078125_angle80_* > SLIP1_facets_Ca0.1_LEVEL9_eps0.078125_angle80_all
sort SLIP1_facets_Ca0.1_LEVEL10_eps0.078125_angle80_* > SLIP1_facets_Ca0.1_LEVEL10_eps0.078125_angle80_all
sort SLIP1_facets_Ca0.1_LEVEL10_eps0.15625_angle80_* > SLIP1_facets_Ca0.1_LEVEL10_eps0.15625_angle80_all
sort SLIP1_facets_Ca0.1_LEVEL10_eps0.3125_angle80_* > SLIP1_facets_Ca0.1_LEVEL10_eps0.3125_angle80_all

sort facets_Ca0.1_LEVEL7_eps0.15625_angle90_* > facets_Ca0.1_LEVEL7_eps0.15625_angle90_all
sort facets_Ca0.1_LEVEL7_eps0.3125_angle90_* > facets_Ca0.1_LEVEL7_eps0.3125_angle90_all
sort facets_Ca0.1_LEVEL7_eps0.625_angle90_* > facets_Ca0.1_LEVEL7_eps0.625_angle90_all
sort facets_Ca0.1_LEVEL8_eps0.15625_angle90_* > facets_Ca0.1_LEVEL8_eps0.15625_angle90_all
sort facets_Ca0.1_LEVEL8_eps0.3125_angle90_* > facets_Ca0.1_LEVEL8_eps0.3125_angle90_all
sort facets_Ca0.1_LEVEL8_eps0.625_angle90_* > facets_Ca0.1_LEVEL8_eps0.625_angle90_all
sort facets_Ca0.1_LEVEL9_eps0.15625_angle90_* > facets_Ca0.1_LEVEL9_eps0.15625_angle90_all
sort facets_Ca0.1_LEVEL9_eps0.3125_angle90_* > facets_Ca0.1_LEVEL9_eps0.3125_angle90_all
sort facets_Ca0.1_LEVEL9_eps0.625_angle90_* > facets_Ca0.1_LEVEL9_eps0.625_angle90_all

sort SLIP1_facets_Ca0.1_LEVEL8_eps0.078125_angle90_* > SLIP1_facets_Ca0.1_LEVEL8_eps0.078125_angle90_all
sort SLIP1_facets_Ca0.1_LEVEL9_eps0.078125_angle90_* > SLIP1_facets_Ca0.1_LEVEL9_eps0.078125_angle90_all
sort SLIP1_facets_Ca0.1_LEVEL10_eps0.078125_angle90_* > SLIP1_facets_Ca0.1_LEVEL10_eps0.078125_angle90_all
sort SLIP1_facets_Ca0.1_LEVEL10_eps0.15625_angle90_* > SLIP1_facets_Ca0.1_LEVEL10_eps0.15625_angle90_all
sort SLIP1_facets_Ca0.1_LEVEL10_eps0.3125_angle90_* > SLIP1_facets_Ca0.1_LEVEL10_eps0.3125_angle90_all

sort dudy_Ca0.1_LEVEL7_eps0.15625_angle80_* > dudy_Ca0.1_LEVEL7_eps0.15625_angle80_all
sort dudy_Ca0.1_LEVEL7_eps0.3125_angle80_* > dudy_Ca0.1_LEVEL7_eps0.3125_angle80_all
sort dudy_Ca0.1_LEVEL7_eps0.625_angle80_* > dudy_Ca0.1_LEVEL7_eps0.625_angle80_all
sort dudy_Ca0.1_LEVEL8_eps0.15625_angle80_* > dudy_Ca0.1_LEVEL8_eps0.15625_angle80_all
sort dudy_Ca0.1_LEVEL8_eps0.3125_angle80_* > dudy_Ca0.1_LEVEL8_eps0.3125_angle80_all
sort dudy_Ca0.1_LEVEL8_eps0.625_angle80_* > dudy_Ca0.1_LEVEL8_eps0.625_angle80_all
sort dudy_Ca0.1_LEVEL9_eps0.15625_angle80_* > dudy_Ca0.1_LEVEL9_eps0.15625_angle80_all
sort dudy_Ca0.1_LEVEL9_eps0.3125_angle80_* > dudy_Ca0.1_LEVEL9_eps0.3125_angle80_all
sort dudy_Ca0.1_LEVEL9_eps0.625_angle80_* > dudy_Ca0.1_LEVEL9_eps0.625_angle80_all

sort SLIP1_dudy_Ca0.1_LEVEL8_eps0.078125_angle80_* > SLIP1_dudy_Ca0.1_LEVEL8_eps0.078125_angle80_all
sort SLIP1_dudy_Ca0.1_LEVEL9_eps0.078125_angle80_* > SLIP1_dudy_Ca0.1_LEVEL9_eps0.078125_angle80_all
sort SLIP1_dudy_Ca0.1_LEVEL10_eps0.078125_angle80_* > SLIP1_dudy_Ca0.1_LEVEL10_eps0.078125_angle80_all
sort SLIP1_dudy_Ca0.1_LEVEL10_eps0.15625_angle80_* > SLIP1_dudy_Ca0.1_LEVEL10_eps0.15625_angle80_all
sort SLIP1_dudy_Ca0.1_LEVEL10_eps0.3125_angle80_* > SLIP1_dudy_Ca0.1_LEVEL10_eps0.3125_angle80_all

sort dudy_Ca0.1_LEVEL7_eps0.15625_angle90_* > dudy_Ca0.1_LEVEL7_eps0.15625_angle90_all
sort dudy_Ca0.1_LEVEL7_eps0.3125_angle90_* > dudy_Ca0.1_LEVEL7_eps0.3125_angle90_all
sort dudy_Ca0.1_LEVEL7_eps0.625_angle90_* > dudy_Ca0.1_LEVEL7_eps0.625_angle90_all
sort dudy_Ca0.1_LEVEL8_eps0.15625_angle90_* > dudy_Ca0.1_LEVEL8_eps0.15625_angle90_all
sort dudy_Ca0.1_LEVEL8_eps0.3125_angle90_* > dudy_Ca0.1_LEVEL8_eps0.3125_angle90_all
sort dudy_Ca0.1_LEVEL8_eps0.625_angle90_* > dudy_Ca0.1_LEVEL8_eps0.625_angle90_all
sort dudy_Ca0.1_LEVEL9_eps0.15625_angle90_* > dudy_Ca0.1_LEVEL9_eps0.15625_angle90_all
sort dudy_Ca0.1_LEVEL9_eps0.3125_angle90_* > dudy_Ca0.1_LEVEL9_eps0.3125_angle90_all
sort dudy_Ca0.1_LEVEL9_eps0.625_angle90_* > dudy_Ca0.1_LEVEL9_eps0.625_angle90_all

sort SLIP1_dudy_Ca0.1_LEVEL8_eps0.078125_angle90_* > SLIP1_dudy_Ca0.1_LEVEL8_eps0.078125_angle90_all
sort SLIP1_dudy_Ca0.1_LEVEL9_eps0.078125_angle90_* > SLIP1_dudy_Ca0.1_LEVEL9_eps0.078125_angle90_all
sort SLIP1_dudy_Ca0.1_LEVEL10_eps0.078125_angle90_* > SLIP1_dudy_Ca0.1_LEVEL10_eps0.078125_angle90_all
sort SLIP1_dudy_Ca0.1_LEVEL10_eps0.15625_angle90_* > SLIP1_dudy_Ca0.1_LEVEL10_eps0.15625_angle90_all
sort SLIP1_dudy_Ca0.1_LEVEL10_eps0.3125_angle90_* > SLIP1_dudy_Ca0.1_LEVEL10_eps0.3125_angle90_all