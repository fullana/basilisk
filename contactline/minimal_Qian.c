#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"
#include "navier-stokes/perfs.h"

#define _height_SI 100.e-9
#define _widthliq_SI 150.e-9
#define muliq_SI 8.77e-4
#define mugas_SI muliq_SI/1000.
#define SIGMA_SI 5.8e-2 
#define rholiq_SI 986.
#define rhogas_SI rholiq_SI/1000.
#define u_plates_SI 100./12.   

#define MU_unit (rholiq_SI*u_plates_SI*_height_SI)
#define SIGMA_unit (rholiq_SI*_height_SI*u_plates_SI*u_plates_SI)
#define _height 1.
#define muliq (muliq_SI/MU_unit) // = 1/Re_l
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit) // =1/We_l 
#define rholiq 1.
#define rhogas rhogas_SI/rholiq_SI
#define u_plates 0.

#define TEND 1.
#define _width 5*_height
#define _widthliq _widthliq_SI/_height_SI
#define TOUT (TEND/10.)
#define TCHAR 1.
#define sliplength 1e-2
#define deltaL 1./(pow(2.,maxlevel))
#define EPSIL 0.05
#define rec 90.
#define adv 90.

// top (j = 1), bottom (j = 2)} and advancing (k = 1), receding (k = 2)
int maxlevel = 8;

//double rec = 85., adv = 110.;
double uemax = 0.01;
double dcl11 = 1e100, dcl12 = 1e100, dcl21 = 1e100, dcl22 = 1e100;
double theta_d11 = adv, theta_d12 = rec, theta_d21 = adv, theta_d22 = rec - 20.;
double middle = 0.;

double ygs(double a, double theta_e, double theta_d){
	double b = (a)*(cos((theta_d))-cos((theta_e)*pi/180.));
	return b;
}

double phi(double a, double b, double pos){
	double c = pow(((1.+cos((pi*(a - (pos)))/b))/2),2);
	return c;
}

scalar ftemp[], fd[];
vector h[];

u.n[top]  = dirichlet(0);
u.t[top]  = dirichlet(0);

u.n[bottom]  = dirichlet(0);
u.t[bottom] = robin((((x > (dcl22-EPSIL)) & (x < (dcl22+EPSIL))) ? phi(x,EPSIL,dcl22)*1. : dirichlet(0)), 1e-02);

h.t[top] = contact_angle ((x > -middle) ? rec*pi/180. : adv*pi/180.);
h.t[bottom] = contact_angle ((x < middle) ? 80.*pi/180. : adv*pi/180.);



int main (int argc, char * argv[])
{
  init_grid (1<<maxlevel);
  origin(-0.5,-0.5); 
  size (_height);

  refine (level < maxlevel);
  DT = 1e-3;
  periodic (right); 
  f.sigma = SIGMA;
  f.height = h;
    
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;

  run();
}

event init (t = 0) {
  if (!restore (file = "restart")) { 
	fraction (ftemp, (x-0.25)*(x+0.25));
	foreach(){
		f[] = 1. - ftemp[];
	}
  }
  boundary ({f});
  dump("dump-initial");
}


event adapt (i++) {
  adapt_wavelet ({f,u}, (double[]){0.001,uemax,uemax,uemax},  maxlevel);
}

event imposeangle (i = 20; i++){
	double x111 = 0., x112 = 0., y111 = 0., y112 = 0.;
	double x121 = 0., x122 = 0., y121 = 0., y122 = 0.;
	double x211 = 0., x212 = 0., y211 = 0., y212 = 0.;
	double x221 = 0., x222 = 0., y221 = 0., y222 = 0.;
	double opp11, opp12, opp21, opp22, adj11, adj12, adj21, adj22;
	vector h3[];
    heights (f, h3);
    foreach() {
		if ((y > (-0.5)) & ((x + Delta*height(h3.x[])) < x221) & ((x + Delta*height(h3.x[]))>-0.5)){
			x221 = x + Delta*height(h3.x[]);
			dcl22 = x + Delta*height(h3.x[]);
		    y221 = y;
		}
		if ((y > (-0.5 + deltaL)) & ((x + Delta*height(h3.x[])) < x222 ) & ((x + Delta*height(h3.x[]))>-0.5 ) ){
			x222 = x + Delta*height(h3.x[]);
			y222 = y;
		}
	}
	middle = dcl22 + 0.25;
	opp22 = fabs(x221 - x222) ;
	adj22 = fabs(y221 - y222) ;

	theta_d22 = (cos(pi/2. - atan(opp22/adj22)));
	
	fprintf(stderr,"%d %lf %lf %lf %lf %lf %lf\n",i, t, theta_d22*180./pi, ygs(1.,rec,theta_d22), (opp22/adj22), dcl22, middle);
	
}

event snapshot (t=0.; t += TEND/10.; t <= TEND){
  char name[80];
  sprintf (name, "snap-%g", t);
  scalar pid[];
  scalar omega[];
  vorticity (u, omega);
  foreach(){
      pid[] = fmod(pid()*(npe() + 37), npe());
      omega[] *= TCHAR;
    }
  boundary ({pid,omega});
  dump (name);
}

event movie (t += TEND/120.) {
  clear();
  box();
  squares ("u.x");
  draw_vof("f",lc = {1.,0.,1.} , lw = 2);
  save ("movie.mp4"); 
}