#include "navier-stokes/centered.h"
//#include "grid/quadtree.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "contact.h"
#include "curvature.h"
#include "utils.h"
#include "navier-stokes/perfs.h"
#include "adapt2.h"

//Dimensionless quantity

#define ksi 7.2e-04
#define Re 24.
#define Ca 2.1

//System properties (SI)
#define SIGMA 70e-03

#define rho_liq 1000
#define mu_liq 25e-03

#define rho_gas 1.2
#define mu_gas (mu_liq*ksi)

#define gravi 9.81

#define h_curt 1e-02
#define w_curt 1e-03

#define U1 (Ca*SIGMA/mu_liq)
#define V1 (Re*mu_liq/(rho_liq*w_curt))
#define h_inf (Q1/U1)
#define Q1 (w_curt*V1)
#define S1 ((mu_liq*U1*U1)/Q1)

//System characterics (dimensionless)
#define SIGMA_nd (SIGMA/(S1*h_inf*U1))

#define rho_liq_nd (rho_liq/(h_inf*S1/Q1))
#define mu_liq_nd (mu_liq/(h_inf*S1))

#define rho_gas_nd (rho_gas/(h_inf*S1/Q1))
#define mu_gas_nd (mu_gas/(h_inf*S1))

#define gravi_nd (gravi/(pow(U1,3)/Q1))

#define h_curt_nd (h_curt/h_inf)
#define w_curt_nd (w_curt/h_inf)

#define U_nd (U1/U1)
#define V_nd (V1/U1)

#define TEND 30000.
#define LEVEL 10
#define uemax 0.01

int augm;
scalar s[], s2[], s3[];
vector h[];
double l_slip = 1e-05
double l_slip_nd = (l_slip/h_inf)
double semicircle(double x, double y, double radius){
	double a = sq((x))+sq((y))-sq((radius));
	double b = min(-(a), -(y));
	return b;
}
double geomf(double x, double y, double center, double radius){
	double a = min((radius) - ((x) + (center)), (radius) + ((x) + (center)));
	double b = min((a), (y));
	//double c = max(b, semicircle(x + center, y, radius));
	return b;
}
//Boundary conditions

u.n[right] = u.n[] > 0 ? neumann(0) : 0;
p[right] = dirichlet(0);
pf[right] = dirichlet(0); 

u.n[left] = u.n[] > 0 ? neumann(0) : 0;
p[left] = dirichlet(0);
pf[left] = dirichlet(0);

u.t[bottom] = robin(U_nd,l_slip_nd);
u.n[bottom] = dirichlet(0);

u.n[top] = dirichlet( ((x > - (w_curt_nd/2.)) & (x < (w_curt_nd/2.))) ? -V_nd : 0) ;
u.t[top] = dirichlet(0) ;

h.t[bottom] = contact_angle (90.*pi/180.);
f[top] = ( ((x > - (w_curt_nd/2.)) & (x < (w_curt_nd/2.))) ? 1 : 0);

	
int main() {
  init_grid (1<<8);
  origin (-h_curt_nd/2., -h_curt_nd/2.);
  size(h_curt_nd);
  refine (level < 8);
  f.sigma = SIGMA_nd;
  f.height = h;
  rho1 = rho_liq_nd; rho2 = rho_gas_nd;
  mu1 = mu_liq_nd; mu2 = mu_gas_nd;
  for (l_slip = 1e-07; l_slip <= 1e-03; l_slip *= 10.)
	lambdaxu = l_slip_nd;
  	run();
  
} 

event init (t = 0) {
	if (!restore (file = "restart")){
	fraction (f, geomf(x, y - (0.9*h_curt_nd/2.), 0., w_curt_nd/2.));
    fprintf(ferr,"Re \t%e\t%e\t%e\n",Re,(rho_liq*w_curt*V1)/mu_liq,(rho_liq_nd*w_curt_nd*V_nd)/mu_liq_nd);
    fprintf(ferr,"Ca \t%e\t%e\t%e\n",Ca,mu_liq*U1/SIGMA,mu_liq_nd*U_nd/SIGMA_nd);
    fprintf(ferr,"Bo \t%e\t%e\n",(rho_liq*gravi/SIGMA)*((w_curt*V1)/U1)*((w_curt*V1)/U1),(rho_liq_nd*gravi_nd/SIGMA_nd)*((w_curt_nd*V_nd)/U_nd)*((w_curt_nd*V_nd)/U_nd));
	fprintf(ferr,"Sliplength (SI) \t%e\n",l_slip);
	fprintf(ferr,"Height (SI) \t%e\n",h_curt);
	fprintf(ferr,"Grid size per Sliplength \t%e\n",l_slip_nd/(h_curt_nd/pow(2.,LEVEL)));
	}
	
	fprintf(ferr,"LEVEL %d\n",LEVEL);
	fprintf(ferr,"Grid size per Sliplength %g\n",l_slip_nd/(h_curt_nd/pow(2.,LEVEL)));
    
	boundary({f});
	dump("dump-initial")
}

event acceleration (i++){
	face vector av = a;
	foreach_face(y)
    	av.y[] -= gravi_nd;
}

event adapt (i++) {
	adapt_wavelet ({f,u}, (double[]){1e-3,uemax,uemax,uemax}, LEVEL);
	
}

event snapshot (t = TEND){
  char name[80];
  sprintf (name, "s-%g", l_slip_nd);
  scalar pid[];
  scalar omega[];
  vorticity (u, omega);
  foreach(){
      pid[] = fmod(pid()*(npe() + 37), npe());
      omega[] *= 0.1;
    }
  boundary ({pid,omega});
  dump (name);
}
