#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"
//#include "navier-stokes/perfs.h"

#define _height_SI 100.e-9
#define _widthliq_SI 150.e-9
#define muliq_SI 8.77e-4
#define mugas_SI muliq_SI/1000.
#define SIGMA_SI 5.8e-2 
#define rholiq_SI 986.
#define rhogas_SI rholiq_SI/1000.
#define u_plates_SI 100./12.   

#define MU_unit (rholiq_SI*u_plates_SI*_height_SI)
#define SIGMA_unit (rholiq_SI*_height_SI*u_plates_SI*u_plates_SI)
#define _height 1.
#define muliq (muliq_SI/MU_unit) // = 1/Re_l
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit) // =1/We_l 
#define rholiq 1.
#define rhogas rhogas_SI/rholiq_SI
#define u_plates 1.

#define TEND 1.
#define _width 5*_height
#define _widthliq _widthliq_SI/_height_SI
#define TOUT (TEND/10.)
#define TCHAR 1.
#define sliplength 1e-3
#define brisbane 1./(pow(2.,maxlevel))

int maxlevel = 9;
int count= 0;
double uemax = 0.1, rec = 84., adv = 110.;
double middle;
double pos_11, pos_12, pos_21, pos_22;
double ystress_11, ystress_12, ystress_21, ystress_22 = 0.;
double opp, adj, argtan = 0.;

scalar s[], ftemp[], ftemp2[], s2[], s3[];
scalar recbot[], advbot[], rectop[], advtop[];
scalar temp_recbot[], temp_advbot[], temp_rectop[], temp_advtop[];
vector h[];

double geomtemp(double x, double y, double center, double radius){
	double a = max(max(min(-(x) - (-(center) + (radius)) , (y) + 2.), min(x + (-(center) - (radius)) , y + 2.)), min(y + (2.-2*(radius)),-x + 2.));
	return a;
}

//u.n[top]  = dirichlet(0);
//u.t[top] = robin(u_plates/2,sliplength);

u.n[bottom]  = dirichlet(0);
u.t[bottom] = robin(-u_plates,sliplength);

//h.t[top] = contact_angle ((x > -middle) ? rec*pi/180. : adv*pi/180.);
h.t[bottom] = contact_angle ((x < middle) ? rec*pi/180. : adv*pi/180.);

f[bottom] = neumann(0);

int main (int argc, char * argv[])
{
  init_grid (1<<maxlevel);
  origin(-2.,-2.); 
  size (4*_height);

  refine (level < maxlevel);
  DT = 1e-3;
  periodic (right); 
  f.sigma = SIGMA;
  f.height = h;
    
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;

  run();
}

event init (t = 0) {
  if (!restore (file = "restart")) { 
	fraction (s, min(-x + 2.,y+1.));
	//fraction (ftemp, (x-0.75)*(x+0.75));
	//fraction(s2, min(min(0.75 - x, 0.75 + x), y + 1. - 2*Delta));
	
	fraction (f, min (min(0.75-x, 0.75+x), -y - (1.- 4./pow(2,maxlevel)))); //(1.- 2./pow(2,maxlevel)))
	fraction (ftemp, min (min(2.-x, 2.+x), -y - (1.- 4./pow(2,maxlevel))));
 	foreach(){
 		s2[] = 1.*s[]*f[];
		s3[] = 1.*s[]*ftemp[];
 	}
  }
  boundary ({f});
  dump("dump-initial");
}

event solid (i++){
	double topleft, topright;
	double leftbot1x = 0., leftbot2x = 0.;
	vector h3[];
    heights (f, h3);
    foreach() {
		if(y<-1.95){
			if (((x + Delta*height(h3.x[])) < leftbot1x) & ((x + Delta*height(h3.x[]))>-2.)){
				leftbot1x = x + Delta*height(h3.x[]);
			}
			if (((x + Delta*height(h3.x[])) > leftbot2x) & ((x + Delta*height(h3.x[]))<2.)){
				leftbot2x = x + Delta*height(h3.x[]);
			}
		}
	}
	topright = fabs(leftbot1x);
	topleft = fabs(leftbot2x);
	middle = (leftbot1x+leftbot2x)/2.;
	//geoms(double x, double y, double center, double radius)
	fraction(temp_recbot, geomtemp(x, y, leftbot1x, 0.1));
	fraction(temp_advbot, geomtemp(x, y, leftbot2x, 0.1));
	//fraction(temp_recbot, max(max(min(-x - (-leftbot1x + 0.1) , y + 2.), min(x + (-leftbot1x - 0.1) , y + 2.)), min(y + 1.8,-x + 2.)));
	fraction (ftemp2, min (min((topright) -x, (topleft) +x), -y - (1.- 4./pow(2,maxlevel))));
 	foreach(){
 		s2[] = 1.*s[]*ftemp2[];
		recbot[] = (1.-temp_recbot[]);
		advbot[] = (1.-temp_advbot[]);
 	}
	foreach(){
		if (y > -1.){
			u.x[] = 0.;//s3[]*(((2.*Delta*(u_plates))/(Delta+2.*(sliplength)))+ ((2.*(sliplength)-Delta)/(2.*(sliplength)+Delta))*u.x[-1]);	
			u.y[] = 0.;//-s3[]*u.y[-1];
			f[] = 1.*s2[];
		}else {
			u.x[] = u.x[]*(1.-s[]);	
			u.y[] = u.y[]*(1.-s[]);
		}	
	}
	fprintf(stderr,"%d %lf %lf %lf %lf\n",i, t, -topright, topleft, middle);
}

event adapt (i++) {
  adapt_wavelet ({s,s2,u}, (double[]){0.01,0.01,uemax,uemax,uemax},  maxlevel);
  adapt_wavelet ({f}, (double[]){0.001}, maxlevel + 1);
  refine (x > middle-0.8 && x < middle - 0.7 && y < -1.9 && level < (maxlevel + 3));
  refine (x > middle+0.7 && x < middle + 0.8 && y < -1.9 && level < (maxlevel + 3));
  //refine (x > -middle-0.8 && x < -middle - 0.7 && y > -1.1 && y < -1. && level < (maxlevel + 1));
  //refine (x > -middle+0.7 && x < -middle + 0.8 && y > -1.1 && y < -1. && level < (maxlevel + 1));
  //adapt_wavelet ({recbot,advbot}, (double[]){0.001,0.001}, maxlevel+2);
	//adapt_wavelet ({recbot,advbot}, (double[]){0.001,0.001}, maxlevel+2);
  //adapt_wavelet ({f}, (double[]){0.001}, maxlevel);
  //adapt_wavelet ({s2,s3}, (double[]){0.01,0.01},  maxlevel-1);
  //adapt_wavelet ({u}, (double[]){uemax,uemax},  maxlevel-2);
}

event interface_end(t = TEND){
  vector h2[];
  heights (f, h2);
  foreach(){
	  if ((fabs(x) < 2.) & (y < -1.)){
		  if (h2.x[] != nodata)
			  fprintf (stdout, "%g %g\n", x + Delta*height(h2.x[]), y);
		  if (h2.y[] != nodata)
			  fprintf (stdout, "%g %g\n", x, y + Delta*height(h2.y[]));
	  }
  }	
}

event snapshot (t+= 0.1; t <= TEND){
  char name[80];
  sprintf (name, "snap-%g", t);
  scalar pid[];
  scalar omega[];
  vorticity (u, omega);
  foreach(){
      pid[] = fmod(pid()*(npe() + 37), npe());
      omega[] *= TCHAR;
    }
  boundary ({pid,omega});
  
  dump (name);
}
/*
event movies (i ++; t <= TEND)
{
  scalar omega[], m[];
  vorticity (u, omega);
  foreach()
    m[] = s[];
  boundary ({m});
  output_ppm (omega, file = "vort.mp4", box = {{-2.,-2.},{2.,-1.}},
	      min = -10, max = 10, linear = true, mask = m);
  output_ppm (f, file = "f.mp4", box = {{-2.,-2.},{2.,-1.}},
	      linear = false, min = 0, max = 2, mask = m);
}*/

event movie (t += 1./100.) {
  clear();
  box();
  squares ("u.x");
  draw_vof("f",lc = {1.,0.,1.} , lw = 2);
  save ("ux.mp4"); 
}

event movie2 (t += 1./100.) {
  clear();
  box();
  cells();
  draw_vof("f",lc = {1.,0.,1.} , lw = 2);
  save ("mesh.mp4"); 
}
