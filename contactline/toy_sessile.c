#define JVIEW 1
#define ADAPT 1
#define DYN 1
#define DDB 0

#if JVIEW
#include "display.h"
#endif

#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"

//#define _height_SI 50.e-9    
//#define _width_SI 5.*50.e-9   //running on 5 processors will bring the height
                                // down to 50nm   
//#define _widthliq_SI 50.e-9

#define _height_SI 2.5323477913879776e-6
#define _width_SI 2.5323477913879776e-6   
#define _widthliq_SI 0.5*2.5323477913879776e-6

#define muliq_SI 8.77e-4
#define mugas_SI muliq_SI/100.
#define SIGMA_SI 5.78e-2 
#define rholiq_SI 986.
#define rhogas_SI rholiq_SI/100.
#define u_plates_SI 1.397217787913341 // For Ca = 0.0212   
//#define u_plates_SI 69.860889396 // For Ca = 1.06

#define _width 1.
#define _height _width // (_width/npe())    
#define MU_unit (rholiq_SI*u_plates_SI*_height_SI)
#define SIGMA_unit (rholiq_SI*_height_SI*u_plates_SI*u_plates_SI)
#define T_unit ((_height_SI*u_plates)/(_height*u_plates_SI))
#define L_unit (_height_SI/_height)

#define muliq (muliq_SI/MU_unit) // = 1/Re_l
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit) // =1/We_l 
#define rholiq 1.
#define rhogas rhogas_SI/rholiq_SI
#define u_plates 1.
#define _widthliq (_widthliq_SI/L_unit) 

#define TEND_SI 1e-6
#define TOUT_SI 0.4e-9
#define TEND (TEND_SI/T_unit)
#define TOUT (TOUT_SI/T_unit)
#define TCHAR 1.

#define Ca (muliq/SIGMA)
#define Re (1./muliq)
#define Pe 10.0
#define Cn 0.01
#define S sqrt((Ca*Cn)/Pe)
#define alpha 3.0
#define slip (S*sqrt(alpha/0.8679))

#if DDB 
#define slip2 (pow(slip,2)/2.)
#else
#define slip2 0.
#endif

#define NN 128
#define theta_eq (80.0*pi/180.)


#define LEVEL 8
#define u_err 1e-1
#define f_err 0.

double sol(double y, double l, double u){
        double a = (((10.*u)/(1.+2.*l))*(y+0.4));
        return a;
}

double ORI (double nx, double ny){
	double angle = 0.0;
	if ((nx > 0) & (ny >= 0))
		angle = atan(ny/nx);
	if ((nx > 0) & (ny < 0))
		angle = atan(ny/nx) + 2.*pi;
	if (nx < 0)
		angle = atan(ny/nx) + pi;
	if ((nx == 0) & (ny > 0))
		angle = pi/2.;
	if ((nx == 0) & (ny < 0))
		angle = 3.*pi/2.;
	return angle;
} 

double ORI2 (double nx, double ny, double prev){
	double angle = atan(ny/(nx + 1E-30));
	if (fabs(angle) > pi/18.)
		angle = prev;
	return angle;
} 

double dyn_angle = 80.*pi/180., angle_eq = 80.*pi/180.;
double minx = -0.5, maxx = 0.5;
double prev_r, prev_a;
double clb1, clt1, clb2, clt2, cl2b1, cl2t1;
double t_b, t_t, tmp;
vector h[];
scalar f2[];
scalar sink[];


u.n[bottom]  = dirichlet(0.);
// u.t[bottom] = DDBb(0.,slip, slip2);
// u.t[bottom]  = dirichlet(0.);
u.t[bottom]  = robin(0., slip);
// u.t[bottom] = gnbc(plate.0.,delta(epsilon)*ystress(t));

//f[bottom] = dirichlet(sink);
h.t[bottom] = contact_angle(dyn_angle);
//h.t[top] = contact_angle(x>0. ? (rec) : (angle_eq));


#if dimension > 2
printf("error")
exit(5)
#endif


int main ()
{

  init_grid (NN);

  origin(-0.5,slip); 
  size (_width);

  lmbda = slip;
  lmbda2 = slip2;

  periodic (right); 
  
  f.sigma = SIGMA;
  f.height = h;  
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;
  
  run();
}


event init (t = 0) {
  if (!restore (file = "restart")) {
	fraction (f, - (sq(x) + sq(y) - sq(_widthliq/2)));
    fprintf(ferr,"#Re \t%e\t%e\n",1./muliq,u_plates_SI*_height_SI*rholiq_SI/muliq_SI);
    fprintf(ferr,"#Ca \t%e\t%e\n",muliq/SIGMA,muliq_SI*u_plates_SI/SIGMA_SI);
    fprintf(ferr,"#La \t%e\n",SIGMA*rholiq*_height/sq(muliq));
    fprintf(ferr,"#We \t%e\n",rhogas*u_plates*u_plates*_height/SIGMA);
    fprintf(ferr,"#Slip \t%e\t%e\n",slip, slip2);
    fprintf(ferr,"#L_unit \t%e\n",L_unit);
    fprintf(ferr,"#Slip_SI \t%e\t%e\n",L_unit*slip, L_unit*slip2);
    fprintf(ferr,"#Grid point per slip \t%e\t%e\n",slip/(1./pow(2, LEVEL)),slip2/(1./NN));
    fprintf(ferr,"#shift \t%e\n",(_height_SI-L_unit*slip)/_height_SI);
    fprintf(ferr,"#Processors: \t%d\n",npe());
  }
  boundary ({f});
}

#if ADAPT
event adapt (i++){
    adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err},  LEVEL);
}
#endif


#if DYN
event impose (i++){
	char name[80];
	sprintf (name, "data-%g-%g-%g-%d", slip, slip2, alpha, DYN);
	static FILE * fp = fopen(name, "w");
	double radius = 0.0;
	double Ca_CL = 0.0;
	double Ca_CL2 = 0.0;
	double angle = 0.0;
	double dudy = 0.0;
	double d2udy = 0.0;
	double dudx = 0.0;
	double d2udx = 0.0;
	foreach_boundary(bottom){
		
		
		//sink[] = -slip*fabs(dudy) - slip2*fabs(dudy);
		//d2udx = (u.x[-1,0] - 2.*u.x[] + u.x[1,0])/pow(Delta,2);
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			if (x > 0.){
				radius = x + Delta*height(h.x[]);
				Ca_CL = Ca*u.x[];
				coord n = {0};
				n = interface_normal (point,f);
				angle = pi/2 - ORI(n.x,n.y);
				dudy = (u.x[0,1] - u.x[])/Delta;
				d2udy = (u.x[0,-1] - 2.*u.x[] + u.x[0,1])/pow(Delta,2);
			}
			if (x < 0.){
				Ca_CL2 = Ca*u.x[];
			}
		}
	}
  fflush(fp);
  dyn_angle = acos(cos(theta_eq) - (3./(sqrt(2.)*2.0))*Ca_CL*alpha);
  fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", t, t*T_unit, 2.*radius, Ca_CL, Ca_CL2, 180.*angle/pi, 180.*dyn_angle/pi, dudy, d2udy);
}
#endif

// event extract (i++){
// 	char name[80];
// 	#if JVIEW
// 	sprintf (name, "JVIEW_data_a%g", alpha);
// 	#else
// 	sprintf (name, "data_angle%g_LEVEL%d", angle_eq*180./pi, LEVEL);
// 	#endif
// 	static FILE * fp = fopen(name, "w");
// 	double angle_extracted = 0., angle_above = 0., angle_extrapolated = 0., position = 0.;
// 	scalar kappa[];
// 	double Ca_CL = 0.;

// 	curvature (f,kappa);
	
// 	foreach_boundary(bottom){
// 		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.)){
// 			Ca_CL = Ca*u.x[];
// 			position = x + Delta*height(h.x[]);
// 			coord n = {0};
// 			n = interface_normal (point,f);
// 			angle_extracted = atan2(n.x, n.y);
// 		}
// 	}

// 	foreach(){
// 		if ((y <= (slip + Delta)) & (y > (slip)) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
// 				// coord n = {0};
// 				// n = interface_normal (point,f);
// 				// angle_above = atan2(n.x, n.y);
				
// 				angle_above = acos(cos(angle_eq) - (3./(sqrt(2.)*2.0))*Ca*u.x[]*alpha);
// 				double tmp = pow(((h.x[1,0] - h.x[])/1.0), 2);
// 				angle_extrapolated = angle_above + slip*kappa[]*sqrt(1 + tmp)/sin(angle_above);
// 		}
// 	}

// 	dyn_angle = angle_extrapolated;

// 	fflush(fp);
// 	fprintf(fp,"%lf %lf %lf %lf %lf\n", t, position, 180.*angle_above/pi, 180.*angle_extrapolated/pi, 180.*angle_extracted/pi);
// }

event output (t = 0.; t += 0.001; t <= TEND){
	char name[80];
	sprintf (name, "massflux_%g", t);
	FILE * fp = fopen(name, "w");
	scalar s[];
	foreach(){
		if ((y <= (slip + Delta)) & (y > (slip))){
			s[] = u.y[];
		}
	}
	foreach(){
		if ((y <= (slip + Delta)) & (y > (slip))){
			fprintf(fp,"%lf %lf\n", x, s[]);
		}
	}
	fclose(fp);
}

// event output (t = TEND){
// 	char name[80];
// 	sprintf (name, "cells-%g-%d",t, tid());
// 	FILE * fp = fopen (name, "w");
// 	output_cells(fp);
// }

// event output (t = TEND){
// 	char name[80];
// 	sprintf (name, "FACETEND");
// 	FILE * fp = fopen (name, "w");
// 	output_facets(f, fp);
// }

// event final (t = 0.; t+= 0.01; t <= TEND)
// {
//   char name[80];
//   sprintf (name, "facets_all-%g-%g-%g-%d", slip, slip2, alpha, DYN);
//   static FILE * fp2 = fopen(name, "w");
//   char name2[80];
//   sprintf (name2, "facets_sink_t%g-%g-%g-%g-%d", t, slip, slip2, alpha, DYN);
//   FILE * fp3 = fopen(name2, "w");
//   char name3[80];
//   sprintf (name3, "sink_t%g-%g-%g-%g-%d", t, slip, slip2, alpha, DYN);
//   FILE * fp4 = fopen(name3, "w");
//   output_facets (f, fp2);
//   output_facets (f, fp3);
//   foreach_boundary(bottom){
// 	fprintf(fp4,"%lf %lf\n", x, sink[] );
//   }
//   fclose(fp3);
//   fclose(fp4);
// }