#define JVIEW 1
#define ADAPT 1

#if JVIEW
#include "display.h"
#endif

#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"

#define DDB 0
#define DYN 1

#define LEVEL 9
#define uerr 1e-3
#define ferr 1e-6

#define xi 1e-1
#define mu_l 0.25
#define rho_l 1.0
#define sig 7.5
#define R0 0.5
#define U sqrt(sig/(rho_l*R0))

#define mu_g (xi*mu_l)
#define rho_g (xi*mu_l)

#define Ca 0.12909944487358058
#define Re 7.745966692414834

#define Cn 0.01
#define Pe 20.0
#define S sqrt((Ca*Cn)/Pe)
#define alpha 3.
#define slip (S*sqrt(alpha/0.8679))


#if DDB 
#define slip2 (pow(slip,2)/2.)
#else
#define slip2 0.
#endif

#define theta_eq (80.*pi/180.)

vector h[];

double dyn_angle = 90.*pi/180.;

//u.t[bottom] = DDBb(0.,slip,slip2);
u.t[bottom] = robin(0.,slip);
u.n[bottom]  = dirichlet(0);

h.t[bottom] = contact_angle(dyn_angle);

double ORI (double nx, double ny){
	double angle = 0.0;
	if ((nx > 0) & (ny >= 0))
		angle = atan(ny/nx);
	if ((nx > 0) & (ny < 0))
		angle = atan(ny/nx) + 2.*pi;
	if (nx < 0)
		angle = atan(ny/nx) + pi;
	if ((nx == 0) & (ny > 0))
		angle = pi/2.;
	if ((nx == 0) & (ny < 0))
		angle = 3.*pi/2.;
	return angle;
} 

int main()
{
  origin (-1, 0.);
  size(2);
  N = 256;
  f.sigma = sig;
  f.height = h;  
  rho1 = rho_l; rho2 = rho_g;
  mu1 = mu_l; mu2 = mu_g;  
  
  lmbda = slip;
  lmbda2 = slip2;	

  run();
}

event init (t = 0)
{
  fraction (f, - (sq(x) + sq(y + slip) - sq(0.5)));
  //fraction (f, - (sq(x) + sq(y) - sq(0.5)));
  fprintf(stderr, "%lf %lf %lf", slip/(1./pow(2,LEVEL-1)), U/R0, U);
}

#if ADAPT
event adapt (i++){
    adapt_wavelet ({f,u}, (double[]){ferr,uerr,uerr,uerr},  LEVEL);
}
#endif

#if DYN
event impose (i++){
	char name[80];
	sprintf (name, "log-%g-%g", slip, slip2);
	static FILE * fp = fopen(name, "w");
	double radius = 0.0;
	double Ca_CL = 0.0;
	double angle = 0.0;
	double dudy = 0.0;
	double d2udy = 0.0;
	foreach_boundary(bottom){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			radius = x + Delta*height(h.x[]);
			Ca_CL = mu_l*(u.x[]/U)/sig;
			coord n = {0};
			n = interface_normal (point,f);
			angle = pi/2 - ORI(n.x,n.y);
			dudy = (u.x[0,1] - u.x[])/Delta/U;
			d2udy = (u.x[0,-1] - 2.*u.x[] + u.x[0,1])/pow(Delta,2)/U;
		}
	}
  fflush(fp);
  dyn_angle = acos(cos(theta_eq) - (3./(sqrt(2.)*2.0))*Ca_CL*alpha);
  fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf %lf %lf\n", fabs(0.5+radius), t, t*(U/R0),t/(U/R0), Ca_CL, 180.*angle/pi, 180.*dyn_angle/pi, dudy, d2udy);
}
#endif

event end (t = 0.; t+= 0.01; t <= 1.0)
{
  char name[80];
  sprintf (name, "facets_all-%g", alpha);
  static FILE * fp2 = fopen(name, "w");
  char name2[80];
  sprintf (name2, "facets-%g", t);
  FILE * fp3 = fopen(name2, "w");
  output_facets (f, fp2);
  output_facets (f, fp3);
  fclose(fp3);
}