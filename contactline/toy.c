#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"

//#define _height_SI 50.e-9    
//#define _width_SI 5.*50.e-9   //running on 5 processors will bring the height
                                // down to 50nm   
//#define _widthliq_SI 50.e-9

#define _height_SI 2.5323477913879776e-6
#define _width_SI 5.*2.5323477913879776e-6   
#define _widthliq_SI 2.5323477913879776e-6

#define muliq_SI 8.77e-4
#define mugas_SI muliq_SI/100.
#define SIGMA_SI 5.78e-2 
#define rholiq_SI 986.
#define rhogas_SI rholiq_SI/100.
#define u_plates_SI 1.397217787913341 // For Ca = 0.0212   
//#define u_plates_SI 69.61915621436717

#define _width 1.
#define _height (_width/npe())    
#define MU_unit (rholiq_SI*u_plates_SI*_height_SI)
#define SIGMA_unit (rholiq_SI*_height_SI*u_plates_SI*u_plates_SI)
#define T_unit ((_height_SI*u_plates)/(_height*u_plates_SI))
#define L_unit (_height_SI/_height)

#define muliq (muliq_SI/MU_unit) // = 1/Re_l
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit) // =1/We_l 
#define rholiq 1.
#define rhogas rhogas_SI/rholiq_SI
#define u_plates 1.
#define _widthliq (_widthliq_SI/L_unit) 

#define TEND_SI 8e-7
#define TOUT_SI 0.4e-9
#define TEND (TEND_SI/T_unit)
#define TOUT (TOUT_SI/T_unit)
#define TCHAR 1.

#define Ca (muliq/SIGMA)
#define Ca_n 1.06
#define Re (1./muliq)
#define Pe 100.0
#define Cn 0.01
#define S sqrt((Ca*Cn)/Pe)
#define S2 sqrt((Ca_n*Cn)/Pe)
#define alpha 3.
#define slip (S*sqrt(alpha/0.8679))
#define slip2 S2*sqrt(alpha/0.8679)

#define NN 512

//#define sol(y,l) = (((10.*u_plates)/(1.+2.*l))*(y+0.4))

double sol(double y, double l, double u){
        double a = (((10.*u)/(1.+2.*l))*(y+0.4));
        return a;
}

double ORI (double nx, double ny){
	double angle = 0.0;
	if ((nx > 0) & (ny >= 0))
		angle = atan(ny/nx);
	if ((nx > 0) & (ny < 0))
		angle = atan(ny/nx) + 2.*pi;
	if (nx < 0)
		angle = atan(ny/nx) + pi;
	if ((nx == 0) & (ny > 0))
		angle = pi/2.;
	if ((nx == 0) & (ny < 0))
		angle = 3.*pi/2.;
	return angle;
} 

double ORI2 (double nx, double ny, double prev){
	double angle = atan(ny/(nx + 1E-30));
	if (fabs(angle) > pi/18.)
		angle = prev;
	return angle;
} 

double rec = 89.*pi/180., adv = 91.*pi/180.;
//double rec_t = 90.*pi/180., adv_t = 90.*pi/180., rec_b = 90.*pi/180., adv_b = 90.*pi/180.;
double minx = -0.5, maxx = 0.5;
double prev_r, prev_a;
double clb1, clt1, clb2, clt2, cl2b1, cl2t1;
double t_b, t_t, tmp;
vector h[];
scalar f2[];

u.n[top]  = dirichlet(0);
u.n[bottom]  = dirichlet(0);
h.t[bottom] = contact_angle(x<0. ? (rec) : (adv));
h.t[top] = contact_angle(x>0. ? (rec) : (adv));
//h.t[bottom] = contact_angle(x<0. ? (rec_b) : (adv_b));
//h.t[top] = contact_angle(x>0. ? (rec_t) : (adv_t));
u.t[bottom] = robin(-u_plates,slip);
u.t[top] = robin(u_plates,slip);

#if dimension > 2
printf("error")
exit(5)
#endif


int main ()
{

  init_grid (NN);
  
  origin(-0.5,-0.5); 
  size (_width);

  lmbda = slip;
  
  periodic (right); 
  
  f.sigma = SIGMA;
  f.height = h;  
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;
  
  run();
}


event init (t = 0) {
  if (!restore (file = "restart")) {

    fraction (f2, (x-_widthliq/2.)*(x+_widthliq/2.));
    foreach(){
	f[] = 1. - f2[];
    }

    fprintf(ferr,"#Re \t%e\t%e\n",1./muliq,u_plates_SI*_height_SI*rholiq_SI/muliq_SI);
    fprintf(ferr,"#Ca \t%e\t%e\n",muliq/SIGMA,muliq_SI*u_plates_SI/SIGMA_SI);
    fprintf(ferr,"#La \t%e\n",SIGMA*rholiq*_height/sq(muliq));
    fprintf(ferr,"#We \t%e\n",rhogas*u_plates*u_plates*_height/SIGMA);
    fprintf(ferr,"#Slip \t%e\t%e\n",slip, slip2);
    fprintf(ferr,"#L_unit \t%e\n",L_unit);
    fprintf(ferr,"#Slip_SI \t%e\t%e\n",L_unit*slip, L_unit*slip2);
    fprintf(ferr,"#Grid point per slip \t%e\t%e\n",slip/(1./NN),slip2/(1./NN));
    fprintf(ferr,"#shift \t%e\n",(_height_SI-L_unit*slip)/_height_SI);
    fprintf(ferr,"#Processors: \t%d\n",npe());
  }
  boundary ({f});
  foreach(){
      u.x[] = sol(y,slip,u_plates);
     }
}

event imposeangle (i++){
double mini = -1E-10, maxi = 1E-10;
double urec = 1e100, uadv = 1e100;
double theta_r = 0.0, theta_a = 2.*pi;
double shift_r = 0.0, shift_a = 0.0;

vector h2[];
/*scalar pid[], ff[];
foreach() {
	pid[] = fmod(pid()*(npe() + 37), npe());
	ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
}
boundary ({pid,ff});*/
heights(f,h2);
foreach_boundary(bottom reduction(min:mini) reduction(min:urec) reduction(max:theta_r)){
	//if ((h2.x[] != nodata) & (f[0,1] < 1.0e-6) & (f[0,1] > (1. - 1.0e-6))){
	if (h2.x[] != nodata){
		if ((mini < 0.) & ((x + Delta*height(h2.x[])) < mini)){
			if (fabs(u.x[]) < urec){ 
				mini = x + Delta*height(h2.x[]);
				urec = u.x[];
				if (theta_r <= pi/2.){
					coord n = {0};
        			n = interface_normal (point,f);
        			theta_r = pi/2. + ORI2(n.x,n.y, prev_r);//atan(n.y/(n.x + 1E-30));
        			//mini2 = x + Delta*height(h2.x[1,0]);
				}
				//rec = acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-u.x[]));
			}
		}
	}
}
foreach_boundary(bottom reduction(max:maxi) reduction(min:uadv) reduction(min:theta_a)){
	//if ((h2.x[] != nodata) & (f[0,1] < 1.0e-6) & (f[0,1] > (1. - 1.0e-6))){
	if (h2.x[] != nodata){
		if ((maxi > 0.) & ((x + Delta*height(h2.x[])) > maxi)){
			if (fabs(u.x[]) < uadv){ 
				maxi = x + Delta*height(h2.x[]);
				uadv = u.x[];
				if (theta_a >= pi/2.){
					coord n = {0};
        			n = interface_normal (point,f);
        			theta_a = pi/2. - ORI2(n.x, n.y, prev_a);//atan(n.y/(n.x + 1E-30));
        			//maxi2 = x + Delta*height(h2.x[1,0]);
				}
				//rec = acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-u.x[]));
			}
		}
	}
}
/*
	//if ((h2.x[] != nodata) & (f[0,1] < 1.0e-6) & (f[0,1] > (1. - 1.0e-6))){
	if (h2.x[] != nodata){
		if ((maxi > 0.) & ((x + Delta*height(h2.x[])) > maxi)){
			maxi = x + Delta*height(h2.x[]);
			uadv = u.x[];
			//adv = pi - acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-u.x[]));
		}
	}
}*/
//rec = pi/2.;
//adv = pi/2.;

if ((fabs(theta_r-rec)*180./pi) < 10.)
	shift_r = fabs(theta_r-rec);
if ((fabs(theta_a-adv)*180./pi) < 10.)
	shift_a = fabs(theta_a-adv);
prev_r = shift_r;
prev_a = shift_a;
//rec = acos((3./(sqrt(2.)*2.0))*Ca*alpha*fabs(urec)) - pi/400.;//fabs(-u_plates-urec)); //- pi/73.;
//adv = pi - acos((3./(sqrt(2.)*2.0))*Ca*alpha*fabs(uadv)) + pi/600.;//*fabs(-u_plates-uadv));// + pi/73.;
fprintf(stderr,"%g %g %g %g %g %g %g %g %g\n",t*T_unit*1e12, mini, maxi, rec*180./pi, adv*180./pi, theta_r*180./pi, theta_a*180./pi, shift_r*180./pi, shift_a*180./pi);
//fprintf(stdout,"%g %g %g %g\n", maxi, maxi2, mini, mini2);
}
/*
event testheight (i++){

double urec_b = 0.0, uadv_b = 0.0, urec_t = 0.0, uadv_t = 0.0;
double uadv = 0.0, urec = 0.0, app_a = 0.0, app_r = 0.0;
vector h2[];
scalar pid[], ff[], c[];
   foreach() {
    pid[] = fmod(pid()*(npe() + 37), npe());
    ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
  }
boundary ({pid,ff});
heights(ff,h2);
	foreach(reduction(max:maxx) reduction(min:minx)) {
		if ((x > -0.5) & (x < 0.)){
			if (y < (-0.5 + Delta)){
			if (h2.x[] != nodata){
				clb1 = x + Delta*height(h2.x[]);
				tmp = 3.*Delta;
			}
			}
			if (y > (-0.3 - Delta)){
			if (h2.x[] != nodata){
				clt1 = x + Delta*height(h2.x[]);
			}
			}
			if ((y < (-0.5 + 2.*Delta)) & (y > (-0.5 + Delta))){
			if (h2.x[] != nodata){
				clb2 = x + Delta*height(h2.x[]);
			}
			}
			if ((y > (-0.3 - 2.*Delta)) & (y < (-0.3 - Delta))){
			if (h2.x[] != nodata){
				clt2 = x + Delta*height(h2.x[]);
			}
			}	
		}
		if ((x > 0.) & (x < 0.5)){
			if (y < (-0.5 + Delta)){
			if (h2.x[] != nodata){
				cl2b1 = x + Delta*height(h2.x[]);
				//uadv = u.x[];
			}
			}
			if (y > (-0.3 - Delta)){
			if (h2.x[] != nodata){
				cl2t1 = x + Delta*height(h2.x[]);
			}
			}
		}
	}
double Caloc = 0.0;
double clb1bis = 0.0;
//double app_r_t = 0.0, app_a_t = 0.0, app_r_b = 0.0, app_a_b = 0.0;
	foreach_boundary(bottom){
		if ((h2.x[] != nodata) & (ff[]< (1. - 1e-6)) & (ff[]> (1e-6))){
			clb1bis = x + Delta*height(h2.x[]);
			Caloc = 2.*muliq*(fabs(u.x[-1,0])+2*fabs(u.x[])+fabs(u.x[1,0]))/(4.*SIGMA);
			//Ca = mu_liq*u.x[]/sig;
			if ((x > -0.5) & (x < 0.)){
				urec_b = u.x[];
				urec = u.x[];
				rec_b = acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-urec_b));
			}
			if ((x > 0.) & (x < 0.5)){
				uadv_b = u.x[];
				uadv = u.x[];
				adv_b = pi - acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-uadv_b));
			}
		}
	}
	foreach_boundary(top){
		if ((h2.x[] != nodata) & (ff[]< (1. - 1e-6)) & (ff[]> (1e-6)) & (x< (0.)) ){
			if ((x > 0.) & (x < 0.5)){
				urec_t = u.x[];
				//uadv = 0.25*(u.x[-1,0]+2.*u.x[]+u.x[1,0]);
				rec_t = acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(u_plates-urec_t));
			}
			if ((x > -0.5) & (x < 0.)){
				uadv_t = u.x[];
				//urec = 0.25*(u.x[-1,0]+2.*u.x[]+u.x[1,0]);
				adv_t = pi - acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(u_plates-uadv_t));
			}
		}
	}
app_r = acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-urec));
app_a = pi - acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(u_plates-uadv));
rec = app_r;
adv = app_a;
t_b = 180. - 180.*atan(tmp/(fabs(clb1-clb2)+1e-15))/pi;
t_t = 180. - 180.*atan(tmp/(fabs(clt1-clt2)+1e-15))/pi;
fprintf(stdout, "%g %g %g %g %g %g %g %g %g %g %g %g\n", t*T_unit*1e12, t_b, t_t, fabs(clb1-clt1)*L_unit*1e09, clb1, clt1, Caloc, clb1bis, 180.*rec/pi, 180.*app_r/pi, 180.*adv/pi, 180.*app_a/pi);
}
*/
/**
event snapshot (t += TOUT) {

	  char name[100];
	    sprintf (name, "facet_t-%05.0fps_%d",t*T_unit*1.e+12,pid());

	      FILE * fp = fopen (name, "w");
	        scalar pid[], ff[];
		  foreach() {

			      pid[] = fmod(pid()*(npe() + 37), npe());
			          ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
				    }
		    boundary ({pid,ff});

		      output_facets (ff, fp);

		        fclose (fp);

}**/

event snapshot2 (t = TEND) {

	  char name[100];
	    sprintf (name, "VOF90_%d_%d",NN,pid());

	      FILE * fp = fopen (name, "w");
	        scalar pid[], ff[];
		  foreach() {

			      pid[] = fmod(pid()*(npe() + 37), npe());
			          ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
				    }
		    boundary ({pid,ff});

		      output_facets (ff, fp);

		        fclose (fp);

}

/**
event output (t = 0){
char name[100];
	    sprintf (name, "facets");

	      FILE * fp = fopen (name, "w");
 vector h2[];
scalar pid[], ff[], c[];
   foreach() {
    pid[] = fmod(pid()*(npe() + 37), npe());
    ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
  }
boundary ({pid,ff});
heights(ff,h2);
foreach(reduction(max:maxx) reduction(min:minx)){
	if ((h2.x[] != nodata) & (ff[]< (1. - 1e-6)) & (ff[]> (1e-6))){
		fprintf(fp, "%g %g\n", x + Delta*height(h2.x[]), y);
		}
		}
	fclose(fp);
}**/
/**
event movie_plot (t += TOUT/8.){
  //char fname[80];
  scalar omega[];
  vorticity (u, omega);
  foreach()
    omega[] *= TCHAR;
  clear();
  
  view (fov = 3.93493, quat = {0,0,0,1}, tx = -0.00146128, ty = 0.402463, bg = {1,1,1}, width = 1000, height = 200, samples = 1);
  // cells();
  box();
  squares("u.x"); 
  draw_vof("f",lc = {1.,0.,1.} , lw = 2);
  save ("movie.mp4");
  //sprintf (fname, "t-%05.0fps.png", t*T_unit*1.e+12);
  //save (file = fname);
  
}**/


/**


event snapshot (t = TEND) {

	  char name[100];
	    sprintf (name, "facet_t-%05.0fps_%d",t*T_unit*1.e+12,pid());

	      FILE * fp = fopen (name, "w");
	        scalar pid[], ff[];
		  foreach() {

			      pid[] = fmod(pid()*(npe() + 37), npe());
			          ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
				    }
		    boundary ({pid,ff});

		      output_facets (ff, fp);

		        fclose (fp);

}

event velo (t = TEND) {
  char name[100];
  sprintf (name, "velo%d",pid());
  FILE * fp4 = fopen (name, "w");
  scalar pid[], ff[];
  foreach() {
    pid[] = fmod(pid()*(npe() + 37), npe());
    ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
  }
  boundary ({pid,ff});
  foreach_boundary(bottom){
	fprintf (fp4, "%g %g \n", x, u.x[]);
  }
}

event ouputstream(t = TEND) {
	
  char name[100];
  sprintf (name, "stream");
  scalar pid[], ff[], pos[];
  foreach() {
    pid[] = fmod(pid()*(npe() + 37), npe());
    ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
  }
  
  boundary ({pid,ff});
  //FILE * fp5 = fopen (name, "w");
  output_vtu_MPI( (scalar *) {ff}, (vector *) {u}, name, 0);
  //fclose(fp5);
    //     output_vtk (scalar * list, int n, FILE * fp, bool linear)

}**/


