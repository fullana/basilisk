@if _XOPEN_SOURCE < 700
  @undef _XOPEN_SOURCE
  @define _XOPEN_SOURCE 700
@endif
@if _GNU_SOURCE
@include <stdint.h>
@include <string.h>
@include <fenv.h>
@endif
#define _CATCH
#define dimension 2
#define BGHOSTS 2
#include "common.h"
#include "grid/quadtree.h"
@include "_boundarydecl.h"
#ifndef BASILISK_HEADER_0
#define BASILISK_HEADER_0
#line 1 "geometry_toy.c"
#include "navier-stokes/centered.h"
#include "contact.h"
#include "vof.h"
#include "tension.h"
#include "display.h"

#define RADIUS (0.5)
#define DIAMETER (2.*RADIUS)
#define MU sqrt(DIAMETER/LAPLACE)
#define TMAX 0.25*(sq(DIAMETER)/MU)

#define eps 0.1
#define initial_position sqrt(pow(RADIUS, 2) - pow(eps, 2))

scalar f[], * interfaces = {f};
double LAPLACE = 10000;
int LEVEL = 6;
vector h[];
double theta0 = 70.;
h.t[bottom] = contact_angle (theta0*pi/180.);

int main()
{ 
    size (2);
    origin (-1., 0.);
    N = pow(2.,LEVEL);

    const face vector muc[] = {MU,MU};
    mu = muc;

    f.height = h;
    f.sigma = 1.;
    run();	
}


event init (t = 0)
{
  fraction (f, -(sq(x) + sq(y) - sq(RADIUS)));
//   fraction (f, -(sq(x) + sq(y) - sq(RADIUS)));
  boundary ({f});
  }


event logfile (i++; t <= TMAX)
{
	char name[80];
	sprintf (name, "eps%g",eps);
	static FILE * fp = fopen(name, "w");
	scalar un[];
    double position_x = 0., position_y = 0, angle_extracted = 0.;
	foreach()
	    un[] = norm(u);
    // foreach_boundary(bottom){
	// 	if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x >= 0.)){
	// 		position = x + Delta*height(h.x[]);
	// 		coord n = {0};
	// 		n = interface_normal (point,f);
	// 		angle_extracted = atan2(n.x, n.y);
	// 	}
	// }
    foreach(){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x >= 0.) & (y >= eps) & (y <= eps + Delta)){
			position_x = x + Delta*height(h.x[]);
            position_y = y;
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
		}
	}
    double angle_eps = pi/2. - atan(eps/initial_position);
    double angle_eps_theta = (theta0/90.)*pi/2. - atan(eps/initial_position);
  	fprintf (fp, "%g %g %g %g %g %g %g %g\n", MU*t/sq(DIAMETER), normf(un).max*MU, initial_position, position_x, position_y, 180.*angle_extracted/pi, 180.*angle_eps/pi, 180.*angle_eps_theta/pi);
	fflush(fp);
}
#endif
