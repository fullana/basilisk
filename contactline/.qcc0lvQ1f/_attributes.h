typedef struct {

#line 1028 "/home/tf/basilisk/src/common.h"

  double (** boundary) (Point, Point, scalar, void *);
  double (** boundary_homogeneous) (Point, Point, scalar, void *);
  double (* gradient) (double, double, double);
  void (* delete) (scalar);
  char * name;
  struct {
    int x;

    int y;




  } d;
  vector v;
  int face;
  bool nodump, freed;
  int block;
  scalar * depends;

#line 18 "/home/tf/basilisk/src/grid/stencils.h"

  double * write;
  int * read;
  int dirty;




#line 17 "/home/tf/basilisk/src/grid/multigrid-common.h"

  void (* prolongation) (Point, scalar);
  void (* restriction) (Point, scalar);

#line 8 "/home/tf/basilisk/src/grid/tree-common.h"

  void (* refine) (Point, scalar);

#line 94 "/home/tf/basilisk/src/grid/tree-common.h"

  void (* coarsen) (Point, scalar);

#line 81 "/home/tf/basilisk/src/fractions.h"

  vector n;

#line 454 "/home/tf/basilisk/src/heights.h"

  vector height;

#line 27 "/home/tf/basilisk/src/vof.h"

  scalar * tracers, c;
  bool inverse;

#line 20 "/home/tf/basilisk/src/iforce.h"

  scalar phi;

#line 21 "/home/tf/basilisk/src/tension.h"

  double sigma;
