#define JVIEW 1
#define SLIP 0
#define GNBC 0
#define TOY 0
#define DYN 0
#define NEWTOY 1

#if JVIEW
// #include "display.h"
#endif
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "contact.h"

#define rho_1 1.
#define rho_2 1e-2

#define g 0.0

#define sig 1.
#define V_s sqrt(Ca)
#define mu_1 (V_s)
#define mu_2 (V_s*1e-2)

#define l_c 0.5
#define L (2.*l_c)

#define Re (rho_1*V_s*l_c/mu_1)

#define TCHAR (l_c/V_s)
#define TEND (0.15*TCHAR)

#define u_err (2e-3)
#define f_err 0.

#if TOY
#define Pe 3.0
#define Cn 0.01
#define S sqrt((Ca*Cn)/Pe)
#define alpha 1.0
#define eps (S*sqrt(alpha/0.8679))
#else
double eps = 0.1;
#endif

int LEVEL = 7;
double Ca = 5e-3;

vector h[];

#if GNBC
double bell(double y, double eps){
	return ((1. - pow(tanh(y/eps), 2))/eps);
}
double Young_stress(double y, double eps, double theta, double theta_eq){
	return bell(y, eps)*(cos(theta_eq) - cos(theta));
}
scalar gnbc[];
double dynamic_angle = pi/2., angle_eq = 80.*pi/180.;
h.t[bottom] = contact_angle(dynamic_angle);
#if SLIP
u.t[bottom] = robin(gnbc[], eps);
#else
u.t[bottom] = dirichlet(gnbc[]);
#endif
#else
#if DYN
double dynamic_angle = pi/2., angle_eq = 80.*pi/180.;
h.t[bottom] = contact_angle(dynamic_angle);
#else
// double angle_eq = 80.*pi/180.;
// h.t[bottom] = contact_angle(angle_eq);
double dynamic_angle = pi/2., angle_eq = 80.*pi/180.;
h.t[bottom] = contact_angle(dynamic_angle);
#endif
#if SLIP
u.t[bottom] = robin(0., eps);
#else
u.t[bottom] = dirichlet(0.);
#endif
#endif

u.n[top]  = neumann(0.);
u.t[top] = neumann(0.);

u.n[right]  = neumann(0.);
u.t[right] = neumann(0.);

u.n[left]  = neumann(0.);
u.t[left] = neumann(0.);

u.n[bottom]  = dirichlet(0.);

int main ()
{
	#if TOY
	origin(0., eps);
	#else
	origin(0., 0.);
	#endif
  init_grid (128);

  f.sigma = sig;
  f.height = h;  
  rho1 = rho_1; rho2 = rho_2;
  mu1 = mu_1; mu2 = mu_2;

	#if !JVIEW
	for (Ca = 0.04; Ca <= 0.081; Ca += 0.02){
		for (LEVEL = 11; LEVEL <= 13; LEVEL += 1){
			for (eps = 0.01; eps <= 0.051; eps *= 5.){
				for (angle_eq = 70.*pi/180.; angle_eq <= 111.*pi/180.; angle_eq += 20.*pi/180.){
					lmbda = eps;
					size (L);
					run();
				}
			}
		}
	}
	#else 
	lmbda = eps;
  size (L); 
	run();
	#endif
}

event init (t = 0) {
	fraction (f, - (sq(x - L/2.) + sq(y) - sq(l_c/2.)));
	double delta = L/pow(2,LEVEL);
	fprintf(ferr, "# Re = %g  Ca = %g  GNBC = %d  SLIP = %d TOY = %d LEVEL = %d\n", Re, Ca, GNBC, SLIP, TOY, LEVEL);
	fprintf(ferr, "# La \t%e\n",sig*rho_1*l_c/sq(mu_1));
	fprintf(ferr, "# Delta = %g  eps = %g  eps/Delta = %g  angle_eq = %lf\n", delta, eps, eps/delta, 180.*angle_eq/pi);
	fprintf(ferr, "--------------------------------------------------\n");
	boundary ({f});
}

// event acceleration(i++){
// 	face vector av = a;
// 	foreach_face(y)
//     av.y[] += g;
// }

event adapt(i++){
	adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err},  LEVEL);
}

event extract (i++){
	char name[80];
	#if JVIEW
	sprintf (name, "JVIEW_data");
	#else
	sprintf (name, "GNBC%d_SLIP%d_Ca%g_LEVEL%d_eps%g_angle%g", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi);
	#endif
	static FILE * fp = fopen(name, "w");
	double angle_extracted = 0., angle_above = 0., angle_extrapolated = 0., position = 0., maxYS = 1e-300;
	double curv = 0., Ca_loc = 0.;
	scalar kappa[];

	curvature (f,kappa);

	#if NEWTOY
	foreach(reduction(max:angle_extrapolated) reduction(max:angle_above)){
		if ((y <= (eps + Delta)) & (y > (eps)) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
				double tmp3 = (3./(sqrt(2.)*2.0))*1.0*mu_1*u.x[0,0]/sig;
				double tmp2 = acos(cos(angle_eq) - tmp3);
				double tmp = pow(((h.x[0,1] - h.x[])/1.0), 2);
				angle_extrapolated = tmp2 + 1.5*Delta*kappa[]*sqrt(1 + tmp)/sin(tmp2);
		}
	}
	dynamic_angle = angle_extrapolated;
	foreach_boundary(bottom reduction(max:position) reduction(max:angle_extracted) reduction(max:curv) reduction(max:Ca_loc)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.)){
			position = x + Delta*height(h.x[]);
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
			curv = kappa[];
			Ca_loc = mu_1*u.x[]/sig;
		}
	}
	#else
	foreach(reduction(max:angle_extrapolated) reduction(max:angle_above)){
		if ((y <= (2.*Delta)) & (y > (Delta)) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
				coord n = {0};
				n = interface_normal (point,f);
				angle_above = atan2(n.x, n.y);
				double tmp = pow(((h.x[0,1] - h.x[])/1.0), 2);
				angle_extrapolated = angle_above + 1.5*Delta*kappa[]*sqrt(1 + tmp)/sin(angle_above);
		}
	}

	foreach_boundary(bottom reduction(max:position) reduction(max:angle_extracted) reduction(max:curv) reduction(max:Ca_loc)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.)){
			position = x + Delta*height(h.x[]);
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
			curv = kappa[];
			Ca_loc = mu_1*u.x[]/sig;
		}
	}
	#endif

	#if GNBC
	foreach_boundary(bottom reduction(max:maxYS)){
		double relative_position = y - position;
		gnbc[] = V_s + (sig/mu_1)*Young_stress(relative_position, eps, angle_extracted, angle_eq);
		if (fabs(maxYS) < fabs(gnbc[]))
			maxYS = gnbc[];
	}
	dynamic_angle = angle_extrapolated;
	#endif

	#if DYN
	dynamic_angle = acos(cos(angle_eq) - (3./(sqrt(2.)*2.0))*Ca_loc*alpha);
	#endif

	fflush(fp);
	fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf %lf\n", t/TCHAR, position, 180.*angle_above/pi, 180.*angle_extrapolated/pi, 180.*angle_extracted/pi, Ca_loc, curv, maxYS);
}

event final (t = TEND){
	char name[80], name2[80], name3[80], name4[80];
	#if JVIEW
	sprintf (name, "GNBC%d_SLIP%d_pressure_JVIEW_%d", GNBC, SLIP, tid());
	sprintf (name2, "GNBC%d_SLIP%d_curvature_JVIEW_%d", GNBC, SLIP, tid());
	sprintf (name3, "GNBC%d_SLIP%d_facets_JVIEW_%d", GNBC, SLIP, tid());
	sprintf (name4, "GNBC%d_SLIP%d_dudy_JVIEW_%d", GNBC, SLIP, tid());
	#else
	sprintf (name, "GNBC%d_SLIP%d_pressure_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	sprintf (name2, "GNBC%d_SLIP%d_curvature_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	sprintf (name3, "GNBC%d_SLIP%d_facets_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	sprintf (name4, "GNBC%d_SLIP%d_dudy_Ca%g_LEVEL%d_eps%g_angle%g_%d", GNBC, SLIP, Ca, LEVEL, eps, angle_eq*180./pi, tid());
	#endif
	FILE * fp = fopen(name, "w");
	FILE * fp2 = fopen(name2, "w");
	FILE * fp3 = fopen(name3, "w");
	FILE * fp4 = fopen(name4, "w");
	scalar kappa[];
	double position = 0.;

	curvature (f,kappa);

	foreach_boundary(bottom reduction(max:position)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			position = x + Delta*height(h.x[]);
		}
		double dudy = (u.x[0,1] - u.x[])/Delta;
		fprintf(fp4,"%lf %lf\n", x, dudy);
	}

	foreach(){
		fprintf(fp,"%lf %lf %lf\n", x, y, p[]);
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.)){
			double xcl = x + Delta*height(h.x[]);
			double r = sqrt(pow(x,2) + pow(xcl, 2));
			coord n = {0};
			n = interface_normal (point,f);
			double theta = atan2(n.x, n.y);
			fprintf(fp2,"%lf %lf %lf\n", r, kappa[], 180.*theta/pi);
		}
	}

	output_facets (f, fp3);

	fclose(fp);
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
}