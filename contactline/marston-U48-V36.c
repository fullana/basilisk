#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "contact.h"
#include "curvature.h"
#include "utils.h"
#include "navier-stokes/perfs.h"
#include "adapt2.h"

//Dimensionless quantity


//System properties (SI)
#define SIGMA 67e-03

#define rho_liq 1000
#define mu_liq 117e-03

#define rho_gas 1.2
#define mu_gas 1.8e-05

#define gravi 9.81
#define l_slip 450e-09
#define h_curt 2.6e-02
#define w_curt 1e-03

#define U1 0.48
#define V1 0.36
#define h_inf (Q1/U1)
#define Q1 (w_curt*V1)
#define S1 ((mu_liq*U1*U1)/Q1)

//System characterics (dimensionless)
#define SIGMA_nd (SIGMA/(S1*h_inf*U1))

#define rho_liq_nd (rho_liq/(h_inf*S1/Q1))
#define mu_liq_nd (mu_liq/(h_inf*S1))

#define rho_gas_nd (rho_gas/(h_inf*S1/Q1))
#define mu_gas_nd (mu_gas/(h_inf*S1))

#define gravi_nd (gravi/(pow(U1,3)/Q1))
#define l_slip_nd (l_slip/h_inf)
#define h_curt_nd (h_curt/h_inf)
#define w_curt_nd (w_curt/h_inf)

#define U_nd (U1/U1)
#define V_nd (V1/U1)

#define TEND 30000.
#define LEVEL 17
#define uemax 0.001

int augm;
scalar s[], s2[], s3[];
vector h[];


double semicircle(double x, double y, double radius){
	double a = sq((x))+sq((y))-sq((radius));
	double b = min(-(a), -(y));
	return b;
}
double geomf(double x, double y, double center, double radius){
	double a = min((radius) - ((x) + (center)), (radius) + ((x) + (center)));
	double b = min((a), (y));
	//double c = max(b, semicircle(x + center, y, radius));
	return b;
}
//Boundary conditions

u.n[right] = u.n[] > 0 ? neumann(0) : 0;
p[right] = dirichlet(0);
pf[right] = dirichlet(0); 

u.n[left] = u.n[] > 0 ? neumann(0) : 0;
p[left] = dirichlet(0);
pf[left] = dirichlet(0);

u.t[bottom] = robin(U_nd,l_slip_nd);
u.n[bottom] = dirichlet(0);

u.n[top] = dirichlet( ((x > - (w_curt_nd/2.)) & (x < (w_curt_nd/2.))) ? -V_nd : 0) ;
u.t[top] = dirichlet(0) ;

h.t[bottom] = contact_angle (67.*pi/180.);
f[top] = ( ((x > - (w_curt_nd/2.)) & (x < (w_curt_nd/2.))) ? 1 : 0);

	
int main() {
  init_grid (1<<8);
  origin (-h_curt_nd/2., -h_curt_nd/2.);
  size(h_curt_nd);
  refine (level < 8);
  f.sigma = SIGMA_nd;
  f.height = h;
  rho1 = rho_liq_nd; rho2 = rho_gas_nd;
  mu1 = mu_liq_nd; mu2 = mu_gas_nd;
  run();
  
} 

event init (t = 0) {
	if (!restore (file = "restart")){
	fraction (f, geomf(x, y - (0.9*h_curt_nd/2.), 0., w_curt_nd/2.));
	fprintf(ferr,"Sliplength (SI) \t%e\n",l_slip);
	fprintf(ferr,"Height (SI) \t%e\n",h_curt);
	fprintf(ferr,"Grid size per Sliplength \t%e\n",l_slip_nd/(h_curt_nd/pow(2.,LEVEL)));
	}
	
	fprintf(ferr,"LEVEL %d\n",LEVEL);
	fprintf(ferr,"Grid size per Sliplength %g\n",l_slip_nd/(h_curt_nd/pow(2.,LEVEL)));
    
	boundary({f});
	dump("dump-initial")
}

event acceleration (i++){
	face vector av = a;
	foreach_face(y)
    	av.y[] -= gravi_nd;
}

event adapt (i++) {
	adapt_wavelet ({f,u}, (double[]){1e-3,uemax,uemax,uemax}, LEVEL);
	
}

event snapshot (t = 0; t+=TEND/60.; t <= TEND){
  char name[80];
  sprintf (name, "s-%g", t);
  scalar pid[];
  scalar omega[];
  vorticity (u, omega);
  foreach(){
      pid[] = fmod(pid()*(npe() + 37), npe());
      omega[] *= 0.1;
    }
  boundary ({pid,omega});
  dump (name);
}

event outfacets(t=0.; t+=TEND/60.; t<= TEND){
	char name3[80], name4[80], name5[80];
	
	sprintf (name3, "c-%g-%d",t, tid());
	sprintf (name4, "f-%g-%d",t, tid());
	sprintf (name5, "g-%g-%d",t, tid());
	FILE * fp2 = fopen (name3, "w");
	FILE * fp3 = fopen (name4, "w");
	FILE * fp4 = fopen (name5, "w");
	
	vector h2[];
    scalar kappa[];
    heights (f, h2);
    curvature (f, kappa);
	
    foreach() {
          if (kappa[] != nodata)
        	  fprintf (fp2, "%g %g %g kappa\n", x, y, kappa[]);
    }
	
    output_facets (f, fp3);
    output_cells(fp4);
	
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
}


event movie2 (t += TEND/2400.) {
  clear();
  box();
  cells();
  draw_vof("f",lc = {1.,0.,1.} , lw = 2);
  save ("mesh.mp4"); 
}

event movie3 (t += TEND/2400.) {
  scalar omega[];
  vorticity (u, omega);
  foreach()
    omega[] *= 0.1;
  clear();
  box();
  squares("omega");
  draw_vof("f",lc = {1.,0.,1.} , lw = 2);
  save ("vorticity.mp4"); 
}
