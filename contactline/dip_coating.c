#include "navier-stokes/centered.h"
//#include "grid/quadtree.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "contact.h"
#include "curvature.h"
#include "utils.h"
#include "navier-stokes/perfs.h"


#define l_slip_nd 1e-05
#define U_nd 5
#define LEVEL 8
#define uemax 1e-3

vector h[];

u.t[bottom] = robin(U_nd,l_slip_nd);
u.n[bottom] = dirichlet(0);
h.t[bottom] = contact_angle(90.*pi/180.);

int main() {
  /**
  The domain is 2x2 to minimise finite-size effects. The viscosity is
  constant. The acceleration of gravity is 0. */
    L0 = 2.;
    Y0 = -L0/2.;
    rho1 = 1000, rho2 = 1;
    mu1 = 1e-03;
    mu2 = 1e-5;
    f.sigma = 1;
//  TOLERANCE = 1e-4;
    origin (-L0/2., -L0/2.);
    run();
}


//A wavy interface
event init (t = 0) {
  //fraction (f, x - 0.01*cos (2.*pi*y));
    
  fraction (f, x);

}

event adapt (i++) {
    adapt_wavelet ({f,u}, (double[]){1e-3,uemax,uemax,uemax}, LEVEL);
    }

//dump updated snapshot
event snapshot (t = 10){
  char name[80];
  sprintf (name, "snap");
  dump (name);
}
