#define TOY 1
#define JVIEW 0
#define SINK 1
#define ADAPT 1

#include "navier-stokes/centered.h"
#include "contact.h"
#include "vof.h"
#include "tension.h"
#if JVIEW
#include "display.h"
#endif

#define RADIUS (0.5)
#define DIAMETER (2.*RADIUS)
#define MU sqrt(DIAMETER/LAPLACE)
#define TMAX 1.*(sq(DIAMETER)/MU)


#define initial_position sqrt(pow(RADIUS, 2) - pow(eps, 2))

scalar f[], * interfaces = {f};
double LAPLACE = 1e1;
double eps = 0.02;
double angle0 = 0., position0 = 0.;
double x1 = 0., x2 = 0.;

int LEVEL = 8;
vector h[];
double theta0 = 70.;
double theta_PF = 70.;

double mass_loss = 0., delta_mass_loss = 0., sum_mass_loss = 0.;
double previous_x1 = 0., previous_x = 0.;

h.t[bottom] = contact_angle (theta0*pi/180.);

#if SINK
double sink = 0.;
u.n[bottom] = dirichlet(sink);
#endif

double ORIGIN = 0.02;

double polygonArea(double X[], double Y[], int n)
{
    // Initialize area
    double area = 0.0;
 
    // Calculate value of shoelace formula
    int j = n - 1;
    for (int i = 0; i < n; i++)
    {
        area += (X[j] + X[i]) * (Y[j] - Y[i]);
        j = i;  // j is previous vertex to i
    }
 
    // Return absolute value
    return fabs(area / 2.0);
}

int main()
{ 
    size (2);
    #if TOY
    origin (-1., ORIGIN);
    #else
    origin (-1., 0.);
    #endif
    // for (LEVEL = 5; LEVEL <= 7; LEVEL += 1){
        // for (theta0 = 60.; theta0 <= 85.; theta0+=10.){
            // for (eps = 0.0; eps <= 0.11; eps+=0.05){
                N = pow(2.,LEVEL);
        
                const face vector muc[] = {MU,MU};
                mu = muc;

                f.height = h;
                f.sigma = 1.;
                run();	
            // }
        // }
    // }
}


event init (t = 0)
{
  fraction (f, -(sq(x) + sq(y) - sq(RADIUS)));
//   fraction (f, -(sq(x) + sq(y) - sq(RADIUS)));
    boundary ({f});
  }


#if ADAPT
event adapt(i++){
	adapt_wavelet ({f,u}, (double[]){0.,1e-3,1e-3,1e-3},  LEVEL);
}
#endif

event logfile (i++; t <= TMAX)
{
	char name[80];
	sprintf (name, "SINK%d_TOY%d_eps%g_theta%g_LEVEL%d", SINK, TOY, eps, theta0, LEVEL);
	static FILE * fp = fopen(name, "w");
	scalar un[];
    double position_x = 0., position_y = 0, angle_extracted = 0.;
    double s = 0.;

	foreach()
	    un[] = norm(u);
    
    double angle_eps_theta = (theta_PF/90.)*pi/2. - atan(eps/initial_position);
    #if TOY
    if (i == 1){
        foreach_boundary(bottom){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x >= 0.)){
			position0 = x + Delta*height(h.x[]);
			coord n = {0};
			n = interface_normal (point,f);
			angle0 = atan2(n.x, n.y);
		}
	}
    x1 = position0 + eps*tan(pi/2. - angle0);
    previous_x = position0;
    previous_x1 = x1;
    }
    foreach_boundary(bottom){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x >= 0.)){
			position_x = x + Delta*height(h.x[]);
            position_y = y;
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
		}
	}
    foreach(){
        s += f[]*sq(Delta);
    }
    x2 = position_x + eps*tan(pi/2. - angle_extracted);
    double X[] = {position0, position_x, x2, x1};
    double Y[] = {ORIGIN, ORIGIN, 0., 0.};

    if (i > 2)
        mass_loss = polygonArea(X, Y, 4);

    double X2[] = {previous_x, position_x, x2, previous_x1};
    double Y2[] = {ORIGIN, ORIGIN, 0., 0.};

    if (i > 2)
        delta_mass_loss = polygonArea(X2, Y2, 4);

    previous_x1 = x2;
    previous_x = position_x;

    if (i > 2)
        sum_mass_loss += delta_mass_loss;

    #if SINK
        sink = -2.*delta_mass_loss/dt;
    #endif
    double angle_eps_theta_v = (theta_PF/90.)*pi/2. - atan(eps/position_x);
    theta0 = 180.*angle_eps_theta_v/pi;
    // fprintf (stderr, "%g %g %g %g %g %g\n", position0, position_x, x2, x1, mass_loss, delta_mass_loss);
    fprintf (fp, "%g %g %g %g %g %g %g %g %g %g %g %g\n", MU*t/sq(DIAMETER), normf(un).max*MU, initial_position, position_x, position_y, 180.*angle_extracted/pi, 180.*angle_eps_theta_v/pi, 180.*angle_eps_theta/pi, s, mass_loss, delta_mass_loss, sum_mass_loss);
    #else
    foreach(){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x >= 0.) & (y >= eps) & (y <= eps + Delta)){
			position_x = x + Delta*height(h.x[]);
            position_y = y;
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
		}
        if (y > eps){
            s += f[]*sq(Delta);
        }
	}
    double angle_eps_theta_v = (theta_PF/90.)*pi/2. - atan(position_y/position_x);
    fprintf (fp, "%g %g %g %g %g %g %g %g %g\n", MU*t/sq(DIAMETER), normf(un).max*MU, initial_position, position_x, position_y, 180.*angle_extracted/pi, 180.*angle_eps_theta_v/pi, 180.*angle_eps_theta/pi, s);

    #endif
	fflush(fp);
}

event save_facets(t = TMAX){
    char name[80];
    sprintf (name, "facets_SINK%d_TOY%d_eps%g_theta%g_LEVEL%d", SINK, TOY, eps, theta_PF, LEVEL);
	static FILE * fp = fopen(name, "w");
    output_facets (f, fp);
}