#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "view.h"

#define u_wall 1
#define sol(y) (((-2.*u_wall)/(1.+2.*lmbda))*y)

double tmp = 0.125;
double clb1 = 0;

//Cosine bell shape (Tomas EPJST)
double phi(double a, double b, double pos){
        double c = pow(((1.+cos((pi*(a - (pos)))/b))/2),2);
            return c;
}

face vector muv[];
double lmbda = 10;

int main() {
  L0 = 1.;
  origin (-L0/2, -L0/2.);
  mu = muv;
  periodic(right);
    for (N = 8; N <= 32; N *= 2){
        for (lmbda = 10; lmbda >= 0.1; lmbda *= 0.1)
            run();
    }
}


//u.t[bottom] = dirichlet(0);
//Slip
//u.t[bottom] = robin(u_wall,lmbda)
//Localised Slip
u.t[bottom] = (((x < (clb1 + tmp)) & (x > (clb1 - tmp))) ? robin(u_wall , phi(x,tmp,clb1)*lmbda) : dirichlet(u_wall));
u.n[bottom] = dirichlet(0);


u.t[top] = dirichlet(0);
u.n[top] = dirichlet(0);

event init(t =0){
    foreach()
      u.x[] = 1;
}

event properties (i++)
{
  foreach_face()
    muv.x[] = fm.x[]*1./10.;
}

event profile (t = 60) {
  char name[80];
  sprintf (name, "Wall-velocity-%d-%g-%g.txt", N,lmbda,tmp);
  FILE * fp = fopen (name, "w");
    foreach_boundary(bottom){
        fprintf (fp, "%g %g %g\n", x, u.x[], Delta);
        
    }
}
