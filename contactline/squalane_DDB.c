#include "display.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"

#define MU 0.1
#define SIGMA 1.
#define radius0 0.5
#define theta_s 60.
#define DDB 1
#define ADAPT 0

vector h[];

double ORI (double nx, double ny){
	double angle = 0.0;
	if ((nx > 0) & (ny >= 0))
		angle = atan(ny/nx);
	if ((nx > 0) & (ny < 0))
		angle = atan(ny/nx) + 2.*pi;
	if (nx < 0)
		angle = atan(ny/nx) + pi;
	if ((nx == 0) & (ny > 0))
		angle = pi/2.;
	if ((nx == 0) & (ny < 0))
		angle = 3.*pi/2.;
	return angle;
} 

double lambda = 0.;
double lambda2 = 0.;

int main() {
  N = 64;
  L0 = 1.;
  origin (-L0/2, -L0/2.);

  f.sigma = SIGMA;
  f.height = h;

  rho1 = 1.; rho2 = 1.;
  mu1 = MU; mu2 = MU;  
  #if DDB
	for (lambda = 0.; lambda <= 0.1; lambda += 0.1){
		for (lambda2 = 0.; lambda2 <= 0.01; lambda2 += 0.01){
  		lmbda = lambda;
  		lmbda2 = lambda2;	
  		run();
		}
	} 
	#else
  		lmbda = lambda;
  		lmbda2 = lambda2;	
  		run();
	#endif
}

u.n[bottom]  = dirichlet(0);
u.t[bottom] = DDBb(0.,lambda,lambda2);

u.n[top]  = dirichlet(0);
u.t[top]  = dirichlet(0);

u.n[right]  = neumann(0);
u.t[right]  = neumann(0);

h.t[bottom] = contact_angle (theta_s*pi/180);


event init (t = 0) {
  if (!restore (file = "restart")) { 
	fraction (f, -(sq((x+0.5))+sq((y+0.5))-sq((0.5))));
 	}
 	boundary ({f});
}

#if ADAPT
event adapt (i++){
    adapt_wavelet ({f,u}, (double[]){0.00001,0.00001,0.00001,0.00001},  8);
}
#endif

event impose (i++){
	char name[80];
	sprintf (name, "log-%g-%g", lambda, lambda2);
	static FILE * fp = fopen(name, "w");
	double radius = 0.0;
	double Ca = 0.0;
	double angle = 0.0;
	double dudy = 0.0;
	double d2udy = 0.0;
	foreach_boundary(bottom){
		if ((x > 0.) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			radius = x + Delta*height(h.x[]);
			Ca = MU*u.x[]/SIGMA;
			coord n = {0};
			n = interface_normal (point,f);
			angle = pi/2 - ORI(n.x,n.y);
			dudy = (u.x[0,1] - u.x[])/Delta;
			d2udy = (u.x[0,-1] - 2.*u.x[] + u.x[0,1])/pow(Delta,2);
		}
	}
	fprintf(fp,"%lf %lf %lf %lf %lf %lf\n",t, fabs(0.5+radius), Ca, 180.*angle/pi, dudy, d2udy);
    fflush(fp);
}

event snapshot (t = 10){
	double tmp, tmp2;
	char name[80];
	char name2[80];
	sprintf (name2, "dudy-%g-%g", lambda, lambda2);
	FILE * fp2 = fopen(name2, "w");
	foreach_boundary(bottom){
		point = locate(x,y);
		tmp = u.x[];
		point = locate(x,y+Delta);
		tmp2 = fabs(u.x[]-tmp);
		fprintf(fp2,"%lf %lf %lf\n",x, tmp2, tmp);
	}
	sprintf (name, "facets-%g-%g", lambda, lambda2);
	FILE * fp3 = fopen(name, "w");
	output_facets (f, fp3);
	fclose(fp2);
	fclose(fp3);
}
