#include "navier-stokes/centered.h"

#define u_plate 0.
#define PROFILES 0
#define DDB 1

double lambda = 0.;
double lambda2 = 0.;

double sol(double y, double l, double l2){
return 0.5*(0.25 + l + 2.*l2 - y*y);
}

int main() {
  origin (0, -0.5);
  stokes = true;
  TOLERANCE = 1e-5;
  #if PROFILES == 1
  N = 128;
	for (lambda = 0.; lambda <= 0.2; lambda += 0.1){
		for (lambda2 = 0.; lambda2 <= 0.2; lambda2 += 0.1){
		  lmbda = lambda;
		  lmbda2 = lambda2;	
			run();
		}
	}
	#else
	for (N = 8; N <= 128; N*=2){
		#if DDB == 1
		lambda = 1.*(1./N);
		lambda2 = 1.*pow((1./N), 2);
		#endif
		lmbda = lambda;
		lmbda2 = lambda2;	
		run();
  }
  #endif
}

face vector muv[];

u.t[bottom] = DDBb(-u_plate,lambda,lambda2);
u.n[bottom] = dirichlet(0);

u.t[top] = DDBt(u_plate,lambda,lambda2);
u.n[top] = dirichlet(0);

u.n[left] = neumann(0);
p[left] = dirichlet(y + 0.5);
u.n[right] = neumann(0);
p[right] = dirichlet(y + 0.5);

scalar un[];

event init (t = 0) {
  const face vector g[] = {1.,1.};
  a = g;
  const face vector muc[] = {1.,1.};
  mu = muc;
  foreach()
    un[] = u.x[];
}

event logfile (t += 0.1; i <= 100) {
  double du = change (u.x, un);
  if (i > 0 && du < 1e-6)
    return 1;
}

#if PROFILES == 1
event profile (t = end) {
  char name[80];
  sprintf (name, "velo_%g_%g", lambda, lambda2);
  FILE * fp = fopen (name, "w");
  foreach()
    fprintf (fp, "%g %g %g %g %g\n", x, y, u.x[], u.y[], p[]);
}
#else
event error_analytical (t = end) {
  scalar e[];
  foreach()
    e[] = u.x[] - sol(y, lambda, lambda2);
  norm n = normf (e);
  #if DDB == 1
  fprintf (stderr, "%d %g %g %g %g %g %g %g\n", N, n.avg, n.rms, n.max, lambda, lambda2, lambda/(1./N), lambda2/(1./N));
  #else
  fprintf (stderr, "%d %g %g %g\n", N, n.avg, n.rms, n.max);
  #endif
}
#endif
