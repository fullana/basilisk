//#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "view.h"

#define lambda 0.
#define lambda2 0.
#define u_plate 0.
#define sol(y) u_plate*y
face vector muv[];

//u.t[bottom] = dirichlet(-u_plate);
u.t[bottom] = DDBb(-u_plate,lambda,lambda2);
u.n[bottom] = dirichlet(0);

//u.t[top] = dirichlet(u_plate);
u.t[top] = DDBt(u_plate,lambda,lambda2);
u.n[top] = dirichlet(0);

u.n[left] = neumann(0);
p[left] = dirichlet(fabs(y));
u.n[right] = neumann(0);
p[right] = dirichlet(fabs(y));

int main() {
  L0 = 2.;
  origin (-L0/2, -L0/2.);
  N = 32;
  mu = muv;
  lmbda = lambda;
  lmbda2 = lambda2;
  TOLERANCE = 1e-5;
  //stokes = true;
  //periodic(right);
  //DT = 0.1;
  //for (N = 2; N <= 16; N *= 2)
  run();
}



event init(t =0){
    foreach()
      u.x[] = sol(y);
}

event properties (i++)
{
  foreach_face()
    muv.x[] = 1.;
}

event profile (t = 10) {
  char name[80];
  sprintf (name, "velo");
  FILE * fp = fopen (name, "w");
  foreach()
    fprintf (fp, "%g %g %g %g\n", x, y, u.x[], sol(y));
}

event errorsol (t = 10) {
  static FILE * fp1 = fopen ("error", "w");
  double e = 0;
  double tmp = 0;
  foreach(){
    e += fabs(u.x[] - sol(y)) * sq(Delta);
  	tmp = Delta;
	}
  fprintf (fp1, "%g %g %g %d %g\n", t, lambda/tmp, tmp, N, e);
}
