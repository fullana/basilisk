#include "view.h"
#include "display.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"
#include "display.h"

#define Oh 0.21

#define rho_liq 809.
#define mu_liq 0.034

#define rho_gas 1.292
#define mu_gas 1.85E-05

#define sig 0.032
#define R0 1e-3


#define D_MU (Oh*sqrt(rho_liq*sig*R0))
#define D_RHO (pow(mu_liq,2)/(sig*R0*pow(Oh,2)))
#define D_R0 (pow(mu_liq,2)/(sig*rho_liq*pow(Oh,2)))
#define D_SIG (pow(mu_liq,2)/(R0*rho_liq*pow(Oh,2)))


#define MU1 (mu_liq/D_MU)
#define MU2 (mu_gas/D_MU)

#define RHO1 (rho_liq/D_RHO)
#define RHO2 (rho_gas/D_RHO)

#define radius0 (R0/D_R0)

#define SIGMA (sig/D_SIG)

//#define lambda 1.0/512.0
#define theta_s 61.5*pi/180.

scalar f2[];
vector h[];
double dtmp;

double lambda = 1e-01;
double theta_d = 90.*pi/180.;
double L2 = 1e-06;
double lambda2 = 1e-9;
int j;
double g_0(double x){
        double a = (1./9.)*pow((x),3) - 0.00183985*pow((x),4.5)+1.845823*(1e-06)*pow((x),12.258487);
        return a;
}
double g_1(double x){
        double a = (1./4.33)*((9.)*pow((x),1./3.) + 0.0727387*(x) -0.0515388*pow((x),2) + 0.00341336*pow((x),3));
        return a;
}
double polyCox(double xCa, double b, double c, double statict){
		double a = g_1(g_0(statict) + xCa*log(b/c));
        return a;
}
u.n[bottom]  = dirichlet(0);
//u.t[bottom]  = robin(0,lambda);
u.t[bottom]  = dirichlet(0);
/*
u.n[left]  = dirichlet(0);
u.t[left]  = dirichlet(0);
*/
u.n[top]  = dirichlet(0);
u.t[top]  = dirichlet(0);

u.n[right]  = dirichlet(0);
u.t[right]  = dirichlet(0);

h.t[bottom] = contact_angle (theta_d);

//f[left] = neumann(0);
int main() {
      N = 256;
      L0 = 3.*radius0;
      origin (-L0/2, -L0/2.);
  
      //DT = 5e-5;
	  
      f.sigma = SIGMA;
      f.height = h;
    
      lmbda = lambda;	
      rho1 = RHO1; rho2 = RHO2;
      mu1 = MU1; mu2 = MU2;  
    	run();
  
}

event init (t = 0) {
  if (!restore (file = "restart")) { 
	fraction (f2, sq((x+L0/2.))+sq((y+(1.05*L0/6.)))-sq((radius0)));
	foreach(){
		f[] = 1.0 - f2[];
	}
 	}
	char name[80];
	sprintf (name, "init_snap-%d", N);
    dump(name);
}

event adapt (i++){
    adapt_wavelet ({f,u}, (double[]){0.00001,0.01,0.01,0.01},  7);
}
event impose (i = 20; i++){

	char name[80];
	sprintf (name, "log1");
	static FILE * fp = fopen(name, "w");
	double radius = 0.0;
	double Ca = 0.0;

	foreach_boundary(bottom){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			radius = x + Delta*height(h.x[]);
			Ca = MU1*(u.x[-1,0]+2.*u.x[]+u.x[1,0])/4.*SIGMA;
		}
	}
	//theta_d = acos(cos(theta_s)+5.63*Ca*log(0.02/lambda));
	theta_d = polyCox(Ca, L2, lambda2, theta_s);
	fprintf(fp,"%lf %lf %lf %lf\n",t, fabs(radius), Ca, (180./pi)*(theta_d));
    fflush(fp);
	
}

event snapshot (t = 1){
	double tmp, tmp2;
	char name[80];
	char name2[80];
	sprintf (name2, "dudy");
	static FILE * fp2 = fopen(name2, "w");
	foreach_boundary(bottom){
		point = locate(x,y);
		tmp = u.x[];
		point = locate(x,y+Delta);
		tmp2 = fabs(u.x[]-tmp);
		fprintf(fp2,"%lf %lf %lf\n",x, tmp2, tmp);
	}
	sprintf (name, "snap");
    dump(name);
}
