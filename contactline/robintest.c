#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "view.h"

#define u_wall 1.
#define sol(y) (((-2.*u_wall)/(1.+2.*lmbda))*y)

face vector muv[];
double lmbda = 0.1;

int main() {
  L0 = 1.;
  origin (-L0/2, -L0/2.);
  mu = muv;
  periodic(right);
  for (N = 2; N <= 128; N *= 2)
	 run();
}


u.t[bottom] = robin(u_wall,lmbda);
u.n[bottom] = dirichlet(0);


u.t[top] = robin(-u_wall,lmbda);
u.n[top] = dirichlet(0);

event init(t =0){
    foreach()
      u.x[] = sol(y);
}

event properties (i++)
{
  foreach_face()
    muv.x[] = fm.x[]*1./10.;
}

event profile (t = 10) {
  char name[80];
  sprintf (name, "profile-%g", lmbda);
  FILE * fp = fopen (name, "w");
  foreach()
    fprintf (fp, "%g %g %g\n", x, y, u.x[]);
}

event errorsol (t=10) {
  static FILE * fp1 = fopen ("error", "w");
  double e = 0;
  double tmp = 0;
  foreach(){
    e += fabs(u.x[] - sol(y)) * sq(Delta);
  	tmp = Delta;
	}
  fprintf (fp1, "%g %g %g %d %g\n", t, lmbda, tmp, N, e);
}
