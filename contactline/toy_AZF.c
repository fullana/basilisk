#define JVIEW 0
#define SLIP 1
#define GNBC 0
#define TOY 1
#define DYN 1
#define NEWTOY 0

#if JVIEW
#include "display.h"
#endif

#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "contact.h"

// Phase-Field dimensionless parameters 
#define Ca 0.0212
#define Re 3.978
#define Cn 0.01
// #define Pe 3.0
#define Xi_mu 1e-2
#define Xi_rho 1e-2

// Liquid-Gas physical parameters
#define muliq_SI 8.77e-4
#define rholiq_SI 986.
#define mugas_SI (muliq_SI*Xi_mu)
#define rhogas_SI (rholiq_SI*Xi_rho)
#define SIGMA_SI 5.78e-2 

// Corresponding VOF scalings
#define u_plates_SI (SIGMA_SI*Ca/muliq_SI)
#define _width_SI (Re*muliq_SI/(rholiq_SI*u_plates_SI))
#define _width 1.
#define u_plates 1.
#define MU_unit (rholiq_SI*u_plates_SI*_width_SI)
#define SIGMA_unit (rholiq_SI*_width_SI*u_plates_SI*u_plates_SI)
#define T_unit ((_width_SI*u_plates)/(_width*u_plates_SI))
#define L_unit (_width_SI/_width)

// VOF dimensionless parameters
#define muliq (muliq_SI/MU_unit) 
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit)  
#define rholiq 1.
#define rhogas Xi_rho
#define _widthliq (_width_SI/L_unit)
#define Oh_d (muliq_SI/sqrt(rholiq_SI*SIGMA_SI*_width_SI)) 
#define Oh (muliq/sqrt(rholiq*SIGMA*_widthliq)) 
#define La (1./pow(Oh, 2))

// Toy AFZ parameters
#define S sqrt((Ca*Cn)/Pe)
#define eps (S*sqrt(A/0.8679))
double A = 1.0;
double Pe = 3.0;
#define initial_position sqrt(pow(_widthliq/2., 2) - pow(eps, 2))
#define delta_angle atan(eps/initial_position)*180./pi

// Equilibrium angle
#define angle_eq (70.)*pi/180.
double angle_dyn = pi/2.;

// VOF numerical parameters
#define TEND 0.2
#define TOUT (TEND/20.)
#define LEVEL 8
#define u_err 1e-3
#define f_err 0.
vector h[];

// Boundary conditions
u.n[top]  = neumann(0.);
u.t[top] = neumann(0.);

u.n[right]  = neumann(0.);
u.t[right] = neumann(0.);

u.n[left]  = neumann(0.);
u.t[left] = neumann(0.);

u.n[bottom]  = dirichlet(0.); // Modify to allow penetration

#if GNBC
double bell(double y){
	return ((1. - pow(tanh(y/eps), 2))/eps);
}
double Young_stress(double y, double theta, double theta_eq){
	return bell(y)*(cos(theta_eq) - cos(theta));
}
scalar gnbc[];
h.t[bottom] = contact_angle(angle_dyn);
u.t[bottom] = robin(gnbc[], eps);
// u.t[bottom] = dirichlet(gnbc[]);
#else
#if DYN
h.t[bottom] = contact_angle(angle_dyn);
#else
h.t[bottom] = contact_angle(angle_eq);
#endif
#if SLIP
u.t[bottom] = robin(0., eps);
#else
u.t[bottom] = dirichlet(0.);
#endif
#endif

double arr[] = {0.01, 0.1, 1., 5., 10., 20., 50., 100.};
// double arr[] = {1., 5., 10., 20., 50., 100.};
int arrLen = sizeof arr / sizeof arr[0];

int main ()
{
	size(2.);
  init_grid (128);

	lmbda = eps;

  f.sigma = SIGMA;
  f.height = h;  
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;

	#if TOY
	// for (Pe = 1.; Pe <= 100.; Pe += 1.0){
		// origin(-_widthliq, eps);
		// run();
	// }
	for (int i = 0; i < arrLen; i++) {
        Pe =  arr[i];
		origin(-_widthliq, eps);
		run();
    }
	#else
	origin(-_widthliq, 0.); 
	run();
	#endif
}

event init (t = 0) {
	fraction (f, - (sq(x) + sq(y) - sq(_widthliq/2.)));
	double delta = 2.*_widthliq/pow(2,LEVEL);
	fprintf(ferr, "# JVIEW = %d  SLIP = %d  DYN = %d  TOY = %d  GNBC = %d\n", JVIEW, SLIP, DYN, TOY, GNBC);
	fprintf(ferr, "# Ca = %g  Re = %g  Cn = %g  Pe = %g  mu2 = %g  rho2 = %g  angle_eq = %g\n", Ca, Re, Cn, Pe, Xi_mu, Xi_rho, 180.*angle_eq/pi);
	fprintf(ferr, "# inital_position = %g\n", initial_position);
	fprintf(ferr, "# Oh_d = %g  Oh = %g  La = %g\n", Oh_d, Oh, La);
	fprintf(ferr, "# Delta = %g  eps = %g  eps/Delta = %g  a = %g\n", delta, eps, eps/delta, A);
	fprintf(ferr, "--------------------------------------------------\n");
	boundary ({f});
	// FILE * fp = fopen("angles", "w");
	// foreach(){
	// 	if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
	// 	coord n = {0};
	// 	n = interface_normal (point,f);
	// 	double angle_temp = atan2(n.x, n.y);
	// 	fprintf(fp,"%lf %lf\n", y, angle_temp*180./pi);
	// 	}
	// }
	// fclose(fp);
}

// event acceleration(i++){
// 	face vector av = a;
// 	foreach_face(y)
//     av.y[] += g;
// }

event adapt(i++){
	adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err},  LEVEL);
}

event extract (i++){
	char name[80];
	#if JVIEW
	sprintf (name, "JVIEW_data");
	#else
	sprintf (name, "SLIP%d_Ca%g_Re%g_angle%g_Pe%g_LEVEL%d", SLIP, Ca, Re, angle_eq*180./pi, Pe, LEVEL);
	#endif
	static FILE * fp = fopen(name, "w");
	double angle_extracted = 0., angle_above = 0., angle_extrapolated = 0., position = 0., position2 = 0., maxYS = 1e-300;
	double curv = 0., Ca_loc = 0.;
	scalar kappa[];

	curvature (f,kappa);

	#if NEWTOY
	foreach(reduction(max:angle_extrapolated) reduction(max:angle_above)){
		if ((y <= (eps + Delta)) & (y > (eps)) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
				double tmp3 = (3./(sqrt(2.)*2.0))*1.0*muliq*u.x[0,0]/SIGMA;
				double tmp2 = acos(cos(angle_eq) - tmp3);
				double tmp = pow(((h.x[0,1] - h.x[])/1.0), 2);
				angle_extrapolated = tmp2 + 1.5*eps*kappa[]*sqrt(1 + tmp)/sin(tmp2);
		}
	}
	angle_dyn = angle_extrapolated;
	foreach_boundary(bottom reduction(max:position) reduction(max:angle_extracted) reduction(max:curv) reduction(max:Ca_loc)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.)){
			position = x + Delta*height(h.x[]);
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
			curv = kappa[];
			Ca_loc = Ca*u.x[]; //muliq*u.x[]/SIGMA;
		}
	}
	#else
	foreach(reduction(max:angle_extrapolated) reduction(max:angle_above)){
		if ((y <= (2.*Delta)) & (y > (Delta)) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
				coord n = {0};
				n = interface_normal (point,f);
				angle_above = atan2(n.x, n.y);
				double tmp = pow(((h.x[0,1] - h.x[])/1.0), 2);
				angle_extrapolated = angle_above + 1.5*Delta*kappa[]*sqrt(1 + tmp)/sin(angle_above);
		}
	}

	foreach_boundary(bottom reduction(max:position) reduction(max:position2) reduction(max:angle_extracted) reduction(max:curv) reduction(max:Ca_loc)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x >= 0.)){
			position = x + Delta*height(h.x[]);
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
			curv = kappa[];
			Ca_loc = Ca*u.x[]; //muliq*u.x[]/SIGMA;
		}
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x < 0.)){
			position2 = x + Delta*height(h.x[]);
		}
	}
	#endif

	#if GNBC
	foreach_boundary(bottom reduction(max:maxYS)){
		if (x >= 0.){
		double relative_position = x - position;
		gnbc[] = (SIGMA/muliq)*Young_stress(relative_position, angle_extracted, angle_eq);
		if (fabs(maxYS) < fabs(gnbc[]))
			maxYS = gnbc[];
		}
		if (x < 0.){
		double relative_position = x - position2;
		gnbc[] = (SIGMA/muliq)*Young_stress(relative_position, angle_extracted, angle_eq);
		if (fabs(maxYS) < fabs(gnbc[]))
			maxYS = gnbc[];
		}
	}
	angle_dyn = angle_extrapolated;
	#endif

	#if DYN
	angle_dyn = acos(cos(angle_eq) - (3./(sqrt(2.)*2.0))*Ca_loc*A);
	#endif

	fflush(fp);
	fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf %lf\n", t, position, 180.*angle_above/pi, 180.*angle_extrapolated/pi, 180.*angle_extracted/pi, Ca_loc, curv, maxYS);
}

#if !JVIEW
event extract_facets (t += TOUT){
	char name[80];
	sprintf (name, "./facets/facets_t%g_SLIP%d_Ca%g_Re%g_angle%g_Pe%g_LEVEL%d_%d", t, SLIP, Ca, Re, angle_eq*180./pi, Pe, LEVEL, tid());
	FILE * fp = fopen(name, "w");
	output_facets (f, fp);
	fclose(fp);
}
#endif

event final (t = TEND){
	char name[80], name2[80], name3[80], name4[80];
	#if JVIEW
	sprintf (name, "TOY%d_Ca%g_Re%g_angle%g_eps%g_a%g_LEVEL%d_pressure_JVIEW_%d", TOY, Ca, Re, angle_eq*180./pi, eps, A, LEVEL, tid());
	sprintf (name2, "TOY%d_Ca%g_Re%g_angle%g_eps%g_a%g_LEVEL%d_curvature_JVIEW_%d", TOY, Ca, Re, angle_eq*180./pi, eps, A, LEVEL, tid());
	sprintf (name3, "TOY%d_Ca%g_Re%g_angle%g_eps%g_a%g_LEVEL%d_facets_JVIEW_%d", TOY, Ca, Re, angle_eq*180./pi, eps, A, LEVEL, tid());
	sprintf (name4, "TOY%d_Ca%g_Re%g_angle%g_eps%g_a%g_LEVEL%d_dudy_JVIEW_%d", TOY, Ca, Re, angle_eq*180./pi, eps, A, LEVEL, tid());
	#else
	sprintf (name, "SLIP%d_Ca%g_Re%g_angle%g_eps%g_Pe%g_LEVEL%d_pressure_%d", SLIP, Ca, Re, angle_eq*180./pi, eps, Pe, LEVEL, tid());
	sprintf (name2, "SLIP%d_Ca%g_Re%g_angle%g_eps%g_Pe%g_LEVEL%d_curvature_%d", SLIP, Ca, Re, angle_eq*180./pi, eps, Pe, LEVEL, tid());
	sprintf (name3, "SLIP%d_Ca%g_Re%g_angle%g_eps%g_Pe%g_LEVEL%d_facets_%d", SLIP, Ca, Re, angle_eq*180./pi, eps, Pe, LEVEL, tid());
	sprintf (name4, "SLIP%d_Ca%g_Re%g_angle%g_eps%g_Pe%g_LEVEL%d_dudy_%d", SLIP, Ca, Re, angle_eq*180./pi, eps, Pe, LEVEL, tid());
	#endif
	FILE * fp = fopen(name, "w");
	FILE * fp2 = fopen(name2, "w");
	FILE * fp3 = fopen(name3, "w");
	FILE * fp4 = fopen(name4, "w");
	scalar kappa[];
	double position = 0.;

	curvature (f,kappa);

	foreach_boundary(bottom reduction(max:position)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			position = x + Delta*height(h.x[]);
		}
		double dudy = (u.x[0,1] - u.x[])/Delta;
		fprintf(fp4,"%lf %lf\n", x, dudy);
	}

	foreach(){
		fprintf(fp,"%lf %lf %lf\n", x, y, p[]);
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.)){
			double xcl = x + Delta*height(h.x[]);
			double r = sqrt(pow(x,2) + pow(xcl, 2));
			coord n = {0};
			n = interface_normal (point,f);
			double theta = atan2(n.x, n.y);
			fprintf(fp2,"%lf %lf %lf\n", r, kappa[], 180.*theta/pi);
		}
	}

	output_facets (f, fp3);

	fclose(fp);
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
}