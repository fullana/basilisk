#include "display.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"

#define MU 0.25
#define SIGMA 7.5
#define radius0 0.5
#define lambda 1.0/256.0
#define theta_s 60.*pi/180.

double theta_d = 90.*pi/180.;
double L = 1e-06;
double lambda2 = 1e-9;

double g_0(double x){
        double a = (1./9.)*pow((x),3) - 0.00183985*pow((x),4.5)+1.845823*(1e-06)*pow((x),12.258487);
        return a;
}

double g_1(double x){
        double a = (1./4.33)*((9.)*pow((x),1./3.) + 0.0727387*(x) -0.0515388*pow((x),2) + 0.00341336*pow((x),3));
        return a;
}

double polyCox(double xCa, double b, double c, double statict){
		double a = g_1(g_0(statict) + xCa*log(b/c));
        return a;
}

vector h[];

u.n[bottom]  = dirichlet(0);
u.t[bottom]  = robin(0., lambda);

u.n[top]  = dirichlet(0);
u.t[top]  = dirichlet(0);

u.n[right]  = dirichlet(0);
u.t[right]  = dirichlet(0);

h.t[bottom] = contact_angle (theta_d);

int main() {
      N = 64;
      L0 = 1.;
      origin (-L0/2, -L0/2.);
  
      DT = 5e-5;
	  
      f.sigma = SIGMA;
      f.height = h;
    
      lmbda = lambda;	
      rho1 = 1.; rho2 = 1.;
      mu1 = MU; mu2 = MU;  
      run();
  
}

event init (t = 0) {
  if (!restore (file = "restart")) { 
	fraction (f, -(sq((x+0.5))+sq((y+0.5))-sq((0.5))) );
 	}
	char name[80];
	sprintf (name, "init_snap-%d", N);
    dump(name);
}

/*event adapt (i++){
    adapt_wavelet ({f,u}, (double[]){0.001,0.001,0.001,0.001},  8);
}*/

event impose (i++; t <= 4.){
	char name[80];
	sprintf (name, "log_dyn_poly1-%d", N);
	static FILE * fp = fopen(name, "w");
	double radius = 0.0;
	double Ca = 0.0;
	foreach_boundary(bottom){
		if ((x > 0.) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			radius = x + Delta*height(h.x[]);
			Ca = MU*u.x[]/SIGMA;;
		}
	}
	theta_d = polyCox(Ca, L, lambda2, theta_s);
	fprintf(fp,"%lf %lf %lf %lf\n",t, fabs(0.5+radius), Ca, (180./pi)*(pi-theta_d));
    	fflush(fp);
}
