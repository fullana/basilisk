






  typedef struct {
    double n;
    coord (* normal) (Point, vector);
  } NormNormal;
  struct { NormNormal x, y, z; } n;
  {
#line 270

    n.x.n = val(c,1,0,0) - val(c,-1,0,0), n.x.normal = normal_x;
#line 270

    n.y.n = val(c,0,1,0) - val(c,0,-1,0), n.y.normal = normal_y;}




  if (fabs(n.x.n) < fabs(n.y.n))
    swap (NormNormal, n.x, n.y);
#line 288 "/home/tf/basilisk/src/curvature.h"
  coord normal = {nodata, nodata, nodata};
  {
#line 289

    if (normal.x == nodata)
      normal = n.x.normal (point, h);
#line 289

    if (normal.y == nodata)
      normal = n.y.normal (point, h);}

  return normal;
