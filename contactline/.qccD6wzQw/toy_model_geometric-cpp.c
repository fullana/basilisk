@if _XOPEN_SOURCE < 700
  @undef _XOPEN_SOURCE
  @define _XOPEN_SOURCE 700
@endif
@if _GNU_SOURCE
@include <stdint.h>
@include <string.h>
@include <fenv.h>
@endif
#define _CATCH
#define dimension 2
#define BGHOSTS 2
#include "common.h"
#include "grid/quadtree.h"
@include "_boundarydecl.h"
#ifndef BASILISK_HEADER_0
#define BASILISK_HEADER_0
#line 1 "toy_model_geometric.c"
#define JVIEW 0
#define LOOP 1
#define ADAPT 1

#define TOY 1
#define SINK 1
#define SLIP 1
#define DYN 0

#if JVIEW
#include "display.h"
#endif

#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "contact.h"

// Phase-Field dimensionless parameters 
#define Ca 0.0212
#define Re 3.978
#define Cn 0.01
#define Xi_mu 1e-2
#define Xi_rho 1e-2

// Liquid-Gas physical parameters
#define muliq_SI 8.77e-4
#define rholiq_SI 986.
#define mugas_SI (muliq_SI*Xi_mu)
#define rhogas_SI (rholiq_SI*Xi_rho)
#define SIGMA_SI 5.78e-2 

// Corresponding VOF scalings
#define u_plates_SI (SIGMA_SI*Ca/muliq_SI)
#define _width_SI (Re*muliq_SI/(rholiq_SI*u_plates_SI))
#define _width 1.
#define u_plates 1.
#define MU_unit (rholiq_SI*u_plates_SI*_width_SI)
#define SIGMA_unit (rholiq_SI*_width_SI*u_plates_SI*u_plates_SI)
#define T_unit ((_width_SI*u_plates)/(_width*u_plates_SI))
#define L_unit (_width_SI/_width)

// VOF dimensionless parameters
#define muliq (muliq_SI/MU_unit) 
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit)  
#define rholiq 1.
#define rhogas Xi_rho
#define _widthliq (_width_SI/L_unit)
#define Oh_d (muliq_SI/sqrt(rholiq_SI*SIGMA_SI*_width_SI)) 
#define Oh (muliq/sqrt(rholiq*SIGMA*_widthliq)) 
#define La (1./pow(Oh, 2))

// Toy model parameters
#define S sqrt((Ca*Cn)/Pe)
#define eps S*sqrt(A/0.8679)
double A = 1.0;
double Pe = 1.0;

// Initial position of the interface
#define initial_position sqrt(pow(_widthliq/2., 2) - pow(eps, 2))

// Equilibrium angle and numerical angles
#define ANGLE_PF (70.*pi/180.)
#define angle_eq (71.5*pi/180. - atan(eps/initial_position)) //- delta_angle) // ANGLE_PF 
double angle_dyn = 70*pi/180.;
double delta_angle = 0.;

// VOF numerical parameters
#define TEND 0.4
#define TOUT 0.01
#define LEVEL 8
#define u_err 1e-3
#define f_err 0.
vector h[];

// Penetration velocity
double sink = 0.;

// Positions polygon
double previous_X1, previous_X2, current_X1, current_X2;

// Mass loss
double total_mass_loss = 0.;

// Velocity boundary conditions
u.n[top]  = neumann(0.);
u.t[top] = neumann(0.);

u.n[right]  = neumann(0.);
u.t[right] = neumann(0.);

u.n[left]  = neumann(0.);
u.t[left] = neumann(0.);

u.n[bottom]  = dirichlet(sink); // Modify to allow penetration

// scalar sink2[];
// u.n[bottom]  = dirichlet(sink2[]); // Modify to allow penetration

#if SLIP
double variable_slip = 0.;
// u.t[bottom] = robin(0., variable_slip);
u.t[bottom] = robin(0., eps/2.);
scalar local_slip[];
// u.t[bottom] = robin(0., local_slip[]);
#else
u.t[bottom] = dirichlet(0.);
#endif

// Contact angle condition
h.t[bottom] = contact_angle(angle_dyn);
// h.t[bottom] = contact_angle(angle_eq);

// Peclet numbers considered
double Peclets[] = {1., 5., 10.};
int arrLen = sizeof Peclets / sizeof Peclets[0];
double As[] = {1.};
int arrLen2 = sizeof As / sizeof As[0];

// Compute a polygon area 
double polygonArea(double X[], double Y[], int n)
{
    // Initialize area
    double area = 0.0;
 
    // Calculate value of shoelace formula
    int j = n - 1;
    for (int i = 0; i < n; i++)
    {
        area += (X[j] + X[i]) * (Y[j] - Y[i]);
        j = i;  // j is previous vertex to i
    }
 
    // Return absolute value
    return fabs(area / 2.0);
}

// Bell function
double bell(double y){
	return ((1. - pow(tanh(y/eps), 2))/eps);
}

int slip = 1;

int main (){

    size(2.);
    N = pow(2.,LEVEL);

    f.sigma = SIGMA;
    f.height = h;  
    
    rho1 = rholiq; rho2 = rhogas;
    mu1 = muliq; mu2 = mugas;

	#if TOY
    #if LOOP
    for (int i = 0; i < arrLen; i++) {
        for (int j = 0; j < arrLen2; j++) {
            Pe =  Peclets[i];
            A = As[j];
			#if DYN
            // delta_angle = atan(eps/initial_position);
			#endif
            lmbda = eps; // Slip length
			lmbda2 = pow(eps,2); // Slip length^2
            origin(-_widthliq, eps);
			// origin(-_widthliq, 0.);
            run();
        }
	}
    #else
    origin(-_widthliq, eps);
    run();
    #endif
	#else
	origin(-_widthliq, 0.);
    run();
	#endif
}


event init (t = 0) {
    char name[200];
    sprintf (name, "initial_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL);
    static FILE * fp = fopen(name, "w");

	fraction (f, - (sq(x) + sq(y) - sq(_widthliq/2.)));
	double delta = 2.*_widthliq/pow(2,LEVEL);

	fprintf(fp, "# TOY = %d  SINK = %d  SLIP = %d  DYN = %d LEVEL = %d\n", TOY, SINK, SLIP, DYN, LEVEL);
	fprintf(fp, "# Ca = %g  Re = %g  Cn = %g  Pe = %g  mu2 = %g  rho2 = %g  angle_eq = %g\n", Ca, Re, Cn, Pe, Xi_mu, Xi_rho, angle_eq*180./pi);
	fprintf(fp, "# inital_position = %g\n", initial_position);
	fprintf(fp, "# Oh_d = %g  Oh = %g  La = %g\n", Oh_d, Oh, La);
	fprintf(fp, "# Delta = %g  eps = %g  eps/Delta = %g  a = %g\n", delta, eps, eps/delta, A);
	fprintf(fp, "--------------------------------------------------\n");

	boundary ({f});

    fclose(fp);
    previous_X1 = 0., previous_X2 = 0., current_X1 = 0., current_X2 = 0.;
}

#if ADAPT
event adapt(i++){
	adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err},  LEVEL);
}
#endif

event extract (i++){
	char name[200], name2[200], name3[200];
	#if JVIEW
	sprintf (name, "angles_JVIEW");
    sprintf (name2, "positions_JVIEW");
    sprintf (name3, "speed_JVIEW");
	#else
	sprintf (name, "angles_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL);
    sprintf (name2, "positions_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL);
    sprintf (name3, "speed_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL);
	#endif
	static FILE * fp = fopen(name, "w");
    static FILE * fp2 = fopen(name2, "w");
    static FILE * fp3 = fopen(name3, "w");

	double angle_extracted = 0.;
	double curv = 0., Ca_loc = 0.;
    double delta_mass_loss = 0.;
	double angle_above = 0., pos = 0.;
	scalar kappa[];

	curvature (f,kappa);

	foreach_boundary(bottom reduction(max:current_X1) reduction(max:angle_extracted) reduction(max:curv) reduction(max:Ca_loc)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x >= 0.)){
			current_X1 = x + Delta*height(h.x[]);
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.x, n.y);
			curv = kappa[];
			Ca_loc = Ca*u.x[];
			// Ca_loc = Ca*(u.x[-1,0]+2.*u.x[]+u.x[1,0])/4.; 
		}
	}

	foreach(reduction(max:angle_above) reduction(max:pos)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.) & (y > (eps)) & (y < (eps + Delta))){
			coord n = {0};
			n = interface_normal (point,f);
			angle_above = atan2(n.x, n.y);
			pos = y;
		}
	}
    #if DYN
    delta_angle = atan(eps/current_X1);
	#endif

    #if TOY
    current_X2 = current_X1 + pos*tan(pi/2. - angle_above); //current_X1 + eps*tan(pi/2. - angle_extracted);
    double X[] = {previous_X1, current_X1, current_X2, previous_X2};
    double Y[] = {eps, eps, 0., 0.};
    if (i > 2){
        delta_mass_loss = polygonArea(X, Y, 4);
        total_mass_loss += delta_mass_loss;
    }
	#if SINK
	// foreach(){
	// 	double dudx = u.x[1,0] - u.x[];
	// 	sink2[] = eps*dudx;
	// }
    sink = -delta_mass_loss/dt;
    #endif
	angle_dyn = acos(cos(angle_eq) - (3./(sqrt(2.)*2.0))*Ca_loc*2.0);
    previous_X1 = current_X1;
    previous_X2 = current_X2;
	#else

    #endif

	variable_slip = 0.5*eps*exp(-t/0.1);

	// #if SLIP
	// variable_slip = 0.5*eps*(t/TEND);
	// #endif
	// #if SLIP
	// foreach_boundary(bottom){
	// 	if (x > 0.){
	// 		double relative_position = x - current_X1;
	// 		local_slip[] = eps*bell(relative_position);
	// 	}
	// 	if (x < 0.){
	// 		double relative_position = x - current_X1;
	// 		local_slip[] = eps*bell(relative_position);
	// 	}
	// }
	// #endif

	fflush(fp);
    fflush(fp2);
    fflush(fp3);
	fprintf(fp,"%g %g %g %g %g %g\n", t, ANGLE_PF*180./pi, angle_eq*180./pi, angle_extracted*180./pi, angle_dyn*180./pi, angle_above*180./pi);
    fprintf(fp2,"%g %g %g %g %g %g %g\n", t, current_X1, current_X2, previous_X1, previous_X2, delta_mass_loss, total_mass_loss);
    fprintf(fp3,"%g %g %g %g\n", t, Ca_loc, sink, curv);
}

#if !JVIEW
event extract_facets (t += TOUT){
	char name[200];
	sprintf (name, "./facets/facets_t%g_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d_%d", t, TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL, tid());
	FILE * fp = fopen(name, "w");
	output_facets (f, fp);
	fclose(fp);
}
#endif

event final (t = TEND){
	char name[200], name2[200], name3[200], name4[200];
	#if JVIEW
    sprintf (name, "pressure_JVIEW");
    sprintf (name2, "curvature_JVIEW");
    sprintf (name3, "facets_JVIEW");
    sprintf (name4, "dudy_JVIEW");
	#else
    sprintf (name, "pressure_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d_%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL, tid());
    sprintf (name2, "curvature_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d_%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL, tid());
    sprintf (name3, "facets_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d_%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL, tid());
    sprintf (name4, "dudy_TOY%d_SINK%d_SLIP%d_DYN%d_Ca%g_Re%g_angle%g_Pe%g_a%g_LEVEL%d_%d", TOY, SINK, SLIP, DYN, Ca, Re, ANGLE_PF*180./pi, Pe, A, LEVEL, tid());
	#endif
	FILE * fp = fopen(name, "w");
	FILE * fp2 = fopen(name2, "w");
	FILE * fp3 = fopen(name3, "w");
	FILE * fp4 = fopen(name4, "w");
	scalar kappa[];

	curvature (f,kappa);

	foreach_boundary(bottom){
		double dudy = (u.x[0,1] - u.x[])/Delta;
		fprintf(fp4,"%g %g\n", x, dudy);
	}

	foreach(){
		fprintf(fp,"%g %g %g\n", x, y, p[]);
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x > 0.)){
			double xcl = x + Delta*height(h.x[]);
			double r = sqrt(pow(x,2) + pow(xcl, 2));
			coord n = {0};
			n = interface_normal (point,f);
			double theta = atan2(n.x, n.y);
			fprintf(fp2,"%g %g %g\n", r, kappa[], 180.*theta/pi);
		}
	}

	output_facets (f, fp3);

	fclose(fp);
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
}
#endif
