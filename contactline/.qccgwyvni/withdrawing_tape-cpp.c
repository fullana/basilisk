@if _XOPEN_SOURCE < 700
  @undef _XOPEN_SOURCE
  @define _XOPEN_SOURCE 700
@endif
@if _GNU_SOURCE
@include <stdint.h>
@include <string.h>
@include <fenv.h>
@endif
#define _CATCH
#define dimension 2
#define BGHOSTS 2
#include "common.h"
#include "grid/quadtree.h"
@include "_boundarydecl.h"
#ifndef BASILISK_HEADER_0
#define BASILISK_HEADER_0
#line 1 "withdrawing_tape.c"
#define JVIEW 1

#if JVIEW
#include "display.h"
#endif

#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "vof.h"
#include "contact.h"


#define xi 5.
#define q 1.

#define rho_1 5.
#define mu_1 1.
#define V_s 1.
#define N_g 25./64.
#define g 16.0

#define rho_2 (rho_1/xi)
#define mu_2 (mu_1/q)

#define l_c sqrt((sig)/((rho_1 - rho_2)*g))
#define L (9.*l_c)
#define h_0 (3.1*l_c)

#define sig (mu_1*V_s/Ca)

double Ca = 0.03;

vector h[];

int main ()
{
  init_grid (64);

  origin(0., 0.); 
  size (L);

  f.sigma = sig;
  f.height = h;  
  rho1 = rho_1; rho2 = rho_2;
  mu1 = mu_1; mu2 = mu_2;
  
  run();
}

event init (t = 0) {
	fraction (f, -(y-h_0));
  fprintf(ferr, "%lf %lf %lf \n", l_c, L, h_0);
	Ca = 0.05;
	fprintf(ferr, "%lf %lf %lf \n", l_c, L, h_0);
	boundary ({f});
}
#endif
