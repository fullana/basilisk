/* Flags */

#define OIL1 0
#define OIL2 1

#define CONSERVING 0

#define JVIEW 0
#define DUMP 1
#define LOOP 0

/* Header files */

#include "grid/octree.h"
#include "embed.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#if CONSERVING
#include "navier-stokes/conserving.h"
#endif
#include "tension.h"
#if !LOOP
#include "navier-stokes/perfs.h"
#endif

#if JVIEW
#include "display.h"
#else
#include "view.h"
#endif

/* --------------------------- */
/* Dimensional properties (SI) */ 
/* --------------------------- */ 

/* Oil or Water in fluid 1 */ 
#if OIL1
#define nu 50e-6
#elif OIL2
#define nu 500e-6           
#endif

#define rho_1 1.04e3
#define sig 21e-3 
#define mu_1 (rho_1*nu)

/* Air in fluid 2 */
#define rho_2 1.2047           
#define mu_2 1.8205e-5         

#define grav 9.8067                                         
#define radius 51.2e-3                             
#define initial_height 112e-3                      
#define total_height (1.25*initial_height)
#define z_measurements 100e-3

#define w_1 180. //rpm

double ratio_f = 0.67;
double eps = 0.057;

#define period (1./(ratio_f*w_1/60.))
#define omega (2*pi*ratio_f*w_1/60.)
#define amplitude (eps*radius)

#define u_ref (omega*radius)                        /* Reference velocity */     

#define tend (10.*period)                                 /* Final time */ 
#define tout 0.1                                  /* Extract data files every */
#define tdump period                                 /* Extract dump files every */
#define tmovie 0.1                                /* Extract movie every */

/* ----------------------------- */
/* Dimensionless properties (SI) */ 
/* ----------------------------- */ 

#define MU_unit (rho_1*u_ref*radius)              /* Scaling viscosity */
#define SIGMA_unit (rho_1*radius*u_ref*u_ref)     /* Scaling surface tension */
#define T_unit (radius/u_ref)                     /* Characteristic time scale */
#define G_unit (radius/(T_unit*T_unit))           /* Scaling gravity */
#define RHO_1 1.                                  /* Dimensionless density 1 */
#define RHO_2 (rho_2/rho_1)                       /* Dimensionless density 2 */
#define MU_1 (mu_1/MU_unit)                       /* Dimensionless viscosity 1 */
#define MU_2 (mu_2/MU_unit)                       /* Dimensionless viscosity 2 */
#define SIG (sig/SIGMA_unit)                      /* Dimensionless surface tension */
#define GRAV (grav/G_unit)                        /* Dimensionless gravity */
#define RADIUS 1.                                 /* Dimensionless diameter */
#define U 1.                                      /* Dimensionless velocity */
#define AMP (eps)                                 /* Dimensionless stage amplitude */
#define PERIOD (period/T_unit)                      /* Dimensionless stage frequency */
#define OMEGA (omega*T_unit)                      /* Dimensionless stage frequency */
#define TEND (tend/T_unit)                        /* Dimensionless final time */ 
#define TOUT (tout/T_unit)                        /* Dimensionless extraction time data*/
#define TDUMP (tdump/T_unit)                      /* Dimensionless extraction time dump*/
#define TMOVIE (tmovie/T_unit)                    /* Dimensionless extraction time movie*/
#define L0 (total_height/radius)                  /* Dimensionless domain size */
#define INITIAL_HEIGHT (initial_height/radius)    /* Dimensionless initial position */
#define Z_MEASUREMENTS (z_measurements/radius)

/* --------------------- */
/* Tank movement */ 
/* --------------------- */

#define TANK_VEL_x(t) -AMP*OMEGA*sin(OMEGA*t)     /* Dimensionless stage velocity x direction */
#define TANK_VEL_y(t) AMP*OMEGA*cos(OMEGA*t)     /* Dimensionless stage velocity y direction */

#define TANK_ACC_x(t) AMP*OMEGA*OMEGA*cos(OMEGA*t)     /* Dimensionless stage acceleration x direction */
#define TANK_ACC_y(t) AMP*OMEGA*OMEGA*sin(OMEGA*t)     /* Dimensionless stage acceleration y direction */

/* --------------------- */
/* Adaptivity parameters */ 
/* --------------------- */ 

#define robin_embed(a,b,c) ((dirichlet ((c)*Delta/(2*(b) + (a)*Delta))) + ((dirichlet (1.))*((2*(b) - (a)*Delta)/(2*(b) + (a)*Delta) - 1.)))

#define double_dirichlet(a, b, c) ((dirichlet ((c)*Delta/(2*(b) + (a)*Delta))) + ((dirichlet (0))*(-(2*(b) - (a)*Delta)/(2*(b) + (a)*Delta) - 1.)))

#define double_neumann(a, b, c) (neumann(0) + ((2.*b - a*Delta)/(2.*b + a*Delta) - 1.)*neumann(0))

double u_err = 1e-3, f_err = 1e-3;                /* Error thresholds */
int LEVEL = 6;                                    /* Level of refinement */

/* ------------------- */
/* Boundary conditions */ 
/* ------------------- */

/* No penetration boundary conditions */

u.n[back] = dirichlet(0);
u.n[front] = dirichlet(0);
u.n[top] = dirichlet(0);
u.n[bottom] = dirichlet(0);
u.n[left] = dirichlet(0);
u.n[right] = dirichlet(0);


u.n[embed] = dirichlet(0.); // normal component
u.t[embed] = dirichlet(0); // double_dirichlet(1., 1e-5, 0.1); // tangent component
u.r[embed] = dirichlet(0); // double_dirichlet(1., 1e-5, 0.1); // z component

vector mean_field[];
int timesteps = 0;

int main() {
#if CONSERVING
    TOLERANCE = 1e-4;
#endif
    size (L0);
    origin(-L0/2, -L0/2, 0.);
    init_grid (1 << LEVEL);

    rho1 = RHO_1, mu1 = MU_1;
    rho2 = RHO_2, mu2 = MU_2;
    
    f.sigma = SIG;
    run();
}

event init (t = 0) {
    char name[360];
    sprintf (name, "properties_nu%g_eps%g_ratio_f%g_tend%g_momentum%d_LEVEL%d", 
      nu*1e6, eps, ratio_f, tend/period, CONSERVING, LEVEL);
    FILE * fp = fopen(name, "w");

    fprintf(fp, "Re = %g, Re_nodim = %g\n", rho_1*omega*radius*radius/mu_1, RHO_1*OMEGA*RADIUS*RADIUS/MU_1);
    fprintf(fp, "Bo = %g, Bo_nodim = %g\n", rho_1*grav*radius*radius/sig, RHO_1*GRAV*RADIUS*RADIUS/SIG);
    fprintf(fp, "Fr = %g, Fr_nodim = %g\n", amplitude*omega*omega/grav, AMP*OMEGA*OMEGA/GRAV);
    fprintf(fp, "AMP AMP OMEGA %g\n", AMP*OMEGA*OMEGA);
    fprintf(fp, "GRAV %g\n", GRAV);
    fprintf(fp, "OMEGA %g\n", OMEGA);
    fprintf(fp, "T_unit %g\n", T_unit);
    fprintf(fp, "TEND %g\n", TEND);
    fprintf(fp, "Z_MEASUREMENTS %g\n", Z_MEASUREMENTS);
    
    fclose(fp);

    #if EMBED
    solid (cs, fs, (sq(RADIUS) - sq(y) - sq(x)));
    #endif
    
    fraction (f, -(z - INITIAL_HEIGHT));
}

// event adapt (i++) {
//   adapt_wavelet ({f,cs,u}, (double[]){f_err,f_err,u_err,u_err,u_err}, LEVEL);
// }

event acceleration (i++){
  face vector av = a;
  foreach_face(x){
    av.x[] += TANK_ACC_x(t);
  }
  foreach_face(y){
    av.y[] += TANK_ACC_y(t);
  }
  foreach_face(z){
    av.z[] -= GRAV;
  }
}

event avereage_field (t=((4. * 2. *pi)/OMEGA), i++){
  // coord us = {TANK_VEL_x(t),TANK_VEL_y(t),0.};
  foreach(){
    foreach_dimension(){
      mean_field.x[] += f[]*cs[]*(u.x[])*dt/(2.*pi/OMEGA);
    }
  }
}

#if DUMP
event dump_event(t=(5.*(2.*pi)/OMEGA), t+=2.*pi/OMEGA, t<=TEND){
// event dump_event(t=TEND){
  char name[360];
    sprintf (name, "dump_nu%g_eps%g_ratio_f%g_tend%g_momentum%d_LEVEL%d_n%g", 
      nu*1e6, eps, ratio_f, tend/period, CONSERVING, LEVEL, (OMEGA*t)/(2.*pi));

  dump(name);

  foreach(){
    foreach_dimension(){
      mean_field.x[] = 0.;
    }
  }
}
#endif

