#include "grid/octree.h"
#include "embed.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "navier-stokes/conserving.h"
#include "tension.h"
#include "utils.h"
#include "output_vtu_foreach.h"

vector mean_field[];

int main (int argc, char** argv) {
  char* filename = strdup (argv[1]);
  restore (file=filename);

  output_vtu((scalar *){cs, f, p}, (vector *){u, mean_field}, strdup (argv[1]));

  return 0;
}
