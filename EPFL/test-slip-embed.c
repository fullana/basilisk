#define MYEMBED 1


#if MYEMBED
#define robin_embed(a,b,c) ((dirichlet ((c)*Delta/(2*(b) + (a)*Delta))) + ((neumann (0))*((2*(b) - (a)*Delta)/(2*(b) + (a)*Delta) + 1.)))
#include "embed.h"
// #include "display.h"
#include "navier-stokes/centered.h"
// #include "navier-stokes/perfs.h"
#else
// #include "display.h"
#include "navier-stokes/centered.h"
#endif

double l = 0.0;

int main() {
  periodic(right);
  stokes = true;
  TOLERANCE = 1e-5;
  #if MYEMBED
  origin (0, -1.0);
  size(2);
  // for (N = 8; N <= 64; N *= 2){
  //   slip = l;
  //   run(); 
  // }
  N = 32;
  run();
  #else
  origin (0, -0.5);
  // for (N = 8; N <= 64; N *= 2){
    slip = l;
    run(); 
  // }
  #endif
}

#if MYEMBED
// u.t[embed] = robin(l, 0);
// u.n[embed] = robin_embed(1., l, 0.);
// u.t[embed] = dirichlet(0);
// u.n[embed] = dirichlet(0);
// u.t[top] = robin(l, 0);
#else
u.t[top] = robin(l, 0);
u.t[bottom] = robin(l, -1);
#endif

u.n[left] = neumann(0);
p[left] = dirichlet(y + 0.5);
u.n[right] = neumann(0);
p[right] = dirichlet(y + 0.5);

// scalar un[];

event init (t = 0) {
  #if MYEMBED
  solid (cs, fs,  intersection((0.25 + y), (0.25 - y)));
  // solid (cs, fs,  y);
  #endif
  dump("test");
  // we also check for hydrostatic balance
  const face vector g[] = {1.,1.};
  a = g;
  const face vector muc[] = {1.,1.};
  mu = muc;
  // foreach()
  //   un[] = u.x[];
}

// event logfile (t += 0.1; i <= 100) {
//   double du = change (u.x, un);
//   if (i > 0 && du < 1e-6)
//     return 1; /* stop */
// }

event profile (i = 100) {
  printf ("\n");
  foreach()
    printf ("%g %g %g %g %g\n", x, y, u.x[], u.y[], p[]);
  scalar e[];
  #if MYEMBED
  foreach()
    e[] = (1. - cs[])*(u.x[] - 0.5*(0.25 + l - y*y));
  #else
  foreach()
    e[] = u.x[] - 0.5*(0.25 + l - y*y);
  #endif
  norm n = normf (e);
  fprintf (stderr, "%d %g %g %g\n", N, n.avg, n.rms, n.max);
}
