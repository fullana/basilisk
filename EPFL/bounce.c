/* Flags */

#define CONSERVING 0

#define JVIEW 1
#define LOOP 0
#define SAVEDUMP 0
#define SAVEMOVIE 0

/* Header files */

#include "axi.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#if CONSERVING
#include "navier-stokes/conserving.h"
#endif
#include "tension.h"
#include "navier-stokes/perfs.h"

#if JVIEW
#include "display.h"
#else
#include "view.h"
#endif

double OMEGA = 1.0;
double GAMMA = 0.0;

// double OMEGA = 0.6;

/* --------------------------- */
/* Dimensional properties (SI) */
/* --------------------------- */

#define rho_1 1.04e3
#define mu_1 20e-3
#define sig 20e-3

#define rho_2 1.2047
#define mu_2 1.8205e-5

#define grav 9.8067

double radius = 0.9e-3; //pow(sq(OMEGA / (2. * pi * freq)) * (sig / rho_1), (1. / 3.)) // 0.9e-3
#define u_ref sqrt(2. * grav * INITIAL_POS * radius)
// #define freq (OMEGA / (2. * pi *sqrt(rho_1 *cube(radius) / sig)))
#define w_d sqrt(sig / (rho_1 * cube(radius)))
#define freq ((0.5 + 1.4*sqrt(Bo)) * OMEGA*w_d/ (2. * pi))
#define amp (GAMMA * grav / sq(2. * pi * freq))
#define period (1. / freq)

#define T_unit (radius / u_ref)
#define MU_unit (rho_1 * u_ref * radius)            /* Scaling viscosity */
#define SIGMA_unit (rho_1 * radius * u_ref * u_ref) /* Scaling surface tension */
#define G_unit (radius / (T_unit * T_unit))         /* Scaling gravity */

#define Bo (rho_1 * grav * sq(radius) / sig)
#define Re (rho_1 * u_ref * radius / mu_1)
#define We (rho_1 * radius * sq(u_ref) / sig)
#define Oh (mu_1 / sqrt(sig * rho_1 * radius))
#define Oha (mu_2 / sqrt(sig * rho_1 * radius))
#define Ca (mu_1 * u_ref / sig)

#define Bo_nd (RHO_1 * GRAV * sq(RADIUS) / SIG)
#define Re_nd (RHO_1 * U * RADIUS / MU_1)
#define We_nd (RHO_1 * RADIUS * sq(U) / SIG)
#define Oh_nd (MU_1 / sqrt(SIG * RHO_1 * RADIUS))
#define Oha_nd (MU_2 / sqrt(SIG * RHO_1 * RADIUS))
#define Ca_nd (MU_1 * U / SIG)

#define tend (40. * period) /* Final time */
#define tout (period / 50.)   /* Extract data files every */
#define tdump (period / 10.)           /* Extract dump files every */
#define tmovie (period / 10.)         /* Extract movie every */

/* ----------------------------- */
/* Dimensionless properties (SI) */
/* ----------------------------- */

#define RHO_1 1.              /* Dimensionless density 1 */
#define RHO_2 (rho_2 / rho_1) /* Dimensionless density 2 */

#define MU_1 (mu_1 / MU_unit) /* Dimensionless viscosity 1 */
#define MU_2 (mu_2 / MU_unit) /* Dimensionless viscosity 2 */

#define SIG (sig / SIGMA_unit) /* Dimensionless surface tension */

#define GRAV (grav / G_unit) /* Dimensionless gravity */
#define RADIUS 1.            /* Dimensionless radius */

#define U 1.                     /* Dimensionless velocity */
#define AMP (amp / radius)       /* Dimensionless stage amplitude */
#define FREQ (freq * T_unit)     /* Dimensionless stage frequency */
#define TEND (tend / T_unit)     /* Dimensionless final time */
#define TOUT (tout / T_unit)     /* Dimensionless extraction time data*/
#define TDUMP (tdump / T_unit)   /* Dimensionless extraction time dump*/
#define TMOVIE (tmovie / T_unit) /* Dimensionless extraction time movie*/

double initial_pos = 6e-3;          /* Dimensionless initial position */
#define INITIAL_POS (initial_pos/radius)
#define l0 1e-3
#define L0 (8. * l0 / radius)        /* Dimensionless domain size */
#define initial_vel 0. // -sqrt(2. * grav * 0.01)
#define INITIAL_VEL (initial_vel/u_ref)           /* Dimensionless initial velocity */

/* --------------------- */
/* Energy budget */
/* --------------------- */

double viscousD1 = 0., viscousD2 = 0., stage_energy = 0., reaction_force = 0., E0 = 0., Etot = 0.;

/* ----------------------------- */
/* Stage movement */
/* ----------------------------- */

#define stage_pos(t) amp *cos(2. * pi * FREQ * (t))                               /* Stage position (m) */
#define stage_vel(t) -2. * pi *freq *amp *sin(2. * pi * FREQ * (t))                /* Stage velocity (m/s) */
#define stage_acc(t) -2. * pi *freq * 2. * pi *freq *amp *cos(2. * pi * FREQ * (t)) /* Stage acceleration (m/s2) */

#define STAGE_POS(t) AMP *cos(2. * pi * FREQ * (t))                               /* Dimensionless stage position */
#define STAGE_VEL(t) -2. * pi *FREQ *AMP *sin(2. * pi * FREQ * (t))                /* Dimensionless stage velocity */
#define STAGE_ACC(t) -2. * pi *FREQ * 2. * pi *FREQ *AMP *cos(2. * pi * FREQ * (t)) /* Dimensionless stage acceleration */

/* --------------------- */
/* Adaptivity parameters */
/* --------------------- */

double u_err = 1e-3, f_err = 1e-3; /* Error thresholds */
int LEVEL = 9;                     /* Level of refinement */

/* ------------------- */
/* Boundary conditions */
/* ------------------- */

// p[top] = dirichlet(0);
// pf[top] = dirichlet(0); 

uf.n[bottom] = 0.;

p[right] = dirichlet(0);
pf[right] = dirichlet(0);

u.t[left] = dirichlet(0);
u.n[left] = dirichlet(0);

f[left] = 0.;

void createMergeScript()
{
  const char *script = "#!/bin/bash\n\n"
                       "# Find all unique patterns in the directory\n"
                       "patterns=$(ls | awk -F_ '{ for (i=1; i<NF; i++) printf(\"%s%s\", $i, (i<NF-1) ? \"_\" : \"\"); printf(\"\\n\") }' | grep '_t[^-]*$' | sort | uniq)\n\n"
                       "for pattern in $patterns; do\n"
                       "    output_file=\"${pattern}-ALL\"\n"
                       "    cat \"${pattern}\"_* >> \"$output_file\"\n"
                       "    rm -f \"${pattern}\"_*\n"
                       "done\n";

  // Write the script to the "merge_facets.sh" file
  FILE *file = fopen("merge_facets.sh", "w");
  if (file == NULL)
  {
    printf("Error: Failed to create the script file.\n");
    return;
  }
  fputs(script, file);
  fclose(file);

  // Make the script file executable
  system("chmod +x merge_facets.sh");
}

int main()
{
#if CONSERVING
  TOLERANCE = 1e-5;
#endif
#if SAVEDUMP
  createMergeScript();
#endif
  size(L0);

  origin(0., 0.);

  init_grid(1 << LEVEL);

  rho1 = RHO_1, mu1 = MU_1;
  rho2 = RHO_2, mu2 = MU_2;

  f.sigma = SIG;
#if LOOP
  // for (OMEGA = 4.0; OMEGA >= 0.1; OMEGA -= 0.1){
    // for (GAMMA = 0.2; GAMMA <= 7.0; GAMMA += 0.2){
    for (radius = 0.6e-3; radius <= 1.3e-3; radius += 0.1e-3){
      viscousD1 = 0., viscousD2 = 0., stage_energy = 0., reaction_force = 0., E0 = 0., Etot = 0.;
      run();
    }
    // }
  // }
#else
  run();
#endif
}

event init(t = 0)
{
  char name[360];
  sprintf(name, "properties_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);
  FILE *fp = fopen(name, "w");

  fprintf(fp, "%-12s %-12s\n", "freq", "FREQ");
  fprintf(fp, "%g %g\n\n", freq, FREQ);
  fprintf(fp, "%-12s %-12s\n", "amp", "AMP");
  fprintf(fp, "%g %g\n\n", amp, AMP);
  fprintf(fp, "%-12s\n", "u_ref");
  fprintf(fp, "%g\n\n", u_ref);
  fprintf(fp, "%-12s\n", "T_unit");
  fprintf(fp, "%g\n\n", T_unit);
  fprintf(fp, "%-12s %-12s %-12s %-12s %-12s %-12s\n", "RHO_1", "RHO_2", "MU_1", "MU_2", "SIG", "GRAV");
  fprintf(fp, "%-12.2e %-12.2e %-12.2e %-12.2e %-12.2e %-12.2e\n\n", RHO_1, RHO_2, MU_1, MU_2, SIG, GRAV);
  fprintf(fp, "%-12s %-12s\n", "Bo", "Bo_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", Bo, Bo_nd);
  fprintf(fp, "%-12s %-12s\n", "Re", "Re_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", Re, Re_nd);
  fprintf(fp, "%-12s %-12s\n", "We", "We_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", We, We_nd);
  fprintf(fp, "%-12s %-12s\n", "Oh", "Oh_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", Oh, Oh_nd);
  fprintf(fp, "%-12s %-12s\n", "Oha", "Oha_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", Oha, Oha_nd);
  fprintf(fp, "%-12s %-12s\n", "Ca", "Ca_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", Ca, Ca_nd);
  fprintf(fp, "%-12s %-12s %-12s\n", "Time (s)", "Time_nodim", "TOUT");
  fprintf(fp, "%-12.2e %-12.2e %-12.2e\n\n", tend, TEND, TOUT);
  fprintf(fp, "%-12s %-12s\n", "OMEGA (Hz)", "OMEGA_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", OMEGA, OMEGA);
  fprintf(fp, "%-12s %-12s\n", "Grid (m)", "Grid_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", L0 * radius / pow(2, LEVEL), L0 / pow(2, LEVEL));
  fprintf(fp, "%-12s %-12s\n", "Domain (m)", "Domain_nodim");
  fprintf(fp, "%-12.2e %-12.2e\n\n", L0 * radius, L0);
  fprintf(fp, "%-12s %-12s\n", "INITIAL_POS", "INITIAL_VEL");
  fprintf(fp, "%-12.2e %-12.2e\n\n", INITIAL_POS, INITIAL_VEL);
  fprintf(fp, "%-12s %-12s\n", "INITIAL_STAGE_POS", "INITIAL_STAGE_VEL");
  fprintf(fp, "%-12.2e %-12.2e\n\n", STAGE_POS(0), STAGE_VEL(0));

  fclose(fp);

  fraction(f, -sq(x - INITIAL_POS) - sq(y) + sq(RADIUS));

  foreach ()
    u.x[] = f[] * INITIAL_VEL;

  system("mkdir -p energies");
  system("mkdir -p positions");
  system("mkdir -p velocities");
  system("mkdir -p volumes");
  system("mkdir -p facets");
  system("mkdir -p dumps");
}

event adapt(i++)
{
  adapt_wavelet({f, u}, (double[]){f_err, u_err, u_err, u_err}, LEVEL);
}

event acceleration(i++)
{
  face vector av = a;
  foreach_face(x)
      av.x[] -= (GRAV - STAGE_ACC(t));
}

event compute_time_integrals(i++)
{ 
  double vd1 = 0., vd2 = 0., instantaneous_stage_energy = 0.;
  reaction_force = 0.;

  foreach (reduction(+:vd1) reduction(+:vd2))
  {
    double dv1 = 2. * pi * f[] * dv();
    double dv2 = 2. * pi * (1. - f[]) * dv();
    double vd = 0.;
    double dudr = sq(u.y[0, 1] - u.y[0, -1]) / sq(2. * Delta);
    double dudy = sq(u.y[1, 0] - u.y[-1, 0]) / sq(2. * Delta);
    double dvdr = sq(u.x[0, 1] - u.x[0, -1]) / sq(2. * Delta);
    double dvdy = sq(u.x[1, 0] - u.x[-1, 0]) / sq(2. * Delta);
    double ur = sq(u.y[] / y);
    vd = 2 * (dudr + dvdy + ur) + (dvdr + dudy);
    vd1 += mu1 * dv1 * vd * dt;
    vd2 += mu2 * dv2 * vd * dt;
  }

  foreach_boundary(left, reduction(+:reaction_force) reduction(+:instantaneous_stage_energy))
  {
    if (f[] > 1e-6 && f[] < 1. - 1e-6)
    {
      coord n = interface_normal(point, f), p_;
      double alpha = plane_alpha(f[], n);
      double area_ = 2. * pi * y * pow(Delta, dimension - 1) * plane_area_center(n, alpha, &p_);
      reaction_force += -p[] * area_ * n.x;
      instantaneous_stage_energy += p[] * area_ * n.x * (STAGE_VEL(t)) * dt; 
    }
  }

  stage_energy += instantaneous_stage_energy;
  viscousD1 += vd1;
  viscousD2 += vd2;

}

event logfile(t += TOUT, t <= TEND)
{
  char datafile1[360], datafile2[360], datafile3[360], datafile4[360], datafile5[360], datafile6[360], datafile7[360];
  double xc = 0., yc = 0., uc = 0., vc = 0., wd = 0., hd = 0.;
  double vol1 = 0., ke1 = 0., gpe1 = 0.;
  double vol2 = 0., ke2 = 0., gpe2 = 0.;
  double x2 = 0., x1 = 0., y1 = 0., xmin = 100.;
  double area = 0.;

  coord us = {STAGE_VEL(t), 0., 0.};

  sprintf(datafile1, "./positions/POS_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);
  sprintf(datafile2, "./positions/pos_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);
  sprintf(datafile3, "./velocities/VEL_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);
  sprintf(datafile4, "./velocities/vel_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);
  sprintf(datafile5, "./energies/ENERGY_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);
  sprintf(datafile6, "./energies/energy_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);
  sprintf(datafile7, "./volumes/volume_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);

  static FILE *fp1 = fopen(datafile1, "w");
  static FILE *fp2 = fopen(datafile2, "w");
  static FILE *fp3 = fopen(datafile3, "w");
  static FILE *fp4 = fopen(datafile4, "w");
  static FILE *fp5 = fopen(datafile5, "w");
  static FILE *fp6 = fopen(datafile6, "w");
  static FILE *fp7 = fopen(datafile7, "w");

  if (t == 0)
  {
    fprintf(fp1, "%-12s\n", "#data file non_dimensional unit");
    fprintf(fp1, "%-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
            "#1 time", "#2 x_stage", "#3 x_center", "#4 y_center", "#5 width", "#6 height", "#7 air layer height");

    fprintf(fp2, "%-12s\n", "#data file dimensional unit (SI)");
    fprintf(fp2, "%-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
            "#1 time", "#2 x_stage", "#3 x_center", "#4 y_center", "#5 width", "#6 height", "#7 air layer height");

    fprintf(fp3, "%-12s\n", "#data file non_dimensional unit");
    fprintf(fp3, "%-12s %-12s %-12s %-12s %-12s %-12s\n\n",
            "#1 time", "#2 ux_stage", "#3 gx_stage", "#4 ux_center", "#5 uy_center", "#6 Webber");

    fprintf(fp4, "%-12s\n", "#data file dimensional unit (SI)");
    fprintf(fp4, "%-12s %-12s %-12s %-12s %-12s %-12s\n\n",
            "#1 time", "#2 ux_stage", "#3 gx_stage", "#4 ux_center", "#5 uy_center", "#6 Webber");

    fprintf(fp5, "%-12s\n", "#data file non_dimensional unit");
    fprintf(fp5, "%-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
            "#1 time", "#2 kinetic_1", "#3 kinetic_2", "#4 potential_1", "#5 potential_2", "#6 viscousD_1", "#7 viscousD_2", "#8 Interfacial Energy", "#9 stage energy", "#10 reaction force", "#11 Etot");

    fprintf(fp6, "%-12s\n", "#data file dimensional unit (SI)");
    fprintf(fp6, "%-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
            "#1 time", "#2 kinetic_1", "#3 kinetic_2", "#4 potential_1", "#5 potential_2", "#6 viscousD_1", "#7 viscousD_2", "#8 Interfacial Energy", "#9 stage energy", "#10 reaction force", "#11 Etot");

    fprintf(fp7, "%-12s\n", "#Volume conservation");
    fprintf(fp7, "%-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
            "#1 time", "#2 area", "#3 vol1", "#4 vol2", "#5 time_dim", "#6 vol1_dim", "#7 vol2_dim");
  }

  foreach (reduction(+:xc) reduction(+:yc) reduction(+:uc) reduction(+:vc)
    reduction(+:vol1) reduction(+:ke1) reduction(+:gpe1)
    reduction(+:vol2) reduction(+:ke2) reduction(+:gpe2)
    reduction(+:area))
  {
    double dv1 = 2. * pi * f[] * dv();
    double dv2 = 2. * pi * (1. - f[]) * dv();
    double norm2 = 0.;
    foreach_dimension()
    {
      norm2 += sq(u.x[] - us.x);
    }
    uc += (u.x[] - us.x) * dv1;
    vc += u.y[] * dv1;
    xc += x * dv1;
    yc += y * dv1;
    vol1 += dv1;
    ke1 += rho1 * norm2 * dv1;
    gpe1 += rho1 * GRAV * (x - STAGE_POS(t) + STAGE_POS(0)) * dv1;
    vol2 += dv2;
    ke2 += rho2 * norm2 * dv2;
    gpe2 += rho2 * GRAV * (x - STAGE_POS(t) + STAGE_POS(0)) * dv2;
    if (f[] > 1e-6 && f[] < 1. - 1e-6)
      {
        coord n = interface_normal(point, f), p_;
        double alpha = plane_alpha(f[], n);
        area += 2. * pi * y * pow(Delta, dimension - 1) * plane_area_center(n, alpha, &p_);
      }
  }

  ke1 /= 2.;
  ke2 /= 2.;

  if (t == 0)
    E0 = ke1 + ke2 + gpe1 + SIG * 4. * pi * sq(RADIUS);

  vector h[];
  heights(f, h);

  foreach (reduction(max:y1) reduction(min:xmin))
  {
    if ((h.y[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)) & (x < (xc / vol1 + Delta)) & (x > (xc / vol1 - Delta)))
    {
      double pos = y + Delta * height(h.y[]);
      if (pos > yc / vol1)
        y1 = pos;
    }
    if ((h.x[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)))
    {
      double pos = x + Delta * height(h.x[]);
      if (pos < xmin)
        xmin = pos;
    }
  }
  foreach_boundary(bottom, reduction(max:x1) reduction(max:x2))
  {
    if ((h.x[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)))
    {
      double pos = x + Delta * height(h.x[]);
      if (pos > xc / vol1)
        x1 = pos;
      if (pos < xc / vol1)
        x2 = pos;
    }
  }

  wd = 2. * fabs(y1);
  hd = fabs(x1 - x2);
  
  Etot = ke1 + gpe1 + SIG * area + viscousD1 + viscousD2 - stage_energy;

  fprintf(fp1, "%-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
          t * T_unit / period, STAGE_POS(t), ((xc / vol1) + STAGE_POS(0)), yc / vol1, wd, hd, xmin);
  fprintf(fp2, "%-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
          t * T_unit, stage_pos(t), radius * ((xc / vol1) + STAGE_POS(0)), radius * yc / vol1, radius * wd, radius * hd, radius * xmin);

  fprintf(fp3, "%-12g %-12g %-12g %-12g %-12g %-12g\n",
          t * T_unit / period, STAGE_VEL(t), STAGE_ACC(t), uc, vc, RHO_1*sq(uc)*RADIUS/SIG);
  fprintf(fp4, "%-12g %-12g %-12g %-12g %-12g %-12g\n",
          t * T_unit, stage_vel(t), stage_acc(t), u_ref * uc, u_ref * vc, rho_1*sq(u_ref*uc)*radius/sig);

  fprintf(fp5, "%-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
          t * T_unit / period, ke1 / E0, ke2 / E0, gpe1 / E0, gpe2 / E0, viscousD1 / E0, viscousD2 / E0, SIG * area / E0, stage_energy / E0, reaction_force, Etot / E0);
  fprintf(fp6, "%-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
          t * T_unit, ke1, ke2, gpe1, gpe2, viscousD1, viscousD2, SIG * area, stage_energy, reaction_force, Etot);

  fprintf(fp7, "%-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
          t * T_unit / period, area, 2. * pi * vol1, 2. * pi * vol2, t * T_unit, 2. * pi * vol1 * cube(radius), 2. * pi * vol2 * cube(radius));

  fflush(fp1);
  fflush(fp2);
  fflush(fp3);
  fflush(fp4);
  fflush(fp5);
  fflush(fp6);
  fflush(fp7);
}

#if SAVEDUMP
event dumpfile(t += TDUMP)
{
  char facetsfile[360], dumpfile[360];

  sprintf(facetsfile, "./facets/facets_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d_t%g_%d",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL, t * T_unit, tid());
  sprintf(dumpfile, "./dumps/dump_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d_t%g",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL, t * T_unit);

  FILE *fp1 = fopen(facetsfile, "w");

  output_facets(f, fp1);

  fclose(fp1);

  dump(dumpfile);

  system("cd facets && ../merge_facets.sh");
}

#endif

#if SAVEMOVIE
event moviefile(t += TMOVIE)
{
  char vofmovie[360];

  sprintf(vofmovie, "vof_pos%g_radius%g_GAMMA%g_OMEGA%g_LEVEL%d.mp4",
          INITIAL_POS, radius*1e3, GAMMA, OMEGA, LEVEL);

  view(quat = {0.000, 0.000, -0.707, 0.707},
       fov = 30, near = 0.01, far = 1000,
       tx = 0.537, ty = -0.452, tz = -2.490,
       width = 1024, height = 1024);
  clear();
  box();
  draw_vof(c = "f", fc = {0, 0, 0}, lc = {0, 0, 0}, lw = 2);
  save(vofmovie);
}
#endif