#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "tag.h"
#include "view.h"
#include "vof.h"
#include "heights.h"
#include "contact.h"
#include "curvature.h"


// Phase-Field dimensionless parameters 
#define Ca 0.0212
#define Re 3.978
#define Xi_mu 1e-2
#define Xi_rho 1e-2

// Liquid-Gas physical parameters
#define muliq_SI 8.77e-4
#define rholiq_SI 986.
#define mugas_SI (muliq_SI*Xi_mu)
#define rhogas_SI (rholiq_SI*Xi_rho)
#define SIGMA_SI 5.78e-2 

#define _height_SI 2.5323477913879776e-6
#define _width_SI 5.*2.5323477913879776e-6 // Works on 5 processors  
#define _widthliq_SI 2.5323477913879776e-6
#define u_plates_SI 1.397217787913341 // For Ca = 0.0212   

#define _width 1.
#define _height (_width/npe())    
#define MU_unit (rholiq_SI*u_plates_SI*_height_SI)
#define SIGMA_unit (rholiq_SI*_height_SI*u_plates_SI*u_plates_SI)
#define T_unit ((_height_SI*u_plates)/(_height*u_plates_SI))
#define L_unit (_height_SI/_height)

#define muliq (muliq_SI/MU_unit) // = 1/Re_l
#define mugas (mugas_SI/MU_unit) 
#define SIGMA (SIGMA_SI/SIGMA_unit) // =1/We_l 
#define rholiq 1.
#define rhogas rhogas_SI/rholiq_SI
#define u_plates 1.
#define _widthliq (_widthliq_SI/L_unit) 

#define TEND_SI 8e-7
#define TOUT_SI 0.4e-9
#define TEND (TEND_SI/T_unit)
#define TOUT (TOUT_SI/T_unit)
#define TCHAR 1.

#define S sqrt((Ca*Cn)/Pe)
#define eps S*sqrt(A/0.8679)
#define slip2 S*sqrt(A/0.8679)

// #define SHIFT ((_height_SI-L_unit*eps)/_height_SI)
#define ORIGIN ((_width/2.) - eps)
#define SIZE (_width - 10.*eps)

#define NN 512

double Cn = 0.04;
double A = 2.5;
double Pe = 30.0;

double rec = 90.*pi/180., adv = 90.*pi/180.;
double minx = -0.5, maxx = 0.5;
double prev_r, prev_a;
double clb1, clt1, clb2, clt2, cl2b1, cl2t1;
double t_b, t_t, tmp;
vector h[];
scalar f2[];

u.n[top]  = dirichlet(0);
u.n[bottom]  = dirichlet(0);
h.t[bottom] = contact_angle(x<0. ? (rec) : (adv));
h.t[top] = contact_angle(x>0. ? (rec) : (adv));
u.t[bottom] = robin(eps,-u_plates);
u.t[top] = robin(eps,u_plates);

double sol(double y, double l, double u){
        double a = (((10.*u)/(1.+2.*l))*(y+0.4));
        return a;
}

double ORI (double nx, double ny){
	double angle = 0.0;
	if ((nx > 0) & (ny >= 0))
		angle = atan(ny/nx);
	if ((nx > 0) & (ny < 0))
		angle = atan(ny/nx) + 2.*pi;
	if (nx < 0)
		angle = atan(ny/nx) + pi;
	if ((nx == 0) & (ny > 0))
		angle = pi/2.;
	if ((nx == 0) & (ny < 0))
		angle = 3.*pi/2.;
	return angle;
} 

double ORI2 (double nx, double ny, double prev){
	double angle = atan(ny/(nx + 1E-30));
	if (fabs(angle) > pi/18.)
		angle = prev;
	return angle;
} 
double angle_eq_r = 90.;
double angle_eq_a = 90.;

double Peclets[] = {30., 100., 160., 300.};
int arrLen = sizeof Peclets / sizeof Peclets[0];
double Cns[] = {0.02, 0.04};
int arrLen2 = sizeof Cns / sizeof Cns[0];

double angle_eq_rs[] = {88., 88., 87., 87., 88., 86., 86., 86.};

double angle_eq_as[] = {90.5, 90.5, 91., 91., 91., 91.2, 92., 92.};

int main ()
{

  init_grid (NN);
  
  origin(-ORIGIN,-ORIGIN); 
  size (SIZE);

  slip = eps;
  
  periodic (right); 
  
  f.sigma = SIGMA;
  f.height = h;  
  rho1 = rholiq; rho2 = rhogas;
  mu1 = muliq; mu2 = mugas;
  int ii = 0;
  for (int i = 0; i < arrLen; i++) {
        for (int j = 0; j < arrLen2; j++) {
            angle_eq_r = angle_eq_rs[ii];
            angle_eq_a = angle_eq_as[ii];
            Pe =  Peclets[i];
            Cn = Cns[j];
            run();
            ii += 1;
        }
  }
}

event init (t = 0) {
  if (!restore (file = "restart")) {

    fraction (f2, (x-_widthliq/2.)*(x+_widthliq/2.));
    foreach(){
	f[] = 1. - f2[];
    }

    char name[200];
    sprintf (name, "initial_NN512_a%g_Pe%g_Cn%g", A, Pe, Cn);
    static FILE * fp = fopen(name, "w");

    fprintf(fp,"#Re \t%e\t%e\n",1./muliq,u_plates_SI*_height_SI*rholiq_SI/muliq_SI);
    fprintf(fp,"#Ca \t%e\t%e\n",muliq/SIGMA,muliq_SI*u_plates_SI/SIGMA_SI);
    fprintf(fp,"#La \t%e\n",SIGMA*rholiq*_height/sq(muliq));
    fprintf(fp,"#We \t%e\n",rhogas*u_plates*u_plates*_height/SIGMA);
    fprintf(fp,"#Slip \t%e\t%e\n",eps, slip2);
    fprintf(fp,"#L_unit \t%e\n",L_unit);
    fprintf(fp,"#Slip_SI \t%e\t%e\n",L_unit*eps, L_unit*slip2);
    fprintf(fp,"#Grid point per slip \t%e\t%e\n",eps/(1./NN),slip2/(1./NN));
    fprintf(fp,"#shift \t%e\n",(_height_SI-L_unit*eps)/_height_SI);
    fprintf(fp,"#_width \t%e\n",(_width));
    fprintf(fp,"#Processors: \t%d\n",npe());

    fclose(fp);
  }
  boundary ({f});
  foreach(){
      u.x[] = sol(y,eps,u_plates);
     }
}

event imposeangle (i++){
double mini = -1E-10, maxi = 1E-10;
double urec = 1e100, uadv = 1e100;
double theta_r = 0.0, theta_a = 2.*pi;
double shift_r = 0.0, shift_a = 0.0;

char name[200];
sprintf (name, "transient_NN512_a%g_Pe%g_Cn%g", A, Pe, Cn);
static FILE * fp = fopen(name, "w");

vector h2[];
heights(f,h2);
foreach_boundary(bottom, reduction(min:mini) reduction(min:urec) reduction(max:theta_r)){
	//if ((h2.x[] != nodata) & (f[0,1] < 1.0e-6) & (f[0,1] > (1. - 1.0e-6))){
	if (h2.x[] != nodata){
		if ((mini < 0.) & ((x + Delta*height(h2.x[])) < mini)){
			if (fabs(u.x[]) < urec){ 
				mini = x + Delta*height(h2.x[]);
				urec = u.x[];
				if (theta_r <= pi/2.){
					coord n = {0};
        			n = interface_normal (point,f);
        			theta_r = pi/2. + ORI2(n.x,n.y, prev_r);//atan(n.y/(n.x + 1E-30));
        			//mini2 = x + Delta*height(h2.x[1,0]);
				}
				//rec = acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-u.x[]));
			}
		}
	}
}
foreach_boundary(bottom, reduction(max:maxi) reduction(min:uadv) reduction(min:theta_a)){
	//if ((h2.x[] != nodata) & (f[0,1] < 1.0e-6) & (f[0,1] > (1. - 1.0e-6))){
	if (h2.x[] != nodata){
		if ((maxi > 0.) & ((x + Delta*height(h2.x[])) > maxi)){
			if (fabs(u.x[]) < uadv){ 
				maxi = x + Delta*height(h2.x[]);
				uadv = u.x[];
				if (theta_a >= pi/2.){
					coord n = {0};
        			n = interface_normal (point,f);
        			theta_a = pi/2. - ORI2(n.x, n.y, prev_a);//atan(n.y/(n.x + 1E-30));
        			//maxi2 = x + Delta*height(h2.x[1,0]);
				}
				//rec = acos((3./(sqrt(2)*2.0))*Ca*alpha*fabs(-u_plates-u.x[]));
			}
		}
	}
}

if ((fabs(theta_r-rec)*180./pi) < 10.)
	shift_r = fabs(theta_r-rec);
if ((fabs(theta_a-adv)*180./pi) < 10.)
	shift_a = fabs(theta_a-adv);
prev_r = shift_r;
prev_a = shift_a;

rec = acos(cos(pi*angle_eq_r/180.) - (3./(sqrt(2.)*2.0))*Ca*fabs(urec)*A);//fabs(-u_plates-urec)); //- pi/73.;
adv = acos(cos(pi*angle_eq_a/180.) - (3./(sqrt(2.)*2.0))*Ca*fabs(uadv)*A);//*fabs(-u_plates-uadv));// + pi/73.;
fprintf(fp,"%g %g %g %g %g %g %g %g %g\n",t*T_unit*1e12, mini, maxi, rec*180./pi, adv*180./pi, theta_r*180./pi, theta_a*180./pi, shift_r*180./pi, shift_a*180./pi);
fflush(fp);
}

event snapshot2 (t = TEND) {

	char name[200];
    sprintf (name, "facets_NN512_a%g_Pe%g_Cn%g_%d", A, Pe, Cn, tid());
    FILE * fp = fopen(name, "w");

	        scalar pid[], ff[];
		  foreach() {

			      pid[] = fmod(pid()*(npe() + 37), npe());
			          ff[] = f[] < 1.0e-6 ? 0 : f[] > 1. - 1.0e-6 ? 1. : f[];
				    }
		    boundary ({pid,ff});

		      output_facets (ff, fp);

		        fclose (fp);

}