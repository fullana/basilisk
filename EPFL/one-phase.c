#include "grid/octree.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "navier-stokes/conserving.h"
#include "tension.h"
#include "lambda2.h"
#include "view.h"
#include "navier-stokes/perfs.h"
#include "maxruntime.h"

const double MU = 6e-3;
const double SIG = 20e-3;

face vector av[];

int main (int argc, char * argv[])
{
  maxruntime (&argc, argv);

  L0 = 2.*pi [1];
  foreach_dimension()
    periodic (right);

  rho1 = 1., mu1 = MU;
  rho2 = 1., mu2 = MU;

  f.sigma = SIG;

  a = av;
  N = 32;
  run();
}

event init (i = 0) {
  if (!restore (file = "restart"))
    foreach() {
      u.x[] = 0.;
      u.y[] = 0.;
      u.z[] = 0.;
    }
}

event acceleration (i++) {
  double f0 = 1., k = 2.;
  foreach_face(x)
    av.x[] += f0*(sin(k*z) + cos(k*y));
  foreach_face(y)
    av.y[] += f0*(sin(k*x) + cos(k*z));
  foreach_face(z)
    av.z[] += f0*(sin(k*y) + cos(k*x));
}


event logfile (i++; t <= 300) {
  coord ubar;
  foreach_dimension() {
    stats s = statsf(u.x);
    ubar.x = s.sum/s.volume;
  }
  
  double ke = 0., vd = 0., vol = 0.;
  foreach(reduction(+:ke) reduction(+:vd) reduction(+:vol)) {
    vol += dv();
    foreach_dimension() {
      // mean fluctuating kinetic energy
      ke += dv()*sq(u.x[] - ubar.x);
      // viscous dissipation
      vd += dv()*(sq(u.x[1] - u.x[-1]) +
		  sq(u.x[0,1] - u.x[0,-1]) +
		  sq(u.x[0,0,1] - u.x[0,0,-1]))/sq(2.*Delta);
    }
  }
  ke /= 2.*vol;
  vd *= MU/vol;

  if (i == 0)
    fprintf (stderr, "t dissipation energy Reynolds\n");
  fprintf (stderr, "%g %g %g %g\n",
	   t, vd, ke, 2./3.*ke/MU*sqrt(15.*MU/(vd + 1e-15)));
}

event movie (t += 0.25; t <= 150)
{
  view (fov = 44, camera = "iso", ty = .2,
	width = 600, height = 600, bg = {1,1,1}, samples = 4);
  clear();
  squares ("u.y", linear = true);
  squares ("u.x", linear = true, n = {1,0,0});
  scalar omega[];
  vorticity (u, omega);
  squares ("omega", linear = true, n = {0,1,0});
  scalar l2[];
  lambda2 (u, l2);
  isosurface ("l2", -1);
  save ("movie.mp4");
}