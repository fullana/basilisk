#include "embed.h"
#include "navier-stokes/centered.h"

#include "display.h"

#define U 1.


int main() {
  periodic(right);

  stokes = true;

  TOLERANCE = 1e-2;

  N = 32;

  run();

}


// u.t[top] = dirichlet(U);
u.n[top] = dirichlet(0);
// u.t[bottom] = dirichlet(-U);
u.n[bottom] = dirichlet(0);

// u.n[embed] = dirichlet(0);

event init (t = 0){
    const face vector muc[] = {1.,1.};
    mu = muc;

    solid (cs, fs,  y - 0.5);

    u.t[embed] = dirichlet(U);
}


event ouput_field(i = 100){
    foreach()
        printf ("%g %g %g %g %g\n", x, y, u.x[], u.y[], p[]);
}