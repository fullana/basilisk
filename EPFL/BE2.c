#include "grid/multigrid3D.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "lambda2.h"
#include "tag.h"
#include "view.h"
#include "navier-stokes/perfs.h"
#include "maxruntime.h"

const double ratio_mu = 1.;
const double RHO = 1.;
const double MU = 6e-3;
const double MU2 = ratio_mu*MU;
const double SIG = 0.46;
const double ratio_fraction = 0.1;
const double TEND = 10.;

face vector av[];

int main (int argc, char * argv[])
{
  maxruntime (&argc, argv);

  L0 = 2.*pi [1];
  foreach_dimension()
    periodic (right);

  rho1 = RHO, mu1 = MU;
  rho2 = RHO, mu2 = MU2;

  f.sigma = SIG;

  a = av;
  N = 64;
  run();
}

event init (t = 0) {
  if (!restore (file = "dump_BE2_N64_TEND10_t0")){
    foreach()
      f[] = 1.;
  } else {
    FILE *file = fopen("/home/tf/All/Misc/bub.in", "r");

    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    double col1, col2, col3, col4;
    int Ndrops = 0;

    while (fscanf(file, "%lg %lg %lg %lg", &col1, &col2, &col3, &col4) == 4) {
        Ndrops++;
    }
    
    fseek(file, 0, SEEK_SET);

    coord pc[Ndrops];
    double R[Ndrops];

    for (int i = 0; i < Ndrops; i++) {
        if (fscanf(file, "%lg %lg %lg %lg", &col1, &col2, &col3, &col4) == 4) {
                printf("Line %d: Col1=%lg, Col2=%lg, Col3=%lg, Col4=%lg\n", i + 1, col1, col2, col3, col4);
                pc[i].x = col1;
                pc[i].y = col2;
                pc[i].z = col3;
                R[i] = col4;
            } else {
                fprintf(stderr, "Error reading line %d\n", i + 1);
                break;
            }
    }

    vertex scalar phi[];

    foreach_vertex() {
    phi[] = HUGE;
        for (int i = 0; i < Ndrops; i++){
            phi[] = intersection (phi[], (sq(x - pc[i].x) + sq(y - pc[i].y) + sq(z - pc[i].z) - sq(R[i])));
        }
    }

    scalar f2[];
    fractions (phi, f2);

    foreach()
        f[] = 1. - f2[];

    fclose(file);
  }
}

event acceleration (i++) {
  double f0 = 1., k = 2.;
  foreach_face(x)
    av.x[] += f0*(sin(k*z) + cos(k*y));
  foreach_face(y)
    av.y[] += f0*(sin(k*x) + cos(k*z));
  foreach_face(z)
    av.z[] += f0*(sin(k*y) + cos(k*x));
}


event logfile10 (i = 0; i += 10; t <= TEND) {
  char datafile[360], datafile1[360];
  sprintf(datafile, "data_BE2_N%d_TEND%g", N, TEND);
  sprintf(datafile1, "velocities_BE2_N%d_TEND%g_i%d", N, TEND, i);
  static FILE *fp = fopen(datafile1, "w");
  FILE *fp1 = fopen(datafile1, "w");

  coord ubar;
  foreach_dimension() {
    stats s = statsf(u.x);
    ubar.x = s.sum/s.volume;
  }
  
  double ke = 0., vd = 0., vol = 0.;
  foreach(reduction(+:ke) reduction(+:vd) reduction(+:vol)) {
    vol += dv();
    foreach_dimension() {
      // mean fluctuating kinetic energy
      ke += dv()*sq(u.x[] - ubar.x);
      // viscous dissipation
      vd += dv()*(sq(u.x[1] - u.x[-1]) +
		  sq(u.x[0,1] - u.x[0,-1]) +
		  sq(u.x[0,0,1] - u.x[0,0,-1]))/sq(2.*Delta);
    }
  }
  ke /= 2.*vol;
  vd *= MU/vol;

  if (i == 0)
    fprintf (fp, "#i t dissipation energy Reynolds\n");
  fprintf (fp, "%d %g %g %g %g\n",
	   i, t, vd, ke, 2./3.*ke/MU*sqrt(15.*MU/(vd + 1e-15)));

  foreach(serial)
      fprintf (fp1, "%g %g %g %g %g %g %g\n", x, y, z, u.x[], u.y[], u.z[], f[]);

  fflush(fp);
  fclose(fp1);
}


event logfile1000 (i = 0; i += 1000; t <= TEND) {
  char datafile2[360], datafile3[360];
  sprintf(datafile2, "droplets_BE2_N%d_TEND%g_i%d", N, TEND, i);
  sprintf(datafile3, "stats_BE2_N%d_TEND%g", N, TEND);
  FILE *fp2 = fopen(datafile2, "w");
  static FILE *fp3 = fopen(datafile3, "w");

  scalar m[];
  foreach()
    m[] = f[] > 1e-3;

  int n = tag (m);
  double v[n];
  double total_volume = 0., total_area = 0.;
  coord b[n];

  for (int j = 0; j < n; j++)
    v[j] = b[j].x = b[j].y = b[j].z = 0.;

  foreach (serial){
    if (m[] > 0) {
      int j = m[] - 1;
      v[j] += dv()*f[];
      coord p = {x,y,z};
      foreach_dimension()
	      b[j].x += dv()*f[]*p.x;
    }
  }

#if _MPI
  MPI_Allreduce (MPI_IN_PLACE, v, n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce (MPI_IN_PLACE, b, 3*n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif

  for (int j = 0; j < n; j++){
    fprintf (fp2, "%d %g %g %g %g %g\n", j, b[j].x/v[j], b[j].y/v[j], b[j].z/v[j], v[j], 2.*pow(v[j] / (pi*4./3.), 1./3.));
    total_volume += v[j];
  }
    
  total_area = interface_area (f);

  fprintf (fp3, "%d %g %d %g %g\n", i, t, n, total_volume, total_area);

  fclose(fp2);
  fflush(fp3);
}


event dumfile (t += 10.; t <= TEND) {
  char dumpfile1[360];
  sprintf(dumpfile1, "dump_BE2_N%d_TEND%g_t%g", N, TEND, t);  
  dump(dumpfile1);
}

