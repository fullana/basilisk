#define JVIEW 0
#define ANGLE 1
#define ADAPT 0
#define GRID_CONV 1

#include "advection.h"
#include "contact.h"
#include "vof.h"
#if JVIEW
#include "display.h"
#endif

#define T 2.0
#define c1 0.1
#define c2 0.1
#define tau 1.0

#define radius 0.25
#define shift 0.01
#define theta_0 acos(shift/radius)

#define f_err 1e-9
#define u_err 1e-9

double maxerr_ref_ext = -1e300, maxerr_ref_right = -1e300, maxerr_ext_right = -1e300;	

scalar f[], cf[];
scalar * interfaces = {f}, * tracers = NULL;
int MAXLEVEL = 9;

double analytical_position (double t){
	double s = tau*sin((pi*t)/tau)/pi;
	return radius*sin(theta_0)*exp(c1*s);
}

double analytical_angle (double t){
	double s = tau*sin((pi*t)/tau)/pi;
	return pi/2. + atan( (-1./tan(theta_0))*exp(2.*c1*s) + (c2/(2.*c1) * (exp(2.*c1*s) - 1.)));
}

double angle_imposed = theta_0;
vector h[];
scalar kappa[];

h.t[bottom] = contact_angle(angle_imposed);

int main()
{
  origin (-0.5, 0.);

  f.tracers = {cf};
	f.height = h;

	#if GRID_CONV
	for (MAXLEVEL = 6; MAXLEVEL <= 9; MAXLEVEL += 1){
		init_grid (1 << MAXLEVEL);
  	run();
	}
	#else
	init_grid (1 << MAXLEVEL);
  run();
  #endif
}

event init (i = 0)
{
	fraction (f, - (sq(x) + sq(y + shift) - sq(radius)));
  foreach()
    cf[] = f[];
	boundary ({f});
	char name[80];
  sprintf (name, "facets");
  static FILE * fp2 = fopen(name, "w");
  output_facets (f, fp2);
}

event impose (i++){
	char name[80], name2[80], name3[80];
	#if GRID_CONV
	sprintf (name, "new_c1_%g_c2_%g_ANGLE%d_positions_LEVEL%d", c1, c2, ANGLE, MAXLEVEL);
	sprintf (name2, "new_c1_%g_c2_%g_ANGLE%d_angles_LEVEL%d", c1, c2, ANGLE, MAXLEVEL);
	sprintf (name3, "new_c1_%g_c2_%g_ANGLE%d_errors_LEVEL%d", c1, c2, ANGLE, MAXLEVEL);
	#else
	sprintf (name, "positions");
	sprintf (name2, "angles");
	sprintf (name3, "errors");
	#endif
	static FILE * fp = fopen(name, "w");
	static FILE * fp2 = fopen(name2, "w");
	static FILE * fp3 = fopen(name3, "w");
	double angle_left = 0., angle_right = 0., angle_above = 0., angle_ref = 0., angle_extrapolated = 0.;
	double position_left = 0., position_right = 0., position_ref = 0., curv_l = 0., curv_r = 0.;

	

	#if ADAPT
	#if TREE
	//adapt_wavelet ({f}, (double[]){f_err}, MAXLEVEL, list = {f, cf});
	adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err},  MAXLEVEL);
	#endif
	#endif

	foreach_face(y){
		u.y[] = -c1*cos(pi*t/tau)*y;
	}
	foreach_face(x){
		u.x[] = c1*cos(pi*t/tau)*x + c2*cos(pi*t/tau)*y;
	}

	
	curvature (f,kappa);

	foreach(reduction(max:angle_extrapolated) reduction(max:angle_above)){
		if ((y <= (2.*Delta)) & (y > (Delta)) & (h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6) & (x > 0.)){
				coord n = {0};
				n = interface_normal (point,f);
				angle_above = atan2(n.x, n.y);
				double tmp = pow(((h.x[0,1] - h.x[])/1.0), 2);
				angle_extrapolated = angle_above +  1.5*Delta*kappa[]*sqrt(1 + tmp)/sin(angle_above);
		}
	}
	
	foreach_boundary(bottom, reduction(max:angle_left) reduction(max:angle_right) reduction(max:position_left) reduction(max:position_right)){
		if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			if (x > 0.){
				coord n = {0};
				n = interface_normal (point,f);
				angle_right = atan2(n.x, n.y);
				position_right = x + Delta*height(h.x[]);
				curv_r = kappa[];
			}
			if (x < 0.){
				coord n = {0};
				n = interface_normal (point,f);
				angle_left = atan2(-n.x, n.y);
				position_left = x + Delta*height(h.x[]);
				curv_l = kappa[];
			}
		}
	}

	position_ref = analytical_position(t);
	angle_ref = analytical_angle(t);

	if (fabs(angle_ref - angle_extrapolated)*180./pi > maxerr_ref_ext){
		maxerr_ref_ext = fabs(angle_ref - angle_extrapolated)*180./pi;
	}

	if (fabs(angle_ref - angle_right)*180./pi > maxerr_ref_right){
		maxerr_ref_right = fabs(angle_ref - angle_right)*180./pi;
	}

	if (fabs(angle_right - angle_extrapolated)*180./pi > maxerr_ext_right){
		maxerr_ext_right = fabs(angle_right - angle_extrapolated)*180./pi;
	}

	#if ANGLE
	angle_imposed = angle_extrapolated;
	#endif

	// double kappa_ref = (1./radius)*exp(3.*c1*t);
	double kappa_ref = (1./radius)*exp(3.*c1*sin(pi*t/tau)*(tau/pi));

  fflush(fp);fflush(fp2);fflush(fp3);
	fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf\n", t, position_ref, position_left, position_right, curv_l, curv_r, kappa_ref);
	fprintf(fp2,"%lf %lf %lf %lf %lf %lf\n", t, angle_ref*180./pi, angle_left*180./pi, angle_right*180./pi, angle_above*180./pi, angle_extrapolated*180./pi);
  fprintf(fp3,"%lf %lf %lf %lf %lf %lf\n", t, fabs(angle_ref - angle_extrapolated)*180./pi, fabs(angle_ref - angle_right)*180./pi, fabs(angle_right - angle_extrapolated)*180./pi, fabs(curv_l - kappa_ref), fabs(curv_r - kappa_ref)) ;
}

event final (t = T) {
	char name[80];
	#if GRID_CONV
	sprintf (name, "new_c1_%g_c2_%g_maxerr_ANGLE%d", c1, c2, ANGLE);
	#else
	sprintf (name, "maxerr_adapt%d", ADAPT);
	#endif
	static FILE * fp = fopen(name, "w");
  fprintf(fp,"%d %lf %lf %lf\n", MAXLEVEL, maxerr_ref_ext, maxerr_ref_right, maxerr_ext_right);
	maxerr_ref_ext = -1e300, maxerr_ref_right = -1e300, maxerr_ext_right = -1e300;
	fflush(fp);
}
