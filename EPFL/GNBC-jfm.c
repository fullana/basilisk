#define JVIEW 0
#define ANGLE 1
#define ADAPT 0
#define GRID_CONV 1

#include "advection.h"
#include "contact.h"
#include "vof.h"

#if JVIEW
#include "display.h"
#endif

#define TEND 10.0
#define c1 0.5
#define c2 0.2
#define tau 1.0

#define radius 0.5
#define shift 0.0
#define theta_0 acos(shift / radius)

#define f_err 1e-3
#define u_err 1e-3

int LEVEL = 8, MINLEVEL = 5, MAXLEVEL = 5;

scalar f[], cf[], kappa[];
scalar *interfaces = {f}, *tracers = NULL;
vector h[];

double angle_imposed_left = theta_0, angle_imposed_right = theta_0;

double kappa_num = 1./(radius * sin(theta_0));
double time_prev = 0.;

double analytical_position_right(double t)
{
	double s = tau * sin((pi * t) / tau) / pi;
	return radius * sin(theta_0) * exp(c1 * s);
}

double analytical_angle_right(double t)
{
	double s = tau * sin((pi * t) / tau) / pi;
	return pi / 2. + atan((-1. / tan(theta_0)) * exp(2. * c1 * s) + (c2 / (2. * c1) * (exp(2. * c1 * s) - 1.)));
}

double analytical_curvature_right(double t)
{
	return (1. / (radius * sin(theta_0))) * exp(3. * c1 * sin(pi * t / tau) * (tau / pi));
}

double curvature_mathematica(double t)
{
	double a = -cos(analytical_angle_right(t));
	double b = sin(analytical_angle_right(t));
	return -3. * cos(pi * t / tau) * (c1 * sq(a) + c2 * a * b - c1 * sq(b));
}

double integral_form(double t)
{
	double a = curvature_mathematica(t);
	double b = curvature_mathematica(0.);
	return (1. / (radius * sin(theta_0))) * exp(a - b);
}

double velocity_x(double t, double x, double y)
{
	return c1 * cos(pi * t / tau) * x + c2 * cos(pi * t / tau) * y;
}

double velocity_y(double t, double x, double y)
{
	return -c1 * cos(pi * t / tau) * y;
}

h.t[bottom] = contact_angle((x < 0.) ? (angle_imposed_left) : (angle_imposed_right));

int main()
{
	origin(-0.5, 0.);

	f.height = h;
	f.tracers = {cf};

#if GRID_CONV
	for (LEVEL = MINLEVEL; LEVEL <= MAXLEVEL; LEVEL += 1)
	{	size(2. [0]);
		init_grid(1 << LEVEL);
		angle_imposed_left = theta_0;
		angle_imposed_right = theta_0;
		DT = 0.5/(max(c1, c2)*(1 << LEVEL));
		time_prev = 0.;
		kappa_num = 1./(radius * sin(theta_0));
		run();
	}
#else
	init_grid(1 << LEVEL);
	DT = 0.5/(max(c1, c2)*(1 << LEVEL));
	run();
#endif
}

event init(t = 0)
{
	fraction(f, -(sq(x) + sq(y + shift) - sq(radius)));
	foreach ()
		cf[] = f[];
	boundary({f});
}

event impose_velocity(i++)
{
	foreach_face(x)
	{
		u.x[] = velocity_x(t, x, y);
	}
	foreach_face(y)
	{
		u.y[] = velocity_y(t, x, y);
	}
}

event angle_extrapolation(i++; t <= TEND)
{
	char name[80], name2[80], name3[80], name4[80], name5[80];
#if GRID_CONV
	sprintf(name, "positions_c1_%g_c2_%g_shift%g_ANGLE%d_LEVEL%d", c1, c2, shift, ANGLE, LEVEL);
	sprintf(name2, "angles_left_c1_%g_c2_%g_shift%g_ANGLE%d_LEVEL%d", c1, c2, shift, ANGLE, LEVEL);
	sprintf(name3, "angles_right_c1_%g_c2_%g_shift%g_ANGLE%d_LEVEL%d", c1, c2, shift, ANGLE, LEVEL);
	sprintf(name4, "curvatures_c1_%g_c2_%g_shift%g_ANGLE%d_LEVEL%d", c1, c2, shift, ANGLE, LEVEL);
	sprintf(name5, "errors_c1_%g_c2_%g_shift%g_ANGLE%d_LEVEL%d", c1, c2, shift, ANGLE, LEVEL);
#else
	sprintf(name, "positions");
	sprintf(name2, "angles_left");
	sprintf(name3, "angles_right");
	sprintf(name4, "curvatures");
	sprintf(name5, "errors");

#endif
	static FILE *fp = fopen(name, "w");
	static FILE *fp2 = fopen(name2, "w");
	static FILE *fp3 = fopen(name3, "w");
	static FILE *fp4 = fopen(name4, "w");
	static FILE *fp5 = fopen(name5, "w");

	double angle_extracted_left = 0., angle_above_left = 0., angle_extrapolated_left = 0.;
	double angle_extracted_right = 0., angle_above_right = 0., angle_extrapolated_right = 0., angle_ref_right = analytical_angle_right(t);

	double position_left = 0.;
	double position_right = 0., position_ref_right = analytical_position_right(t);

	double curvature_left = 0.;
	double curvature_right = 0., curvature_ref_right = analytical_curvature_right(t);

#if ADAPT
	adapt_wavelet({f, u}, (double[]){f_err, u_err, u_err, u_err}, LEVEL);
#endif

	curvature(f, kappa);

	foreach (reduction(max:angle_extrapolated_left) reduction(max:angle_above_left)
		reduction(max:angle_extrapolated_right) reduction(max:angle_above_right))
	{
		if ((y <= (2. * Delta)) & (y > (Delta)) & (h.x[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > 1e-6))
		{
			if (x < 0.)
			{
				coord n = {0};
				n = interface_normal(point, f);
				angle_above_left = pi / 2. - asin(n.y);
				double tmp = pow(((h.x[0, 1] - h.x[]) / 1.0), 2);
				angle_extrapolated_left = angle_above_left + 1.5 * Delta * kappa[] * sqrt(1 + tmp) / sin(angle_above_left);
			}
			if (x > 0.)
			{
				coord n = {0};
				n = interface_normal(point, f);
				angle_above_right = pi / 2. - asin(n.y);
				double tmp = pow(((h.x[0, 1] - h.x[]) / 1.0), 2);
				angle_extrapolated_right = angle_above_right + 1.5 * Delta * kappa[] * sqrt(1 + tmp) / sin(angle_above_right);
			}
		}
	}

	foreach_boundary(bottom,
		reduction(min:position_left) reduction(max:position_right)
		reduction(max:angle_extracted_left) reduction(max:angle_extracted_right)
		reduction(max:curvature_left) reduction(max:curvature_right))
	{
		if ((h.x[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)))
		{
			if (x < 0.)
			{
				coord n = {0};
				n = interface_normal(point, f);
				angle_extracted_left = pi / 2. - asin(n.y);
				position_left = x + Delta * height(h.x[]);
				curvature_left = kappa[];
			}
			if (x > 0.)
			{
				coord n = {0};
				n = interface_normal(point, f);
				angle_extracted_right = pi / 2. - asin(n.y);
				position_right = x + Delta * height(h.x[]);
				curvature_right = kappa[];
			}
		}
	}

#if ANGLE
	angle_imposed_left = angle_extrapolated_left;
	angle_imposed_right = angle_extrapolated_right;
#endif

	double a = -cos(angle_ref_right);
	double b = sin(angle_ref_right);

	double k_ = -3. * kappa_num * cos(pi * t / tau) * (sq(a)*c1 + a*b*c2 - sq(b)*c1);
	double my_dt = t - time_prev;
	
	kappa_num += my_dt*k_;
	time_prev = t;

	double position_err_right = fabs(position_right - position_ref_right)/fabs(position_ref_right);
	double angle_err_right = fabs(angle_extracted_right - angle_ref_right)/fabs(angle_ref_right);
	double curvature_err_right = fabs(curvature_right - curvature_ref_right)/fabs(curvature_ref_right);

	fflush(fp);
	fflush(fp2);
	fflush(fp3);
	fflush(fp4);
	fflush(fp5);

	fprintf(fp, "%lf %lf %lf %lf\n", t, position_left, position_right, position_ref_right);
	fprintf(fp2, "%lf %lf %lf %lf\n", t, angle_above_left, angle_extrapolated_left, angle_extracted_left);
	fprintf(fp3, "%lf %lf %lf %lf %lf\n", t, angle_above_right, angle_extrapolated_right, angle_extracted_right, angle_ref_right);
	fprintf(fp4, "%lf %lf %lf %lf %lf %lf\n", t, curvature_left, curvature_right, curvature_ref_right, kappa_num, my_dt);
	fprintf(fp5, "%lf %lf %lf %lf\n", t, position_err_right, angle_err_right, curvature_err_right);
}