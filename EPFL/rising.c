/* Flags */
#define JVIEW 1
#define AXIS 0
#define ADAPT 1
#define STAGE 0
#define THREED 1

#define OIL 1
#define WATER 0

/* Header files */
#if AXIS
#include "axi.h" 
#endif

#if THREED
#include "grid/octree.h"
#endif

#include "view.h"

#if JVIEW
#include "display.h"
#endif

#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "navier-stokes/conserving.h"
#include "tension.h"
#include "navier-stokes/perfs.h"




/* --------------------------- */
/* Dimensional properties (SI) */ 
/* --------------------------- */ 

/* Oil or Water in fluid 1 */ 
#if OIL
#define rho_1 1.04e3
#define mu_1 20e-3
#define sig 20e-3 
#elif WATER
#define rho_1 997.              
#define mu_1 1.0016e-3   
#define sig 72.86e-3       
#endif

/* Air in fluid 2 */
#define rho_2 1.2047           
#define mu_2 1.8205e-5         

#define grav 9.8067                             /* Acceleration of gravity */           
#define radius 1e-3                             /* Experimental drop radius */            
#define u_ref 0.1                               /* Reference velocity */            
#define amp 5e-4                                /* Stage amplitude */        
#define freq 55.                                /* Stage frequency */          

#define tend 0.5                                /* Final time */ 

/* ----------------------------- */
/* Dimensionless properties (SI) */ 
/* ----------------------------- */ 

#define MU_unit (rho_1*u_ref*radius)            /* Scaling viscosity */
#define SIGMA_unit (rho_1*radius*u_ref*u_ref)   /* Scaling surface tension */
#define T_unit (radius/u_ref)                   /* Dimensionless time scale */
#define G_unit (radius/(T_unit*T_unit))         /* Scaling gravity */
#define RHO_1 1.                                /* Dimensionless density 1 */
#define RHO_2 (rho_2/rho_1)                     /* Dimensionless density 2 */
#define MU_1 (mu_1/MU_unit)                     /* Dimensionless viscosity 1 */
#define MU_2 (mu_2/MU_unit)                     /* Dimensionless viscosity 2 */
#define SIG (sig/SIGMA_unit)                    /* Dimensionless surface tension */
#define GRAV (grav/G_unit)                      /* Dimensionless gravity */
#define RADIUS 1.                               /* Dimensionless radius */
#define U 1.                                    /* Dimensionless velocity */
#define AMP (amp/radius)                        /* Dimensionless stage amplitude */
#define FREQ (freq*T_unit)                      /* Dimensionless stage frequency */
#define TEND (tend/T_unit)                      /* Dimensionless final time */ 

/* --------------------- */
/* Adaptivity parameters */ 
/* --------------------- */ 

double u_err = 1e-3, f_err = 1e-3;              /* Error thresholds */
int LEVEL = 6;                                  /* Level of refinement */


/* ------------------- */
/* Boundary conditions */ 
/* ------------------- */

#if AXIS
uf.n[bottom] = 0.;
u.n[top] = u.n[] > 0 ? neumann(0) : 0;
p[top] = dirichlet(0);
pf[top] = dirichlet(0);
u.t[left] = dirichlet(0); 
#elif !THREED
u.n[top] = u.n[] > 0 ? neumann(0) : 0;
p[top] = dirichlet(0);
pf[top] = dirichlet(0); 

u.n[bottom] = u.n[] > 0 ? neumann(0) : 0;
p[bottom] = dirichlet(0);
pf[bottom] = dirichlet(0);

u.t[left] = dirichlet(0);
#else
u.n[top] = u.n[] > 0 ? neumann(0) : 0;
p[top] = dirichlet(0);
pf[top] = dirichlet(0); 
u.n[bottom] = u.n[] > 0 ? neumann(0) : 0;
p[bottom] = dirichlet(0);
pf[bottom] = dirichlet(0);
u.n[front] = u.n[] > 0 ? neumann(0) : 0;
p[front] = dirichlet(0);
pf[front] = dirichlet(0); 
u.n[back] = u.n[] > 0 ? neumann(0) : 0;
p[back] = dirichlet(0);
pf[back] = dirichlet(0);
u.t[left] = dirichlet(0);
u.r[left] = dirichlet(0);
#endif

#if STAGE
u.n[left] = dirichlet(AMP*sin(2.*pi*FREQ*t));
#endif

f[left] = 0.;

int main() {
    #if AXIS
    size (4.*RADIUS);
    #else
    size (6.*RADIUS);
    #endif

    init_grid (1 << 6);

    rho1 = RHO_1, mu1 = MU_1;
    rho2 = RHO_2, mu2 = MU_2;
    
    f.sigma = SIG;

    for (scalar s in {f})
        s.refine = s.prolongation = fraction_refine;

    TOLERANCE = 1e-4;
    run();
}

event init (t = 0) {
    fprintf(ferr,"Re \t%e\t%e\n", (rho_1*radius*u_ref)/mu_1, (RHO_1*RADIUS*U)/MU_1);
    fprintf(ferr,"Ca \t%e\t%e\n", mu_1*u_ref/sig, MU_1*U/SIG);
    fprintf(ferr,"Bo \t%e\t%e\n", (rho_1-rho_2)*grav*radius*radius/sig, (RHO_1-RHO_2)*GRAV*RADIUS*RADIUS/SIG);
    fprintf(ferr,"Unit scalings \t%e\t%e\t%e\n", MU_unit, SIGMA_unit, T_unit);
    fprintf(ferr,"Final time \t%e\t%e\n", tend, TEND);
    fprintf(ferr,"Dimensionless prop \t%e\t%e\t%e\t%e\t%e\t%e\n", RHO_1, RHO_2, MU_1, MU_2, SIG, GRAV);
    fprintf(ferr,"Stage amplitude \t%e\t%e\n", amp, AMP);
    fprintf(ferr,"Stage frequency \t%e\t%e\n", freq, FREQ);
    #if AXIS
    fraction (f, - sq(x - 2.9*RADIUS) - sq(y) + sq(RADIUS));
    #elif !THREED
    fraction (f, - sq(x - 4.9*RADIUS) - sq(y - 3.*RADIUS) + sq(RADIUS));
    #else
    fraction (f, - sq(x - 4.*RADIUS) - sq(y - 3.*RADIUS) - sq(z - 3.*RADIUS) + sq(RADIUS));
    #endif
}

event acceleration (i++) {
  face vector av = a;
  foreach_face(x)
    av.x[] -= GRAV;
}


event logfile (i++) {
  double xb = 0., vb = 0., sb = 0.;
  foreach(reduction(+:xb) reduction(+:vb) reduction(+:sb)) {
    double dv = (1. - f[])*dv();
#if MOMENTUM
    vb += q.x[]*dv/rho(f[]);
#else
    vb += u.x[]*dv;
#endif
    xb += x*dv;
    sb += dv;
  }
  printf ("%g %g %g %g %g %g %g %g ", 
	  t, sb, -1., xb/sb, vb/sb, dt, perf.t, perf.speed);
  putchar ('\n');
  fflush (stdout);
}

event interface (t = TEND) {
  output_facets (f, stderr);
}


#if ADAPT
event adapt (i++) {
  adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err}, LEVEL);
}
#endif

