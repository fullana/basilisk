/* Flags */

#define CONSERVING 0

#define JVIEW 0
#define LOOP 1

/* Header files */

#include "axi.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#if CONSERVING
#include "navier-stokes/conserving.h"
#endif
#include "tension.h"
#include "navier-stokes/perfs.h"

#if JVIEW
#include "display.h"
#else
#include "view.h"
#endif

double OMEGA = 1.0;
double GAMMA = 2.0;

double INITIAL_POS = 3.0;          /* Dimensionless initial position */

/* --------------------------- */
/* Dimensional properties (SI) */
/* --------------------------- */

#define rho_1 1.04e3
#define mu_1 20e-3
#define sig 20e-3

#define rho_2 1.2047
#define mu_2 1.8205e-5

#define grav 9.8067

#define radius 0.4e-3 //pow(sq(OMEGA / (2. * pi * freq)) * (sig / rho_1), (1. / 3.)) // 0.9e-3
#define u_ref sqrt(2. * grav * INITIAL_POS * radius)
#define freq (OMEGA / (2. * pi *sqrt(rho_1 *cube(radius) / sig)))
#define amp (GAMMA * grav / sq(2. * pi * freq))
#define period (1. / freq)

#define T_unit (radius / u_ref)
#define MU_unit (rho_1 * u_ref * radius)            /* Scaling viscosity */
#define SIGMA_unit (rho_1 * radius * u_ref * u_ref) /* Scaling surface tension */
#define G_unit (radius / (T_unit * T_unit))         /* Scaling gravity */

#define Bo (rho_1 * grav * sq(radius) / sig)
#define Re (rho_1 * u_ref * radius / mu_1)
#define We (rho_1 * radius * sq(u_ref) / sig)
#define Oh (mu_1 / sqrt(sig * rho_1 * radius))
#define Oha (mu_2 / sqrt(sig * rho_1 * radius))
#define Ca (mu_1 * u_ref / sig)

#define Bo_nd (RHO_1 * GRAV * sq(RADIUS) / SIG)
#define Re_nd (RHO_1 * U * RADIUS / MU_1)
#define We_nd (RHO_1 * RADIUS * sq(U) / SIG)
#define Oh_nd (MU_1 / sqrt(SIG * RHO_1 * RADIUS))
#define Oha_nd (MU_2 / sqrt(SIG * RHO_1 * RADIUS))
#define Ca_nd (MU_1 * U / SIG)

#define tend (40. * period) /* Final time */
#define tout period / 50.   /* Extract data files every */
#define tdump 0.1           /* Extract dump files every */
#define tmovie 0.01         /* Extract movie every */

/* ----------------------------- */
/* Dimensionless properties (SI) */
/* ----------------------------- */

#define RHO_1 1.              /* Dimensionless density 1 */
#define RHO_2 (rho_2 / rho_1) /* Dimensionless density 2 */

#define MU_1 (mu_1 / MU_unit) /* Dimensionless viscosity 1 */
#define MU_2 (mu_2 / MU_unit) /* Dimensionless viscosity 2 */

#define SIG (sig / SIGMA_unit) /* Dimensionless surface tension */

#define GRAV (grav / G_unit) /* Dimensionless gravity */
#define RADIUS 1.            /* Dimensionless radius */

#define U 1.                     /* Dimensionless velocity */
#define AMP (amp / radius)       /* Dimensionless stage amplitude */
#define FREQ (freq * T_unit)     /* Dimensionless stage frequency */
#define TEND (tend / T_unit)     /* Dimensionless final time */
#define TOUT (tout / T_unit)     /* Dimensionless extraction time data*/
#define TDUMP (tdump / T_unit)   /* Dimensionless extraction time dump*/
#define TMOVIE (tmovie / T_unit) /* Dimensionless extraction time movie*/
#define L0 (8. * RADIUS)        /* Dimensionless domain size */
#define INITIAL_VEL 0.           /* Dimensionless initial velocity */

/* ----------------------------- */
/* Stage movement */
/* ----------------------------- */

#define stage_pos(t) amp *cos(2. * pi * FREQ * (t))                               /* Stage position (m) */
#define stage_vel(t) -2. * pi *freq *amp *sin(2. * pi * FREQ * (t))                /* Stage velocity (m/s) */
#define stage_acc(t) -2. * pi *freq * 2. * pi *freq *amp *cos(2. * pi * FREQ * (t)) /* Stage acceleration (m/s2) */

#define STAGE_POS(t) AMP *cos(2. * pi * FREQ * (t))                               /* Dimensionless stage position */
#define STAGE_VEL(t) -2. * pi *FREQ *AMP *sin(2. * pi * FREQ * (t))                /* Dimensionless stage velocity */
#define STAGE_ACC(t) -2. * pi *FREQ * 2. * pi *FREQ *AMP *cos(2. * pi * FREQ * (t)) /* Dimensionless stage acceleration */

/* --------------------- */
/* Adaptivity parameters */
/* --------------------- */

double u_err = 1e-3, f_err = 1e-3; /* Error thresholds */
int LEVEL = 6;                     /* Level of refinement */

/* ------------------- */
/* Boundary conditions */
/* ------------------- */

// p[top] = dirichlet(0);
// pf[top] = dirichlet(0); 

uf.n[bottom] = 0.;

p[right] = dirichlet(0);
pf[right] = dirichlet(0);

u.t[left] = dirichlet(0);
u.n[left] = dirichlet(0);

f[left] = 0.;

int main()
{
#if CONSERVING
  TOLERANCE = 1e-5;
#endif
  size(L0);

  origin(0., 0.);

  init_grid(1 << LEVEL);

  rho1 = RHO_1, mu1 = MU_1;
  rho2 = RHO_2, mu2 = MU_2;

  f.sigma = SIG;
#if LOOP
  for (OMEGA = 4.0; OMEGA >= 0.1; OMEGA -= 0.1){
    for (GAMMA = 0.2; GAMMA <= 7.0; GAMMA += 0.2){
      run();
    }
  }
#else
  run();
#endif
}

event init(t = 0)
{
  char name[360];
  sprintf(name, "properties_radius%.2e_GAMMA%g_OMEGA%g_freq%g_amp%.2e_tend%g_momentum%d_LEVEL%d",
          radius*1e3, GAMMA, OMEGA, freq, amp*1e3, tend, CONSERVING, LEVEL);
  FILE *fp = fopen(name, "w");

  fprintf(fp, "%-6s %-6s %-6s %-6s %-6s %-6s\n", "RHO_1", "RHO_2", "MU_1", "MU_2", "SIG", "GRAV");
  fprintf(fp, "%-6.2e %-6.2e %-6.2e %-6.2e %-6.2e %-6.2e\n\n", RHO_1, RHO_2, MU_1, MU_2, SIG, GRAV);
  fprintf(fp, "%-6s %-6s\n", "Bo", "Bo_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", Bo, Bo_nd);
  fprintf(fp, "%-6s %-6s\n", "Re", "Re_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", Re, Re_nd);
  fprintf(fp, "%-6s %-6s\n", "We", "We_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", We, We_nd);
  fprintf(fp, "%-6s %-6s\n", "Oh", "Oh_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", Oh, Oh_nd);
  fprintf(fp, "%-6s %-6s\n", "Oha", "Oha_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", Oha, Oha_nd);
  fprintf(fp, "%-6s %-6s\n", "Ca", "Ca_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", Ca, Ca_nd);
  fprintf(fp, "%-6s %-6s %-6s\n", "Time (s)", "Time_nodim", "TOUT");
  fprintf(fp, "%-6.2e %-6.2e %-6.2e\n\n", tend, TEND, TOUT);
  fprintf(fp, "%-6s %-6s\n", "OMEGA (Hz)", "OMEGA_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", OMEGA, OMEGA);
  fprintf(fp, "%-6s %-6s\n", "Grid (m)", "Grid_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", L0 * radius / pow(2, LEVEL), L0 / pow(2, LEVEL));
  fprintf(fp, "%-6s %-6s\n", "Domain (m)", "Domain_nodim");
  fprintf(fp, "%-6.2e %-6.2e\n\n", L0 * radius, L0);
  fprintf(fp, "%-6s %-6s\n", "INITIAL_POS", "INITIAL_VEL");
  fprintf(fp, "%-6.2e %-6.2e\n\n", INITIAL_POS, INITIAL_VEL);
  fprintf(fp, "%-6s %-6s\n", "INITIAL_STAGE_POS", "INITIAL_STAGE_VEL");
  fprintf(fp, "%-6.2e %-6.2e\n\n", STAGE_POS(0), STAGE_VEL(0));

  fclose(fp);

  fraction(f, -sq(x - INITIAL_POS) - sq(y) + sq(RADIUS));

  foreach ()
    u.x[] = f[] * INITIAL_VEL;
}

event adapt(i++)
{
  adapt_wavelet({f, u}, (double[]){f_err, u_err, u_err, u_err}, LEVEL);
}

event acceleration(i++)
{
  face vector av = a;
  foreach_face(x)
      av.x[] -= (GRAV - STAGE_ACC(t));
}

event logfile(t += TOUT, t <= TEND)
{
  char datafile1[360];
  double xc = 0., yc = 0., uc = 0., vc = 0., wd = 0., hd = 0.;
  double vol1 = 0.;
  double x2 = 0., x1 = 0., y1 = 0., xmin = 100.;

  coord us = {STAGE_VEL(t), 0., 0.};

  sprintf(datafile1, "./data_radius%.2e_GAMMA%g_OMEGA%g_freq%g_amp%.2e_tend%g_momentum%d_LEVEL%d",
          radius*1e3, GAMMA, OMEGA, freq, amp*1e3, tend, CONSERVING, LEVEL);

  static FILE *fp1 = fopen(datafile1, "w");

  if (t == 0)
  {
    fprintf(fp1, "%-6s %-6s %-6s %-6s %-6s %-6s %-6s %-6s %-6s %-6s %-6s %-6s\n\n",
            "#1 time", "2 x_stage", "3 x_center", "4 y_center", "5 width", "6 height", "7 air layer height", "8 ux_stage", "9 gx_stage", "10 ux_center", "11 uy_center", "12 Webber");
  }

  foreach (reduction(+:xc) reduction(+:yc) reduction(+:uc) reduction(+:vc)
    reduction(+:vol1))
  {
    double dv1 = 2. * pi * f[] * dv();
    uc += (u.x[] - us.x) * dv1;
    vc += u.y[] * dv1;
    xc += x * dv1;
    yc += y * dv1;
    vol1 += dv1;
  }

  vector h[];
  heights(f, h);

  foreach (reduction(max:y1) reduction(min:xmin))
  {
    if ((h.y[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)) & (x < (xc / vol1 + Delta)) & (x > (xc / vol1 - Delta)))
    {
      double pos = y + Delta * height(h.y[]);
      if (pos > yc / vol1)
        y1 = pos;
    }
    if ((h.x[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)))
    {
      double pos = x + Delta * height(h.x[]);
      if (pos < xmin)
        xmin = pos;
    }
  }
  foreach_boundary(bottom, reduction(max:x1) reduction(max:x2))
  {
    if ((h.x[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)))
    {
      double pos = x + Delta * height(h.x[]);
      if (pos > xc / vol1)
        x1 = pos;
      if (pos < xc / vol1)
        x2 = pos;
    }
  }

  wd = 2. * fabs(y1);
  hd = fabs(x1 - x2);
  
  fprintf(fp1, "%-6g %-6g %-6g %-6g %-6g %-6g %-6g %-6g %-6g %-6g %-6g %-6g\n",
          t * T_unit / period, STAGE_POS(t), ((xc / vol1) + STAGE_POS(0)), yc / vol1, wd, hd, xmin, STAGE_VEL(t), STAGE_ACC(t), uc, vc, RHO_1*sq(uc)*RADIUS/SIG);

  fflush(fp1);
}