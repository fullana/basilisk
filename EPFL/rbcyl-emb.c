#include "grid/octree.h"
#include "embed.h"
#include "navier-stokes/centered.h"
#include "tracer.h"
#include "diffusion.h"
#include "radial.h"
#include "navier-stokes/perfs.h"
// #include "view.h"
#include "display.h"

#define MAXLEVEL 5
#define MINLEVEL 5
#define Tbot 0.5
#define Ttop -0.5
#define Height (1.)
#define EndTime 200.

scalar T[];
scalar * tracers = {T};

face vector muc[], av[];

double Ra, Pr;

int main (int argc, char * argv[])
{
  size (2.75*Height);//R0
  dtheta=2*pi;
  Z0=-Height/2. + (L0/pow(2,MAXLEVEL+1))/2.;// to avoid superposition between grid and embedded boundary
  init_grid (1 << 7);
  unrefine(z>1.2*Z0 && level >= MINLEVEL);
  a=av;
  mu=muc;
  Ra = 3e3; Pr = 0.71;
  DT = 0.1;
  run();
}


//T[front] = dirichlet(-0.5);
T[back] = dirichlet(Tbot);
T[left] = neumann(0.);
T[right] = neumann(0.);
T[top] = neumann(0.);
T[embed] = dirichlet(Ttop);
T[bottom] = neumann(0.);


u.n[top] = dirichlet(0.);
u.n[embed] = dirichlet(0.);
u.n[bottom] = dirichlet(0.);
u.n[right] = dirichlet(0.);
u.n[left] = dirichlet(0.);
//u.n[front] = dirichlet(0.);
u.n[back] = dirichlet(0.);

u.t[top] = dirichlet(0.);
u.t[embed] = dirichlet(0.);
u.t[bottom] = dirichlet(0.);
u.t[right] = dirichlet(0.);
u.t[left] = dirichlet(0.);
//u.t[front] = dirichlet(0.);
u.t[back] = dirichlet(0.);

u.r[top] = dirichlet(0.);
u.r[embed] = dirichlet(0.);
u.r[bottom] = dirichlet(0.);
u.r[right] = dirichlet(0.);
u.r[left] = dirichlet(0.);
//u.r[front] = dirichlet(0.);
u.r[back] = dirichlet(0.);

event init (t=0) {
  solid (cs, fs, (+Height/2. + (L0/pow(2,MAXLEVEL+1))/2.)- z);
  foreach(){
    T[] = Tbot/Z0*z;
    foreach_dimension()
      u.x[] = 0.001*noise();
  }
  boundary ({T,u});
  //dump("init");
}

event properties (i++) {
  foreach_face()
    muc.x[] = fm.x[]*Pr/sqrt(Ra);
  //boundary ((scalar*){muc});
}


event tracer_diffusion (i++) {
  face vector D[];
  foreach_face()
    D.x[] = fm.x[]*1./sqrt(Ra);
  boundary ((scalar*){D});
  diffusion (T, dt, D);
  boundary ({T});
}

event acceleration (i++) {
  foreach_face(z)
    av.z[] += fm.z[]*Pr*(T[] + T[0,0,-1])/2.;
  foreach_face(y)
    av.y[] += 0.;
  foreach_face(x)
    av.x[] += 0.;
}

scalar un[];
event init_un (i = 0) {
  foreach()
    un[] = u.z[];
}

event logfile(i++){
  scalar div[];
  double deltau = change (u.z, un);
  double avg = normf(u.z).avg, du = deltau/(avg + SEPS);
  foreach() {
    div[] = 0.;
    foreach_dimension()
      div[] += u.x[1] - u.x[];
    div[] /= Delta;
   }
  stats s0 = statsf (div);
  fprintf (stdout, "%f %.9g %.9g %.9g %.9g %.9g %.9g %.9g \n", t, deltau, avg, du, s0.sum/s0.volume, s0.max, statsf(u.z).sum, normf(p).max);
  fflush(stdout);
}

#if 1
event adapt (i++){
  double err = 0.01;
  astats s = adapt_wavelet ((scalar*){cs, T, u},
		 (double[]){err*0.1, err, err, err, err}, MAXLEVEL, MINLEVEL);
  fprintf (stderr, "# refined %d cells, coarsened %d cells\n", s.nf, s.nc);
}
#endif

void radial (coord * p) {
  double r = p->x, theta = p->y*dtheta/L0;
  p->x = r*cos(theta), p->y = r*sin(theta);
}

event movie (t += 1.; t<=EndTime) {
  clear();
  view (quat = {-0.078, 0.599, 0.753, -0.261}, map = radial,
      fov = 30, near = 0.01, far = 1000,
      tx = -0.022, ty = 0.031, tz = -2.943,
      width = 1620, height = 960);
  squares (color = "u.z");
  box ();
  cells ();
  save("T.mp4");
}

event movie2(t += 1.; t<=EndTime){
  clear();
  view (quat = {-0.078, 0.599, 0.753, -0.261}, map = radial,
      fov = 30, near = 0.01, far = 1000,
      tx = -0.022, ty = 0.031, tz = -2.943,
      width = 1620, height = 960);
  box ();
  cells ();
//   isosurface (f = "T", color = "T");
  save("T2.mp4");
}

#if 0
#if TRACE > 1
event profiling (i += 19) {
  static FILE * fp = fopen ("profiling", "w");
  trace_print (fp, 1);
}

event profiling_plot (i = 40) {
  if (getenv ("DISPLAY"))
    popen ("gnuplot -e 'set term x11 noraise noenhanced title profiling' "
	   "$BASILISK/profiling.plot "
	   "& read dummy; kill $!", "w");
}
#endif
#endif

/**

## Results
![u.z field.](rbcyl-emb/T.mp4)
![isosurface T field.](rbcyl-emb/T2.mp4)

*/