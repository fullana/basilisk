#define JVIEW 1
#define NAVIER 1


// #include "grid/multigrid.h"
#include "../sandbox/tfullana/embed-robin.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "../sandbox/tfullana/contact-embed.h"
#if JVIEW
#include "display.h"
#endif

#define Re 3.987
#define Ca 1.06
#define Cn 0.01
#define Pe 100.
#define ksi 1e-2
#define u_plates 0.1

#define ratio 4

#define rho_1 1.
#define mu_1 (1./Re)
#define sig (mu_1/Ca)

#define rho_2 (rho_1*ksi)
#define mu_2 (mu_1*ksi)

#define EPS 1e-14

double theta0 = 90., dynamic_angle = pi/2.;
double l = 0.1;
double f_err = 1e-3, u_err = 1e-3;

int LEVEL = 8;

scalar un[];

u.n[embed] = (y > 0 ? robin(l, u_plates) : robin(l, -u_plates));
u.t[embed] = dirichlet(0.);


scalar c[];

int main() {
    periodic(right);
    // stokes = true;
    origin (-ratio/2, -ratio/2);
    size(ratio);

    N = 64;

    slip = l;

    f.sigma = sig;
    contact_angle = c;
    rho1 = rho_1; rho2 = rho_2;
    mu1 = mu_1; mu2 = mu_2;
    run();
}

event init (t = 0) {
    solid (cs, fs,  intersection(0.5 - y - EPS, 0.5 + y - EPS));
    fraction (f, -(x-0.5)*(x+0.5));

    foreach(){
        if ((f[] > 1e-6) && (f[] < (1. - 1e-6)) && (cs[] > 1e-6)) {
            if (y > 0){
                if (x > 0)
                    c[] = 1.;
                else
                    c[] = 2.;
            }
            else{
                if (x < 0)
                    c[] = 1.;
                else
                    c[] = 2.;
            }
        }
        fprintf(stdout,"%g\n", c[]);
    }
        

}


event adapt (i++) {
  adapt_wavelet ({f,cs,u}, (double[]){f_err,f_err,u_err,u_err,u_err}, LEVEL);

    foreach(){
        if ((f[] > 1e-6) && (f[] < (1. - 1e-6)) && (cs[] > 1e-6)) {
            if (y > 0){
                if (x > 0)
                    c[] = pi;
                else
                    c[] = pi;
            }
            else{
                if (x < 0)
                    c[] = pi;
                else
                    c[] = pi;
            }
        }
        fprintf(stdout,"%g\n", c[]);
    }
}


event test_end (t = 1){
    char name[360];
    #if NAVIER
    sprintf(name, "slip%d_l%g_u%g", NAVIER, l, u_plates);
    #else
    sprintf(name, "dirichlet");
    #endif
    FILE *fp = fopen(name, "w");

    scalar ff[];

    foreach(){
        if (fabs(y) < 0.5)
            ff[] = f[]*cs[];
    }

    output_facets(f, fp);
    dump("test");
}

