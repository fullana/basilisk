// same as poiseuille.c but with periodic boundary conditions

#include "embed.h"
// #include "grid/multigrid.h"
#include "navier-stokes/centered.h"

// #include "display.h"

double lambda = 0.0;

#define newrobin(a,b,c) ((dirichlet ((c)*Delta/(2*(b) + (a)*Delta))) + ((dirichlet (0))*(-(2*(b) - (a)*Delta)/(2*(b) + (a)*Delta) - 1.)))

// #define newrobin(a,b,c) (dirichlet(Delta * ((c - 2.*b)/(2.*b + Delta))) + ((2.*b - Delta)/(2.*b + Delta) - 1.)*dirichlet(Delta))

int main() {
  origin (-0.5, 0);
  periodic (top);
  // dimensions (nx = 1);
  
  stokes = true;
  TOLERANCE = 1e-5;
  
  slip = lambda;

//   u.t[left] = newrobin(1., lambda, 0); //robin(lambda, 0);
//   u.t[right] = newrobin(1., lambda, 0); //robin(lambda, 0);

  u.n[embed] = dirichlet(0);
  u.t[embed] = dirichlet(0);

  for (N = 8; N <= 64; N *= 2)
    run();
}

scalar un[];

event init (t = 0) {
  solid (cs, fs,  intersection((0.25) - x, (0.25) + x));
  // we also check for hydrostatic balance
  const face vector g[] = {1.,1.};
  a = g;
  const face vector muc[] = {1.,1.};
  mu = muc;
  foreach()
    un[] = u.y[];
}

// event logfile (t += 0.1; i <= 100) {
//   double du = change (u.y, un);
//   if (i > 0 && du < 1e-6)
//     return 1; /* stop */
// }

event profile (i = 100) {
  printf ("\n");
  foreach()
    fprintf (stdout, "%g %g %g %g %g\n", x, y, u.x[], u.y[], p[]);
  scalar e[];
  double a = 1.0;
  foreach(){
    e[] = u.y[] - a*(0.25 + lambda - x*x);
    fprintf (stdout, "%g %g %g %g %g\n", x, y, u.x[], a*(0.25 + lambda - x*x), p[]);
  }
  norm n = normf (e);
  fprintf (stderr, "%d %g %g %g\n", N, n.avg, n.rms, n.max);
}
