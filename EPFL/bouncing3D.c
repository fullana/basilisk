/* Flags */

#define OIL 1
#define WATER 0

#define CONSERVING 0
#define REDUCED 0
#define EMBED 0
#define HEIGHTS 1

#define JVIEW 0
#define LOOP 0
#define SAVEDUMP 1

/* Header files */

#include "grid/octree.h"
#if EMBED
#include "embed.h"
#endif
#include "navier-stokes/centered.h"
#include "two-phase.h"
#if CONSERVING
#include "navier-stokes/conserving.h"
#endif
#include "tension.h"
#if !LOOP
#include "navier-stokes/perfs.h"
#endif
#if REDUCED
#include "reduced.h"
#endif
#if HEIGHTS
#include "contact.h"
#endif

#if JVIEW
#include "display.h"
#else
#include "view.h"
#endif

/* --------------------------- */
/* Dimensional properties (SI) */ 
/* --------------------------- */ 

/* Oil or Water in fluid 1 */ 
#if OIL
#define rho_1 1.04e3
#define mu_1 20e-3
#define sig 20e-3 
#elif WATER
#define rho_1 997.              
#define mu_1 1.0016e-3   
#define sig 72.86e-3       
#endif

/* Air in fluid 2 */
#define rho_2 1.2047           
#define mu_2 1.8205e-5         

#define grav 9.8067                               /* Acceleration of gravity */           
#define diameter 2e-3                             /* Experimental drop diameter */            
#define u_ref 0.1                                 /* Reference velocity */                     
double amp = 5e-4;                                /* Stage amplitude */        
double freq = 30.;                                /* Stage frequency */ 

#define tend 0.2                                  /* Final time */ 
#define tout 0.0001                               /* Extract every */

/* ----------------------------- */
/* Dimensionless properties (SI) */ 
/* ----------------------------- */ 

#define MU_unit (rho_1*u_ref*diameter)            /* Scaling viscosity */
#define SIGMA_unit (rho_1*diameter*u_ref*u_ref)   /* Scaling surface tension */
#define T_unit (diameter/u_ref)                   /* Characteristic time scale */
#define G_unit (diameter/(T_unit*T_unit))         /* Scaling gravity */
#define RHO_1 1.                                  /* Dimensionless density 1 */
#define RHO_2 (rho_2/rho_1)                       /* Dimensionless density 2 */
#define MU_1 (mu_1/MU_unit)                       /* Dimensionless viscosity 1 */
#define MU_2 (mu_2/MU_unit)                       /* Dimensionless viscosity 2 */
#define SIG (sig/SIGMA_unit)                      /* Dimensionless surface tension */
#define GRAV (grav/G_unit)                        /* Dimensionless gravity */
#define DIAMETER 1.                               /* Dimensionless diameter */
#define U 1.                                      /* Dimensionless velocity */
#define AMP (amp/diameter)                        /* Dimensionless stage amplitude */
#define FREQ (freq*T_unit)                        /* Dimensionless stage frequency */
#define TEND (tend/T_unit)                        /* Dimensionless final time */ 
#define TOUT (tout/T_unit)                        /* Dimensionless extraction time data file*/
#define TDUMP (TOUT*100.)                         /* Dimensionless extraction time dump file*/
#define L0 (3.5*DIAMETER)                          /* Dimensionless domain size */
double INITIAL_POS = 2.5;                         /* Dimensionless initial position */
double INITIAL_VEL = 0.0;                         /* Dimensionless initial velocity */

#define stage_pos(t) AMP*sin(2.*pi*FREQ*(t))              /* Dimensionless stage position */
#define stage_vel(t) 2.*pi*FREQ*AMP*sin(2.*pi*FREQ*(t))   /* Dimensionless stage velocity */

/* --------------------- */
/* Adaptivity parameters */ 
/* --------------------- */ 

double u_err = 1e-3, f_err = 1e-3;                /* Error thresholds */
int LEVEL = 8;                                    /* Level of refinement */


/* ------------------- */
/* Boundary conditions */ 
/* ------------------- */

p[top] = dirichlet(0);
pf[top] = dirichlet(0); 

p[bottom] = dirichlet(0);
pf[bottom] = dirichlet(0);

p[front] = dirichlet(0);
pf[front] = dirichlet(0); 

p[back] = dirichlet(0);
pf[back] = dirichlet(0);

u.t[left] = dirichlet(0);
u.n[left] = dirichlet(stage_vel(t));
f[left] = 0.;

#if EMBED
u.t[embed] = dirichlet(0);
u.n[embed] = dirichlet(stage_vel(t));
f[embed] = 0.;
#endif

#if HEIGHTS
vector h[];
h.t[left] = contact_angle(pi);
h.r[left] = contact_angle(pi);
#endif

scalar omega[];

int main() {
#if CONSERVING
    TOLERANCE = 1e-4;
#endif
    size (L0);

    init_grid (1 << 6);

    rho1 = RHO_1, mu1 = MU_1;
    rho2 = RHO_2, mu2 = MU_2;
    
    f.sigma = SIG;
#if HEIGHTS
    f.height = h;
#endif
#if REDUCED
    G.x = -GRAV;
    Z.x = 1.;
#endif

#if LOOP
    for (LEVEL = 7; LEVEL < 9; LEVEL++) {
      amp = 0.;
      run();
    }
#else
    amp = 0.;
    run();
#endif
}

event init (t = 0) {
    char name[360];
    sprintf (name, "props_OIL%d_WATER%d_dia%g_amp%g_freq%g_LEVEL%d_uerr%g_ferr%g_CON%d_RED%d_EMB%d_HEI%d", 
      OIL, WATER, diameter, amp, freq, LEVEL, u_err, f_err, CONSERVING, REDUCED, EMBED, HEIGHTS);
    FILE * fp = fopen(name, "w");

    fprintf(fp,"%-12s %-12s %-12s\n", "MU_UNIT", "SIGMA_UNIT", "T_UNIT");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e\n\n", MU_unit, SIGMA_unit, T_unit);
    fprintf(fp,"%-12s %-12s %-12s %-12s %-12s %-12s\n", "RHO_1", "RHO_2", "MU_1", "MU_2", "SIG", "GRAV");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e %-12.2e %-12.2e %-12.2e\n\n", RHO_1, RHO_2, MU_1, MU_2, SIG, GRAV);
    fprintf(fp,"%-12s %-12s\n", "Re", "Re_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", (rho_1*diameter*u_ref)/mu_1, (RHO_1*DIAMETER*U)/MU_1);
    fprintf(fp,"%-12s %-12s\n", "Ca", "Ca_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", mu_1*u_ref/sig, MU_1*U/SIG);
    fprintf(fp,"%-12s %-12s\n", "Bo", "Bo_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", (rho_1-rho_2)*grav*diameter*diameter/sig, (RHO_1-RHO_2)*GRAV*DIAMETER*DIAMETER/SIG);
    fprintf(fp,"%-12s %-12s %-12s\n", "Time (s)", "Time_nodim", "TOUT");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e\n\n", tend, TEND, TOUT);
    fprintf(fp,"%-12s %-12s %-12s %-12s\n", "Amp (m)", "Amp_nodim", "L0", "INITIAL_POS");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e %-12.2e\n\n", amp, AMP, L0, INITIAL_POS);
    fprintf(fp,"%-12s %-12s\n", "Freq (Hz)", "Freq_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", freq, FREQ);
    fprintf(fp,"%-12s %-12s\n", "Grid (m)", "Grid_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", L0*diameter/pow(2,LEVEL), L0*DIAMETER/pow(2,LEVEL));
    fprintf(fp,"%-12s %-12s\n", "Domain (m)", "Domain_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", L0*diameter, L0);
    fprintf(fp,"%-12s %-12s\n", "INITIAL_POS", "INITIAL_VEL");
    fprintf(fp,"%-12.2e %-12.2e\n\n", INITIAL_POS, INITIAL_VEL);

    fraction (f, - sq(x - INITIAL_POS) - sq(y - 0.5*L0) - sq(z - 0.5*L0) + sq(DIAMETER/2));

    foreach()
      u.x[] = INITIAL_VEL;

#if EMBED
  solid (cs, fs, (x - L0/pow(2,LEVEL)));
  adapt_wavelet ({f,cs,u}, (double[]){f_err,0.,u_err,u_err,u_err}, LEVEL);
#else
  adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err}, LEVEL);
#endif

    fclose(fp);
}

event adapt (i++) {
#if EMBED
  adapt_wavelet ({f,cs,u}, (double[]){f_err,0.,u_err,u_err,u_err}, LEVEL);
#else
  adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err}, LEVEL);
#endif
}

#if !REDUCED
event acceleration (i++) {
  face vector av = a;
  foreach_face(x)
    av.x[] -= GRAV;
  vorticity(u, omega);
  foreach_boundary(left)
    u.x[] = stage_vel(t);
}
#endif

#if SAVEDUMP
event dumpfile (t+=TDUMP) {
  char facetsfile[360], dumpfile[360];

  if (t==0){
    mkdir("dir_facets",0777);
    mkdir("dir_dump",0777);
  }

  sprintf (facetsfile, "./dir_facets/facets_OIL%d_WATER%d_dia%.2e_amp%.2e_freq%.2e_LEVEL%d_uerr%.2e_ferr%.2e_CON%d_RED%d_EMB%d_HEI%d_t%g_%d", 
      OIL, WATER, diameter, amp, freq, LEVEL, u_err, f_err, CONSERVING, REDUCED, EMBED, HEIGHTS, t, tid());
  sprintf (dumpfile, "./dir_dump/dump_OIL%d_WATER%d_dia%.2e_amp%.2e_freq%.2e_LEVEL%d_uerr%.2e_ferr%.2e_CON%d_RED%d_EMB%d_HEI%d_t%g", 
      OIL, WATER, diameter, amp, freq, LEVEL, u_err, f_err, CONSERVING, REDUCED, EMBED, HEIGHTS, t);

  FILE * fp1 = fopen(facetsfile, "w");

  output_facets (f, fp1);

  fclose(fp1);

  dump(dumpfile);
}
#endif

event logfile (t+=TOUT,t<=TEND) {
  char datafile[360], vofmovie[360];
  double xc = 0., yc = 0., zc = 0., uc = 0., vc = 0., wc = 0., sd = 0., ke = 0., gpe = 0., wd = 0., hd = 0.;

  sprintf (datafile, "data_OIL%d_WATER%d_dia%.2e_amp%.2e_freq%.2e_LEVEL%d_uerr%.2e_ferr%.2e_CON%d_RED%d_EMB%d_HEI%d", 
      OIL, WATER, diameter, amp, freq, LEVEL, u_err, f_err, CONSERVING, REDUCED, EMBED, HEIGHTS);
  sprintf (vofmovie, "vof_OIL%d_WATER%d_dia%.2e_amp%.2e_freq%.2e_LEVEL%d_uerr%.2e_ferr%.2e_CON%d_RED%d_EMB%d_HEI%d.mp4", 
      OIL, WATER, diameter, amp, freq, LEVEL, u_err, f_err, CONSERVING, REDUCED, EMBED, HEIGHTS);

  static FILE * fp1 = fopen(datafile, "w");

  if (t==0){
    fprintf(fp1,"%-12s\n", "#data file");
    fprintf(fp1,"%-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
      "#t", "#stagepos", "#stagevel", "#xcenter", "#ycenter", "#zcenter", "#ucenter", "#vcenter", "#wcenter", "#volume", "#width", "#height", "#kineticE", "#potentialE");
  }

  foreach(reduction(+:xc) reduction(+:yc) reduction(+:zc) reduction(+:uc) reduction(+:vc) reduction(+:wc) reduction(+:sd) reduction(+:ke) reduction(+:gpe)) {
    double norm2 = 0.;
    foreach_dimension()
      norm2 += sq(u.x[]);
    double dv = f[]*dv();
    uc += u.x[]*dv;
    vc += u.y[]*dv;
    wc += u.z[]*dv;
    xc += x*dv;
    yc += y*dv;
    zc += z*dv;
    sd += dv;
    ke += rho[]*norm2*dv;
    gpe += rho[]*GRAV*x*dv;
  }

  #if HEIGHTS
  double x2 = 0., x1 = 0., y2 = 0., y1 = 0.;
  foreach(reduction(max:x1) reduction(max:y1) reduction(max:x2) reduction(max:y2)){
    if ((h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x < (xc/sd + Delta)) & (x > (xc/sd - Delta)) & (z < (zc/sd + Delta)) & (z > (zc/sd - Delta))){
      double pos = y + Delta*height(h.y[]);
			if (pos > yc/sd)
        y1 = pos;
      if (pos < yc/sd)
        y2 = pos;
    }
    if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (y < (yc/sd + Delta)) & (y > (yc/sd - Delta)) & (z < (zc/sd + Delta)) & (z > (zc/sd - Delta))){
      double pos = x + Delta*height(h.x[]);
			if (pos > xc/sd)
        x1 = pos;
      if (pos < xc/sd)
        x2 = pos;
    }
  }
  wd = fabs(y1 - y2);
  hd = fabs(x1 - x2);
  #endif

  fprintf(fp1,"%-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
    t, stage_pos(t), stage_vel(t), xc/sd, yc/sd, zc/sd, uc/sd, vc/sd, wc/sd, sd, wd, hd, ke, gpe);

  fflush(fp1);

  view (quat = {0.000, 0.000, -0.707, 0.707},
        fov = 30, near = 0.01, far = 1000,
        tx = 0.603, ty = -0.489, tz = -3.082,
        width = 1024, height = 576);
  box ();
  cells ();
  draw_vof (c = "f", fc = {1,1,1});
  save (vofmovie);
}