#define mufThetaBC 0
#define mufUBC 1

//#include "display.h"
#include "axi.h"
#include "navier-stokes/centered.h"
#include "contact.h"
#include "two-phase.h"
#include "tension.h"
#include "reduced.h"
//#include "view.h"

//dimensioned parameters
#define rho_l 1000.
#define rho_g 1.2
#define sig 0.072 //surface tension
#define mu_l 1e-3  //water viscosity
#define mu_g 1.7e-5  //air viscosity
#define mu_f 1.
#define gravity 9.81
#define Vd  1.0e-8   //drop volume
#define radiusd pow((3.*Vd/(4.*pi)),1./3.)  //drop radius
#define radius0 pow((3.*Vd/(pi*(2.+cos(theta_s))*pow((1.-cos(theta_s)),2.))),1./3.)

//dimensionless parameters
#define RHO (rho_l/rho_l)
#define RHOg (rho_g/rho_l)
#define SIGMA (sig/sig)
#define MU (mu_l/sqrt(rho_l*sig*radiusd))
#define MUg (mu_g/sqrt(rho_l*sig*radiusd))
#define MUf (mu_f/sqrt(rho_l*sig*radiusd))
#define Rd (radiusd/radiusd)   //radius0 ~ Rd
#define R0 (radius0/radiusd)
#define GRAV (gravity/(sig/(rho_l*sq(radiusd))))


//contact angle
#define theta_s (105.*pi/180.)
#define theta_a (115.*pi/180.)
#define theta_r (95.*pi/180.)
#define CAH (10.*pi/180.)
#define theta_ini (109.*pi/180.)

double theta_d = 109.*pi/180.;

///////////oscillation setting////////////
#define freq 70.  //freq: actual frequency
#define amp 0.0002  //amplitute
//non-dimensionalization
#define FREQ (freq/sqrt(sig/(rho_l*cube(radiusd))))
#define AMP (amp/radiusd)
//////////////////////////////////////////

//simulation setting
#define LEVEL 9
#define t_end (10./FREQ)   //

//for mesh refinement
double femax = 0.001;
double uemax = 0.001;
double L = 1.e-06/radiusd;  //macroscale length
double lambda2 = 1.e-9/radiusd;  //microscale length

//////////////////DCA models//////////////////
double muf_tmp;
double v_tmp = 0.; //velocity at last time step
double mufHigh = 10./sqrt(rho_l*sig*radiusd); //high value in the sticking region
double mufLow = 0.5/sqrt(rho_l*sig*radiusd); //low value outside the sticking region
#if mufThetaBC
double uDelta = 0.5;
double ulim = 0.025;
//x: vel0
double H1(double x){
  return (1. + 0.5*(tanh((x - ulim)/uDelta) - ((x + ulim)/uDelta)));
}
double polyN = -0.8;
double dpolyN = 0.6;
//variables: x:vel0, y:phaseV
double MUFTheta(double x, double y){
    double c;
    //c = (y > 0.? dpolyN : 0.);
    //return ((mufHigh - mufLow)*(1. -  pow(tanh(x/uDelta),2.)) + mufLow);  
   // return ((mufHigh - mufLow)*(1. -  0.5*tanh(fabs(x)/uDelta)) + mufLow); 
   //c = (y > 0.? 1. : 0.);
  //return (mufHigh*(5. -  4.*tanh(fabs(x)/(uDelta))) );
    //return (mufHigh*(1. - H1(x)) + mufLow*H1(x));
   // return ( fabs(x/0.5) > 1e-5? mufHigh*pow(fabs(x/0.5), (polyN + c)) : mufHigh*pow(1e-5, (polyN + c)));
   return (5./sqrt(rho_l*sig*radiusd));
}
//variables: x: muf_tmp, y: velocity
double mTheta(double x, double y){
  return (theta_s*180./pi + x*y/SIGMA)*pi/180.;
}
#elif mufUBC
double DeltaTheta = 1.*pi/180.;
//variables: x: theta_d
double H(double x){
  return (1. + 0.5*(tanh((x - theta_a)/DeltaTheta) - tanh((x - theta_r)/DeltaTheta)));
}
double MUFU(double x){
    //return (mufHigh*(1. - H(x)) + mufLow*H(x)); 
    return (0.1/sqrt(rho_l*sig*radiusd)); 
}
// velocity boundary
//b:eps
double bell(double y, double b){
	//return ((1. - pow(tanh(y/b), 2))/b);
  return (fabs(y) < b ? pow(0.5*(1. + cos(pi*y/b)), 2.) : 0.);
}
double Young_stress(double y, double eps, double theta, double theta_eq){
	return bell(y, eps)*(cos(theta_eq) - cos(theta));
}

scalar gnbc[];

#endif

double eps = 0.01;

/////////////////////////////////////////////
vector h[];
h.t[left] = contact_angle (theta_d);
#if mufUBC
u.t[left] = robin(eps, gnbc[]);
//u.t[left] = dirichlet(gnbc[]);
#endif

u.n[left]  = dirichlet(0.);
u.n[right]  = dirichlet(0.);

int main() {
	  N = 64;
      L0 = 4.;
      origin (0., 0.);
  
      f.sigma = SIGMA;
      f.height = h;

      G.x = - GRAV;

      slip = eps;	
      rho1 = RHO; rho2 = RHOg;
      mu1 = MU; mu2 =MUg;  
      run();
  
}

event init (t = 0) {
  if (!restore (file = "restart")) { 
	refine ( level < LEVEL);
	//fraction (f, -(sq((x - axisH)/axisS)+sq(y/axisL)-1.));
 	fraction (f, -(sq((x + R0*cos(theta_s)))+sq(y)-sq(R0)));
  }
  boundary ({f});
	char name[80];
	sprintf (name, "init_snap-%d", LEVEL);
    dump(name);
}



event impose (i++; t <= t_end){
	char name[80];
	sprintf (name, "Info-%d", LEVEL);
	static FILE * fp = fopen(name, "w");
	double KE = 0., VD = 0., area=0., vol=0., xd=0., radius = 0., vel0=0., vel1=0., Ca=0.,We=0., topx = 0., Utop = 0.;
  double deltaV = 0.; //deltaV = U_new - U_old (acceleration)
  double phaseV; //phaseV = deltaV*vel0;

    scalar px[], py[],pcl[];
    position (f,px,{1,0});
    position (f,py,{0,1});
    position (f,pcl,{0,1});

  foreach(reduction(+:area) reduction(+:vol) reduction(+:KE) reduction(+:VD) reduction(+:xd)) {
    if (f[] <= 1e-6 || f[] >= 1. - 1e-6)
    {
      px[] = nodata;
      py[] = nodata;
    }

    if (f[] <= 1e-6 || f[] >= 1. - 1e-6 || x > L/pow(2,LEVEL) ){
      pcl[] = nodata;
    }
    
    if (f[] > 1e-6 && f[] < 1. - 1e-6) {
      // interfacial area
      coord n = interface_normal (point, f), p;
      double alpha1 = plane_alpha (f[], n);
      area += pow(Delta, 1.)*plane_area_center (n, alpha1, &p)*2.*pi*py[];
    }
      double dv_axi = pow(Delta, 2.)*2.*pi*y;
      KE += 0.5*dv_axi*(sq(u.x[]) + sq(u.y[]))*rho(f[]); //kinetic energy
      //viscous dissipation
      VD += dv()*(sq(u.x[1] - u.x[-1]) + sq(u.x[0,1] - u.x[0,-1]))/sq(2.*Delta);

	if (f[] > 1e-6) {
		//volume
      vol += dv_axi*f[];
      // centroid
      xd += dv_axi*f[]*x;
      foreach_dimension() {
      KE += dv_axi*sq(u.x[]); //kinetic energy
      //viscous dissipation
      VD += dv()*(sq(u.x[1] - u.x[-1]) + sq(u.x[0,1] - u.x[0,-1]))/sq(2.*Delta);
      }
	}
	}
  KE /= 2.;
  xd /= vol;
  VD *= MU/vol;

	double angle_extracted = 0., angle_above = 0., angle_extrapolated = 0.;
	double curv = 0.;
	scalar kappa[];
	curvature (f,kappa);

  foreach(reduction(max:angle_extrapolated) reduction(max:angle_above) reduction(max:curv)){
		if ((x <= (2.*Delta)) & (x > (Delta)) & (h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> 1e-6)){
				coord n = {0};
				n = interface_normal (point,f);
        curv = kappa[];
				angle_above = atan2(n.y, n.x);
				double tmp = pow(((h.y[1,0] - h.y[])/1.0), 2);
				angle_extrapolated = angle_above + 1.5*Delta*kappa[]*sqrt(1 + tmp)/sin(angle_above);  
		}
	}

	foreach_boundary(left, reduction(+:radius) reduction(+:vel0) reduction(+:Ca) reduction(+:We) reduction(+:angle_extracted)){
		if ((y > 0.) & (h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
			radius = y + Delta*height(h.y[]);
			vel0 = u.y[]; 
      //vel1 = (radius - r1_tmp);
      //vel1 = ((u.y[-2,0]+u.y[2,0]) /2);
      //vel2 = ((u.y[-1,0]+2.*u.y[]+u.y[1,0]) /4);
      //r2_tmp = r1_tmp;
      //r1_tmp = radius;
      //deltaV = radius + r2_tmp - 2.*r1_tmp;
      //phaseV = sign(deltaV*vel0);
			Ca = MU*vel0/SIGMA;
			We = (RHO*(pow(vel0,2))*1.)/SIGMA;
			coord n = {0};
			n = interface_normal (point,f);
			angle_extracted = atan2(n.y, n.x);
		}

	}

	foreach_boundary(bottom, reduction(max:topx)){
        if ((f[]< (1. - 1e-3)) & (f[]> (1e-3) )){
        topx =  x + Delta*height(h.x[]);
          }
      }
  #if mufThetaBC
  muf_tmp = MUFTheta(vel0, phaseV);
  theta_d = mTheta(muf_tmp,vel0);
  #elif mufUBC
  muf_tmp = MUFU(angle_extracted);
  foreach_boundary(left){
		double relative_position = y - radius;
    gnbc[] = eps*(SIGMA/muf_tmp)*Young_stress(relative_position, eps, angle_extracted, theta_s);
    theta_d = theta_s + (muf_tmp*vel0/SIGMA)*pi/180.;

	}
  #endif

	fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",t, dt, radius, statsf(py).max, statsf(px).max, statsf(f).sum, Ca, vel0, We, theta_d/pi*180., 180.*angle_extracted/pi, xd, topx, Utop, vol, KE, VD, area, curv);
    	fflush(fp);
}

// event interface (t += 0.02/FREQ; t = 80./FREQ; t <= t_end) {

//   char name[80];
//   sprintf (name, "infc-%g-%d", t,pid());
//   FILE * fp1 = fopen (name, "w");
//   output_facets (f,fp1);
//   fclose(fp1);
//   char command1[80];
//   sprintf(command1,"LC_ALL=C cat infc-* > Interface-%g",t);
//   system(command1);
//   char command2[80];
//   sprintf(command2,"rm infc*");
//   system(command2);
// }

// event dumpALL (t += 0.02/FREQ; t = 13./FREQ; t <= t_end){
  
//   char name1[80];
//   sprintf (name1, "dump-%03d", (int) t);
//   dump (file = name1);
// }

// event movie (t += 0.1/FREQ; t = 50./FREQ; t <= t_end) {
//   /*view (tx = 0.15*1.145, ty = -0.4*1.145, psi = -90.*pi/180., width = 1600, height = 800);
//   draw_vof ("f",lw = 2.);
//   squares("u.y", linear=true);
//   cells(lc = {0.8, 0.4, 0.1}, lw = 0.5);
//   begin_mirror ({0,1});
//   draw_vof ("f",lw = 2.);
//   box();
//   end_mirror();
//   save("osc.mp4");*/
//   output_ppm (f,file = "f.mp4",linear = true, n = 1024);
//   output_ppm (u.y,file = "Uy.mp4",linear = true, n = 1024);
// }

event acceleration (i++) {
  G.x = (0. - AMP*sq(2.*pi*FREQ)*sin(2.*pi*FREQ*t) - GRAV);

}

event adapt (i++){
    adapt_wavelet ({f,u}, (double[]){femax,uemax,uemax},  minlevel = 3, maxlevel = LEVEL);
    //adapt_wavelet ({f}, (double[]){femax}, minlevel = 3, maxlevel = LEVEL);
	//unrefine ( (y>1.5 || x>1.5) && level >= 3 );
}

