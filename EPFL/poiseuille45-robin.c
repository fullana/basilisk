#include "sandbox/tfullana/embed-robin.h"
#include "navier-stokes/centered.h"
// #include "display.h"

#define EPS 1e-14

double lambda = 0.3;

int main() {
  size (2. [0]); // space and time are dimensionless
  DT = HUGE [0];
  
  origin (0, -1.);
  stokes = true;
  TOLERANCE = 1e-5;

  // periodic(left);

  slip = lambda;

  N = 64;
  for (N = 16; N <= 64; N *= 2)
    run();
  // run(); 
}

u.n[embed] = robin(lambda, 0);
u.t[embed] = dirichlet(0);

u.n[left] = neumann(0);
p[left] = dirichlet(y + 0.5);
u.n[right] = neumann(0);
p[right] = dirichlet(y + 0.5);

scalar un[];

event init (t = 0) {
  solid (cs, fs,  intersection(0.5 - y - EPS, 0.5 + y - EPS));

  const face vector g[] = {1.,1.};
  a = g;
  mu = fm;

  foreach()
    un[] = u.x[];
}

event logfile (t += 0.1; i <= 100) {
  double du = change (u.x, un);
  if (i > 0 && du < 1e-6)
    return 1; /* stop */
}

event profile (t = end) {
  printf ("\n");
  foreach()
    printf ("%g %g %g %g %g\n", x, y, u.x[], u.y[], p[]);
  scalar e[];
  foreach()
    e[] = cs[]*(u.x[] - 0.5*(0.25 + lambda - y*y));
  norm n = normf (e);
  fprintf (stderr, "%d %g %g %g\n", N, n.avg, n.rms, n.max);
}