/* Flags */

#define OIL 1
#define WATER 0

#define CONSERVING 0
#define CONTACT 0

#define JVIEW 0
#define LOOP 0
#define SAVEDUMP 1
#define SAVEMOVIE 0

/* Header files */

#include "axi.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#if CONSERVING
#include "navier-stokes/conserving.h"
#endif
#include "tension.h"
#if !LOOP
#include "navier-stokes/perfs.h"
#endif

#if JVIEW
#include "display.h"
#else
#include "view.h"
#endif

/* --------------------------- */
/* Dimensional properties (SI) */ 
/* --------------------------- */ 

/* Oil or Water in fluid 1 */ 
#if OIL
#define rho_1 1.04e3
#define mu_1 20e-3
#define sig 20e-3 
#elif WATER
#define rho_1 997.              
#define mu_1 1.0016e-3   
#define sig 72.86e-3       
#endif

/* Air in fluid 2 */
#define rho_2 1.2047           
#define mu_2 1.8205e-5        

#define grav 9.8067                                 /* Acceleration of gravity */           
#define diameter 1.8e-3                             /* Experimental drop diameter */            
#define u_ref 0.1                                   /* Reference velocity */                     
double amp = 3.7e-4;                                  /* Stage amplitude */        
double freq = 40.0;                                  /* Stage frequency */ 
double initial_pos = 3.970677159e-3;                                 
double initial_vel = -0.000;                           
double initial_width = 1.717084156353438917e-3;
double initial_height = 1.904013939453347071e-3;


#define ellipse(xc, yc, a, b) (sq((x - xc)/(a)) + sq((y - yc)/(b)) - 1.)

#define tend 0.6                                  /* Final time */ 
#define tout 0.001                                  /* Extract data files every */
#define tdump 0.1                                  /* Extract dump files every */
#define tmovie 0.01                                /* Extract movie every */

/* ----------------------------- */
/* Dimensionless properties (SI) */ 
/* ----------------------------- */ 

#define MU_unit (rho_1*u_ref*diameter)            /* Scaling viscosity */
#define SIGMA_unit (rho_1*diameter*u_ref*u_ref)   /* Scaling surface tension */
#define T_unit (diameter/u_ref)                   /* Characteristic time scale */
#define G_unit (diameter/(T_unit*T_unit))         /* Scaling gravity */
#define RHO_1 1.                                  /* Dimensionless density 1 */
#define RHO_2 (rho_2/rho_1)                       /* Dimensionless density 2 */
#define MU_1 (mu_1/MU_unit)                       /* Dimensionless viscosity 1 */
#define MU_2 (mu_2/MU_unit)                       /* Dimensionless viscosity 2 */
#define SIG (sig/SIGMA_unit)                      /* Dimensionless surface tension */
#define GRAV (grav/G_unit)                        /* Dimensionless gravity */
#define DIAMETER 1.                               /* Dimensionless diameter */
#define U 1.                                      /* Dimensionless velocity */
#define AMP (amp/diameter)                        /* Dimensionless stage amplitude */
#define FREQ (freq*T_unit)                        /* Dimensionless stage frequency */
#define TEND (tend/T_unit)                        /* Dimensionless final time */ 
#define TOUT (tout/T_unit)                        /* Dimensionless extraction time data*/
#define TDUMP (tdump/T_unit)                      /* Dimensionless extraction time dump*/
#define TMOVIE (tmovie/T_unit)                    /* Dimensionless extraction time movie*/
#define L0 (4.*DIAMETER)                          /* Dimensionless domain size */
#define INITIAL_POS (initial_pos/diameter)        /* Dimensionless initial position */
#define INITIAL_WIDTH (initial_width/diameter)
#define INITIAL_HEIGHT (initial_height/diameter)
#define INITIAL_VEL (initial_vel/u_ref)           /* Dimensionless initial velocity */


/* ----------------------------- */
/* Stage movement */ 
/* ----------------------------- */ 

#define stage_pos(t) amp*sin(2.*pi*FREQ*(t-0.0025/T_unit))                            /* Stage position (m) */
#define stage_vel(t) 2.*pi*freq*amp*cos(2.*pi*FREQ*(t-0.0025/T_unit))                /* Stage velocity (m/s) */
#define stage_acc(t) -2.*pi*freq*2.*pi*freq*amp*sin(2.*pi*FREQ*(t-0.0025/T_unit))     /* Stage acceleration (m/s2) */

#define STAGE_POS(t) AMP*sin(2.*pi*FREQ*(t-0.0025/T_unit))                            /* Dimensionless stage position */
#define STAGE_VEL(t) 2.*pi*FREQ*AMP*cos(2.*pi*FREQ*(t-0.0025/T_unit))                /* Dimensionless stage velocity */
#define STAGE_ACC(t) -2.*pi*FREQ*2.*pi*FREQ*AMP*sin(2.*pi*FREQ*(t-0.0025/T_unit))     /* Dimensionless stage acceleration */

/* --------------------- */
/* Adaptivity parameters */ 
/* --------------------- */ 

double u_err = 1e-3, f_err = 1e-3;                /* Error thresholds */
int LEVEL = 9;                                    /* Level of refinement */


/* ------------------- */
/* Boundary conditions */ 
/* ------------------- */

// p[top] = dirichlet(0);
// pf[top] = dirichlet(0); 

uf.n[bottom] = 0.;


p[right] = dirichlet(0);
pf[right] = dirichlet(0);

u.t[left] = dirichlet(0);
u.n[left] = dirichlet(0);
#if CONTACT
f[left] = neumann(0);
#else
f[left] = 0.;
#endif


void createMergeScript() {
    const char* script = "#!/bin/bash\n\n"
                         "# Find all unique patterns in the directory\n"
                         "patterns=$(ls | awk -F_ '{ for (i=1; i<NF; i++) printf(\"%s%s\", $i, (i<NF-1) ? \"_\" : \"\"); printf(\"\\n\") }' | grep '_t[^-]*$' | sort | uniq)\n\n"
                         "for pattern in $patterns; do\n"
                         "    output_file=\"${pattern}-ALL\"\n"
                         "    cat \"${pattern}\"_* >> \"$output_file\"\n"
                         "    rm -f \"${pattern}\"_*\n"
                         "done\n";

    // Write the script to the "merge_facets.sh" file
    FILE* file = fopen("merge_facets.sh", "w");
    if (file == NULL) {
        printf("Error: Failed to create the script file.\n");
        return;
    }
    fputs(script, file);
    fclose(file);

    // Make the script file executable
    system("chmod +x merge_facets.sh");
}

int main() {
#if CONSERVING
    TOLERANCE = 1e-4;
#endif
#if SAVEDUMP
    createMergeScript();
#endif
    size (L0);

    init_grid (1 << 6);

    rho1 = RHO_1, mu1 = MU_1;
    rho2 = RHO_2, mu2 = MU_2;
    
    f.sigma = SIG;
#if LOOP
    // for (amp = 3e-4; amp <= 5e-4; amp += 1e-4) {
    //   for (freq = 30.; freq <= 70.; freq += 20.) {
    //     run();
    //   }
    // }
#else
    // amp = 0.;
    // for (initial_vel = -0.000; initial_vel >= -0.015; initial_vel -= 0.005)
    // for (LEVEL = 7; LEVEL <= 9; LEVEL += 1)
    run();
#endif
}

event init (t = 0) {
    char name[360];
    sprintf (name, "properties_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);
    FILE * fp = fopen(name, "w");

    fprintf(fp,"%-12s %-12s %-12s\n", "MU_UNIT", "SIGMA_UNIT", "T_UNIT");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e\n\n", MU_unit, SIGMA_unit, T_unit);
    fprintf(fp,"%-12s %-12s %-12s %-12s %-12s %-12s\n", "RHO_1", "RHO_2", "MU_1", "MU_2", "SIG", "GRAV");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e %-12.2e %-12.2e %-12.2e\n\n", RHO_1, RHO_2, MU_1, MU_2, SIG, GRAV);
    fprintf(fp,"%-12s %-12s\n", "Re", "Re_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", (rho_1*diameter*u_ref)/mu_1, (RHO_1*DIAMETER*U)/MU_1);
    fprintf(fp,"%-12s %-12s\n", "Ca", "Ca_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", mu_1*u_ref/sig, MU_1*U/SIG);
    fprintf(fp,"%-12s %-12s\n", "Bo", "Bo_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", (rho_1-rho_2)*grav*diameter*diameter/sig, (RHO_1-RHO_2)*GRAV*DIAMETER*DIAMETER/SIG);
    fprintf(fp,"%-12s %-12s %-12s\n", "Time (s)", "Time_nodim", "TOUT");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e\n\n", tend, TEND, TOUT);
    fprintf(fp,"%-12s %-12s %-12s %-12s\n", "Amp (m)", "Amp_nodim", "L0", "INITIAL_POS");
    fprintf(fp,"%-12.2e %-12.2e %-12.2e %-12.2e\n\n", amp, AMP, L0, INITIAL_POS);
    fprintf(fp,"%-12s %-12s\n", "Stage vel", "Stage acc");
    fprintf(fp,"%-12.2e %-12.2e\n\n", 2.*pi*FREQ*AMP, 2.*pi*FREQ*AMP*2.*pi*FREQ*AMP);
    fprintf(fp,"%-12s %-12s\n", "Freq (Hz)", "Freq_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", freq, FREQ);
    fprintf(fp,"%-12s %-12s\n", "Grid (m)", "Grid_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", L0*diameter/pow(2,LEVEL), L0*DIAMETER/pow(2,LEVEL));
    fprintf(fp,"%-12s %-12s\n", "Domain (m)", "Domain_nodim");
    fprintf(fp,"%-12.2e %-12.2e\n\n", L0*diameter, L0);
    fprintf(fp,"%-12s %-12s\n", "initial_pos", "initial_vel");
    fprintf(fp,"%-12.2e %-12.2e\n\n", initial_pos, initial_vel);
    fprintf(fp,"%-12s %-12s\n", "INITIAL_POS", "INITIAL_VEL");
    fprintf(fp,"%-12.2e %-12.2e\n\n", INITIAL_POS, INITIAL_VEL);
    fprintf(fp,"%-12s %-12s\n", "INITIAL_STAGE_POS", "INITIAL_STAGE_VEL");
    fprintf(fp,"%-12.2e %-12.2e\n\n", STAGE_POS(0), STAGE_VEL(0));

    fclose(fp);

    // fraction (f, - sq(x - INITIAL_POS) - sq(y) + sq(DIAMETER/2));

    fraction (f, - ellipse(INITIAL_POS, 0., (INITIAL_HEIGHT/2.), (INITIAL_WIDTH/2.)));

    foreach()
      u.x[] = f[]*INITIAL_VEL;

  system("mkdir -p energies");
  system("mkdir -p positions");
  system("mkdir -p velocities");
  system("mkdir -p volumes");
  system("mkdir -p facets");
  system("mkdir -p dumps");
}

event adapt (i++) {
  adapt_wavelet ({f,u}, (double[]){f_err,u_err,u_err,u_err}, LEVEL);
}

event acceleration (i++) {
  face vector av = a;
  foreach_face(x)
    av.x[] -= (GRAV - STAGE_ACC(t));
}

event logfile (t+=TOUT,t<=TEND) {
  char datafile1[360], datafile2[360], datafile3[360], datafile4[360], datafile5[360], datafile6[360], datafile7[360];
  double xc = 0., yc = 0., uc = 0., vc = 0., wd = 0., hd = 0.;
  double vol1 = 0., ke1 = 0., gpe1 = 0., vd1 = 0.; 
  double vol2 = 0., ke2 = 0., gpe2 = 0., vd2 = 0.;

  coord us = {STAGE_VEL(t),0.,0.};
  
  sprintf (datafile1, "./positions/POS_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);
  sprintf (datafile2, "./positions/pos_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);
  sprintf (datafile3, "./velocities/VEL_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);
  sprintf (datafile4, "./velocities/vel_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);
  sprintf (datafile5, "./energies/ENERGY_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);
  sprintf (datafile6, "./energies/energy_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);
  sprintf (datafile7, "./volumes/volume_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);

  static FILE * fp1 = fopen(datafile1, "w");
  static FILE * fp2 = fopen(datafile2, "w");
  static FILE * fp3 = fopen(datafile3, "w");
  static FILE * fp4 = fopen(datafile4, "w");
  static FILE * fp5 = fopen(datafile5, "w");
  static FILE * fp6 = fopen(datafile6, "w");
  static FILE * fp7 = fopen(datafile7, "w");

  if (t==0){
    fprintf(fp1,"%-12s\n", "#data file non_dimensional unit");
    fprintf(fp1,"%-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
      "#1 time", "#2 x_stage", "#3 x_center", "#4 y_center", "#5 width", "#6 height", "#7 air layer height");
    
    fprintf(fp2,"%-12s\n", "#data file dimensional unit (SI)");
    fprintf(fp2,"%-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
      "#1 time", "#2 x_stage", "#3 x_center", "#4 y_center", "#5 width", "#6 height", "#7 air layer height");
  
    fprintf(fp3,"%-12s\n", "#data file non_dimensional unit");
    fprintf(fp3,"%-12s %-12s %-12s %-12s %-12s\n\n",
      "#1 time", "#2 ux_stage", "#3 gx_stage", "#4 ux_center", "#5 uy_center");

    fprintf(fp4,"%-12s\n", "#data file dimensional unit (SI)");
    fprintf(fp4,"%-12s %-12s %-12s %-12s %-12s\n\n",
      "#1 time", "#2 ux_stage", "#3 gx_stage", "#4 ux_center", "#5 uy_center");

    fprintf(fp5,"%-12s\n", "#data file non_dimensional unit");
    fprintf(fp5,"%-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
      "#1 time", "#2 kinetic_1", "#3 kinetic_2", "#4 potential_1", "#5 potential_2", "#6 viscousD_1", "#7 viscousD_2");

    fprintf(fp6,"%-12s\n", "#data file dimensional unit (SI)");
    fprintf(fp6,"%-12s %-12s %-12s %-12s %-12s %-12s %-12s\n\n",
      "#1 time", "#2 kinetic_1", "#3 kinetic_2", "#4 potential_1", "#5 potential_2", "#6 viscousD_1", "#7 viscousD_2");

    fprintf(fp7,"%-12s\n", "#Volume conservation");
    fprintf(fp7,"%-12s %-12s %-12s %-12s %-12s %-12s\n\n",
      "#1 time", "#2 vol1", "#3 vol2", "#4 time_dim", "#5 vol1_dim", "#6 vol2_dim");
  }

  foreach(reduction(+:xc) reduction(+:yc) reduction(+:uc) reduction(+:vc) 
    reduction(+:vol1) reduction(+:ke1) reduction(+:gpe1) reduction(+:vd1)
    reduction(+:vol2) reduction(+:ke2) reduction(+:gpe2) reduction(+:vd2)) {
    double dv1 = f[]*dv();
    double dv2 = (1. - f[])*dv();
    double norm2 = 0., vd = 0.;
    foreach_dimension(){
      norm2 += sq(u.x[]- us.x);
      vd += (sq(u.x[1] - u.x[-1]) +
		    sq(u.x[0,1] - u.x[0,-1]) +
		    sq(u.x[0,0,1] - u.x[0,0,-1]))/sq(2.*Delta);
    }
    uc += (u.x[]- us.x)*dv1;
    vc += u.y[]*dv1;
    xc += x*dv1;
    yc += y*dv1;
    vol1 += dv1;
    ke1 += rho1*norm2*dv1;
    gpe1 += rho1*GRAV*(x + STAGE_POS(0) - STAGE_POS(t))*dv1;
    vd1 += dv1*vd;
    vol2 += dv2;
    ke2 += rho2*norm2*dv2;
    gpe2 += rho2*GRAV*(x + STAGE_POS(0) - STAGE_POS(t))*dv2;
    vd2 += dv2*vd;
  }

  ke1 /= 2.;
  ke2 /= 2.;
  vd1 *= mu1;
  vd2 *= mu2;

  vector h[];
  heights (f, h);

  double x2 = 0., x1 = 0., y1 = 0., xmin = 100.;
  foreach(reduction(max:y1) reduction(min:xmin)){
    if ((h.y[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6)) & (x < (xc/vol1 + Delta)) & (x > (xc/vol1 - Delta))){
      double pos = y + Delta*height(h.y[]);
			if (pos > yc/vol1)
        y1 = pos;
    }
    if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
      double pos = x + Delta*height(h.x[]);
      if (pos < xmin)
        xmin = pos;
    }
  }
  foreach_boundary(bottom, reduction(max:x1) reduction(max:x2)){
    if ((h.x[] != nodata) & (f[]< (1. - 1e-6)) & (f[]> (1e-6))){
      double pos = x + Delta*height(h.x[]);
			if (pos > xc/vol1)
        x1 = pos;
      if (pos < xc/vol1)
        x2 = pos;
    }
  }

  wd = 2.*fabs(y1);
  hd = fabs(x1 - x2);

  fprintf(fp1,"%-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
    t, STAGE_POS(t), ((xc/vol1) + STAGE_POS(0)), yc/vol1, wd, hd, xmin);
  fprintf(fp2,"%-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
    t*T_unit, stage_pos(t), diameter*((xc/vol1) + STAGE_POS(0)), diameter*yc/vol1, diameter*wd, diameter*hd, diameter*xmin);

  fprintf(fp3,"%-12g %-12g %-12g %-12g %-12g\n",
    t, STAGE_VEL(t), STAGE_ACC(t), uc, vc);
  fprintf(fp4,"%-12g %-12g %-12g %-12g %-12g\n",
    t*T_unit, stage_vel(t), stage_acc(t), u_ref*uc, u_ref*vc);

  fprintf(fp5,"%-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
    t, ke1, ke2, gpe1, gpe2, vd1, vd2);
  fprintf(fp6,"%-12g %-12g %-12g %-12g %-12g %-12g %-12g\n",
    t*T_unit, ke1, ke2, gpe1, gpe2, vd1, vd2);

  fprintf(fp7,"%-12g %-12g %-12g %-12g %-12g %-12g\n",
    t, vol1, vol2, t*T_unit, vol1*pow(diameter,3), vol2*pow(diameter,3));

  fflush(fp1);
  fflush(fp2);
  fflush(fp3);
  fflush(fp4);
  fflush(fp5);
  fflush(fp6);
  fflush(fp7);
}

#if SAVEDUMP
event dumpfile (t+=TDUMP) {
  char facetsfile[360], dumpfile[360];

  sprintf (facetsfile, "./facets/facets_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d_t%g_%d", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL, t*T_unit, tid());
  sprintf (dumpfile, "./dumps/dump_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d_t%g", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL, t*T_unit);

  FILE * fp1 = fopen(facetsfile, "w");

  output_facets (f, fp1);

  fclose(fp1);

  dump(dumpfile);
  
  system("cd facets && ../merge_facets.sh");
}

#endif

#if SAVEMOVIE
event moviefile (t+=TMOVIE) {
  char vofmovie[360];

  sprintf (vofmovie, "vof_OIL%d_dia%.2e_pos%.2e_vel%.2e_amp%.2e_freq%.2e_tend%.2e_momentum%d_contact%d_LEVEL%d.mp4", 
      OIL, diameter, initial_pos, fabs(initial_vel), amp, freq, tend, CONSERVING, CONTACT, LEVEL);

  view (quat = {0.000, 0.000, -0.707, 0.707},
      fov = 30, near = 0.01, far = 1000,
      tx = 0.537, ty = -0.452, tz = -2.490,
      width = 1024, height = 1024);
  clear();
  box ();
  draw_vof (c = "f", fc = {0,0,0}, lc = {0,0,0}, lw = 2);
  save (vofmovie);
}
#endif