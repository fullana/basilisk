#include "grid/octree.h"
#include "utils.h"
#include "advection.h"
#include "vof.h"

#include "display.h"

#define L0 1.

scalar f[], cf[];
scalar * interfaces = {f}, * tracers = NULL;

int MAXLEVEL = 6;

int main() {
    N = 1 << MAXLEVEL;
    periodic(left);
    periodic(top);
    // periodic(front);
    origin(-L0/2, -L0/2, -L0/2);

    f.tracers = {cf};

    run();
}

event init (t=0){
    // fraction (f, - sq(x) - sq(y) - sq(z) + sq(0.25));
    // fraction (f, - z + 1e-3*noise());
    fraction (f, - z + 0.01);
    foreach()
        cf[] = f[];
}

event moveit (i++, i <= 1000){
    adapt_wavelet ({f}, (double[]){5e-3}, MAXLEVEL, list = {f, cf});

    double xloc = 0.5*noise();
    double yloc = 0.5*noise();
    foreach_face(){
        if ((x <= (xloc + Delta/2.)) && (x >= (xloc - Delta/2.)) && (y <= (yloc + Delta/2.)) && (y >= (yloc - Delta/2.))){
            if ((f[] < (1. - 1e-6)) && (f[] > (1e-6))){
            fprintf(stdout, "%d %g %g %g %g %g %g\n", i, xloc, yloc, x, y, z, f[]);
                coord n = {0};
                n = interface_normal (point, f);
                foreach_dimension()
                    u.x[] = -1e-3*n.x;
            }
        }
    }
    fflush(stdout);
}