#include "grid/octree.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "navier-stokes/conserving.h"
#include "tension.h"
#include "lambda2.h"
#include "view.h"
#include "navier-stokes/perfs.h"
#include "maxruntime.h"

const double ratio_mu = 1.;
const double RHO = 1.;
const double MU = 6e-3;
const double MU2 = ratio_mu*MU;
const double SIG = 0.43;
const double ratio_fraction = 0.1;
const double TEND = 136.;

face vector av[];

int main (int argc, char * argv[])
{
  maxruntime (&argc, argv);

  L0 = 2.*pi [1];
  foreach_dimension()
    periodic (right);

  rho1 = RHO, mu1 = MU;
  rho2 = RHO, mu2 = MU2;

  f.sigma = SIG;

  a = av;
  N = 32;
  run();
}

event acceleration (i++) {
  double f0 = 1., k = 2.;
  foreach_face(x)
    av.x[] += f0*(sin(k*z) + cos(k*y));
  foreach_face(y)
    av.y[] += f0*(sin(k*x) + cos(k*z));
  foreach_face(z)
    av.z[] += f0*(sin(k*y) + cos(k*x));
}


event logfile (i++; t <= TEND) {
  char datafile1[360];
  sprintf(datafile1, "data_BE2_SP_N%d_TEND%g", N, TEND);
  static FILE *fp = fopen(datafile1, "w");

  coord ubar;
  foreach_dimension() {
    stats s = statsf(u.x);
    ubar.x = s.sum/s.volume;
  }
  
  double ke = 0., vd = 0., vol = 0.;
  foreach(reduction(+:ke) reduction(+:vd) reduction(+:vol)) {
    vol += dv();
    foreach_dimension() {
      // mean fluctuating kinetic energy
      ke += dv()*sq(u.x[] - ubar.x);
      // viscous dissipation
      vd += dv()*(sq(u.x[1] - u.x[-1]) +
		  sq(u.x[0,1] - u.x[0,-1]) +
		  sq(u.x[0,0,1] - u.x[0,0,-1]))/sq(2.*Delta);
    }
  }
  ke /= 2.*vol;
  vd *= MU/vol;

  if (i == 0)
    fprintf (fp, "#t dissipation energy Reynolds\n");
  fprintf (fp, "%g %g %g %g\n",
	   t, vd, ke, 2./3.*ke/MU*sqrt(15.*MU/(vd + 1e-15)));

  fflush(fp);
}


event dumfile (t = TEND) {
  dump("BE2_SP");
}