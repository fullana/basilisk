#define GNBC 0
#define NBC 1
#define MUF_CONST 0 // muf is a constant
#define MUF_U_tanh 1  // muf is a tanh function of U_CL
#define MUF_U_poly 0 //muf is a polynomial function of U_CL
#define MUF_THETA 0 // muf is a tanh function of dynamic angle

//#include "display.h"
#include "axi.h"
#include "navier-stokes/centered.h"
#include "contact.h"
#include "two-phase.h"
// Adding conserving.h might be helpful because of the density ratio
#include "navier-stokes/conserving.h"
#include "tension.h"
#include "reduced.h"

#define rho_l 1000.
#define rho_g 1.2
#define sig 0.072   // surface tension
#define mu_l 1e-3   // water viscosity
#define mu_g 1.7e-5 // air viscosity
#define mu_f 3.
#define gravity 9.81
#define Vd 1.0e-8                                   // drop volume
#define radiusd pow((3. * Vd / (4. * pi)), 1. / 3.) // drop radius
#define radius0 pow((3. * Vd / (pi * (2. + cos(theta_s)) * pow((1. - cos(theta_s)), 2.))), 1. / 3.)

// dimensionless parameters
#define RHO (rho_l / rho_l)
#define RHOg (rho_g / rho_l)
#define SIGMA (sig / sig)
#define MU (mu_l / sqrt(rho_l * sig * radiusd))
#define MUg (mu_g / sqrt(rho_l * sig * radiusd))
#define MUf (mu_f / sqrt(rho_l * sig * radiusd))
#define Rd (radiusd / radiusd) // radius0 ~ Rd
#define R0 (radius0 / radiusd)
#define GRAV (gravity / (sig / (rho_l * sq(radiusd))))

#define theta_s (105. * pi / 180.)
#define theta_a (115. * pi / 180.)
#define theta_r (95. * pi / 180.)
#define CAH (10. * pi / 180.)
#define theta_ini (109. * pi / 180.)

double theta_d = 109. * pi / 180.;

///////////oscillation setting////////////
#define freq 70.   // freq: actual frequency
#define amp 0.0002 // amplitute
// non-dimensionalization
#define FREQ (freq / sqrt(sig / (rho_l * cube(radiusd))))
#define AMP (amp / radiusd)

#define t_end (10. / FREQ)

// for mesh refinement
double femax = 0.001;
double uemax = 0.001;
double L = 1.e-06 / radiusd;      // macroscale length
double lambda2 = 1.e-9 / radiusd; // microscale length

//////////////////DCA models//////////////////
double muf_tmp;

#if MUF_U_tanh
double uDelta = 0.5;
double MufU_tanh(double x, double y){
    //x: muf reference value, y: U_cl
    return (x * (5. -  4. * tanh(fabs(y) / (uDelta))));
}
#elif MUF_U_poly
double polyN = -0.8;
double MufU_poly(double x, double y, double z){
    //x:muf reference value, y:contact line velocity ,z: polyN
    return (x * pow(fabs(y), z));
}
#elif MUF_THETA
double mufHigh = 10. / sqrt(rho_l * sig * radiusd); // high value in the sticking region
double mufLow = 0.5 / sqrt(rho_l * sig * radiusd);  // low value outside the sticking region

double DeltaTheta = 1. * pi / 180.;
// variables: x: theta_d
double H(double x)
{
    return (1. + 0.5 * (tanh((x - theta_a) / DeltaTheta) - tanh((x - theta_r) / DeltaTheta)));
}
// variables: x: H(x)
double MufTheta(double x)
{
    return (mufHigh*(1. - H(x)) + mufLow*H(x));
}
#endif

#if GNBC
// b:eps
double bell(double y, double b)
{
    // return ((1. - pow(tanh(y/b), 2))/b);
    return (fabs(y) < b ? pow(0.5 * (1. + cos(pi * y / b)), 2.) : 0.);
}
double Young_stress(double y, double eps, double theta, double theta_eq)
{
    return bell(y, eps) * (cos(theta_eq) - cos(theta));
}

scalar gnbc[];
#endif

double eps = 0.01; //Cahn number if compare with PF simulation

// Definition of the Navier boundary condition here
// a = 1, b = slip, c = wall speed (or gnbc[] if you are using that relation)
#define navier(a,b,c) ((dirichlet ((c)*Delta/(2*(b) + (a)*Delta))) + ((neumann (0))*((2*(b) - (a)*Delta)/(2*(b) + (a)*Delta) + 1.)))

/////////////////////////////////////////////
vector h[];
h.t[left] = contact_angle(theta_d);
#if GNBC
u.t[left] = navier(1., eps, gnbc[]);
#elif NBC
u.t[left] = navier(1., eps, 0.);
#endif

u.n[left] = dirichlet(0.);
u.n[right] = dirichlet(0.);

int LEVEL = 8;

int main()
{
    // Tolerance of the Poisson solver 
    TOLERANCE = 1e-5;

    N = 64;
    L0 = 4.;
    origin(0., 0.);

    f.sigma = SIGMA;
    f.height = h;

    G.x = -GRAV;

    // This is not needed now
    // slip = eps;

    rho1 = RHO;
    rho2 = RHOg;
    mu1 = MU;
    mu2 = MUg;
    for (LEVEL = 6; LEVEL <= 10; LEVEL += 1)
    {
        run();
    }
}

event init(t = 0)
{
    if (!restore(file = "restart"))
    {
        refine(level < LEVEL);
        fraction(f, -(sq((x + R0 * cos(theta_s))) + sq(y) - sq(R0)));
    }
    boundary({f});
    char name[80];
    sprintf(name, "init_snap-%d", LEVEL);
    dump(name);
}

event acceleration (i++) 
{
    G.x = (0. - AMP*sq(2.*pi*FREQ)*sin(2.*pi*FREQ*t) - GRAV);
}

event adapt (i++)
{
    adapt_wavelet ({f,u}, (double[]){femax,uemax,uemax},  minlevel = 3, maxlevel = LEVEL);
}

event impose(i++; t <= t_end)
{
    char name[80];
    sprintf(name, "Info-%d", LEVEL);
    static FILE *fp = fopen(name, "w");
    double KE = 0., VD = 0., area = 0., vol = 0., xd = 0., radius = 0., vel0 = 0.,  Ca = 0., We = 0., topx = 0., Utop = 0.;

    scalar px[], py[], pcl[];
    position(f, px, {1, 0});
    position(f, py, {0, 1});
    position(f, pcl, {0, 1});

    foreach (reduction(+: area) reduction(+: vol) reduction(+: KE) reduction(+: VD) reduction(+: xd))
    {
        if (f[] <= 1e-6 || f[] >= 1. - 1e-6)
        {
            px[] = nodata;
            py[] = nodata;
        }

        if (f[] <= 1e-6 || f[] >= 1. - 1e-6 || x > L / pow(2, LEVEL))
        {
            pcl[] = nodata;
        }

        if (f[] > 1e-6 && f[] < 1. - 1e-6)
        {
            // interfacial area
            coord n = interface_normal(point, f), p;
            double alpha1 = plane_alpha(f[], n);
            area += pow(Delta, 1.) * plane_area_center(n, alpha1, &p) * 2. * pi * py[];
        }
        double dv_axi = pow(Delta, 2.) * 2. * pi * y;

        if (f[] > 1e-6)
        {
            // volume
            vol += dv_axi * f[];
            // centroid
            xd += dv_axi * f[] * x;
            foreach_dimension()
            {
                KE += dv_axi * sq(u.x[]); // kinetic energy
                // viscous dissipation
                VD += dv() * (sq(u.x[1] - u.x[-1]) + sq(u.x[0, 1] - u.x[0, -1])) / sq(2. * Delta);
            }
        }
    }
    KE /= 2.;
    xd /= vol;
    VD *= MU / vol;

    double angle_extracted = 0., angle_above = 0., angle_extrapolated = 0.;
    double curv1 = 0., curv2 = 0.; //curv1: contact line cell curvature; curv2: interface cell on the 2nd layer above the wall 
    scalar kappa[];
    curvature(f, kappa);

    foreach (reduction(max: angle_extrapolated) reduction(max: angle_above) reduction(max: curv2))
    {
        if ((x <= (2. * Delta)) & (x > (Delta)) & (h.y[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > 1e-6))
        {
            coord n = {0};
            n = interface_normal(point, f);
            curv2 = kappa[];
            angle_above = atan2(n.y, n.x);
            double tmp = pow(((h.y[1, 0] - h.y[]) / 1.0), 2);
            angle_extrapolated = angle_above + 1.5 * Delta * kappa[] * sqrt(1 + tmp) / sin(angle_above);
        }
    }

    foreach_boundary(left, reduction(+: radius) reduction(+: vel0) reduction(+: Ca) reduction(+: We) reduction(+: angle_extracted) reduction(max: curv1))
    {
        if ((y > 0.) & (h.y[] != nodata) & (f[] < (1. - 1e-6)) & (f[] > (1e-6)))
        {
            radius = y + Delta * height(h.y[]);
            vel0 = u.y[];
            Ca = MU * vel0 / SIGMA;
            We = (RHO * (pow(vel0, 2)) * 1.) / SIGMA;
            coord n = {0};
            n = interface_normal(point, f);
            curv1 = kappa[];
            angle_extracted = atan2(n.y, n.x);
        }
    }

    foreach_boundary(bottom, reduction(max: topx))
    {
        if ((f[] < (1. - 1e-3)) & (f[] > (1e-3)))
        {
            topx = x + Delta * height(h.x[]);
        }
    }

    #if MUF_CONST
    muf_tmp = MUf;
    #elif MUF_U_tanh
    muf_tmp = MufU_tanh(MUf, vel0);
    #elif MUF_U_poly
    muf_tmp = MufU_poly(MUf, vel0, polyN);
    #elif MUF_THETA
    muf_tmp = MufTheta(angle_extracted);
    #endif

    theta_d = theta_s + (muf_tmp * vel0 / SIGMA) * pi / 180.;

    // I think you can't use a dynamic angle relation theta = f(Ca) in combination with a gnbc type relation Ca = f(theta)
    // You need to choose one of the two

    // foreach_boundary(left)
    // {
    //     double relative_position = y - radius;

    //     gnbc[] = eps * (SIGMA / muf_tmp) * Young_stress(relative_position, eps, angle_extracted, theta_s);
    // }

    fprintf(fp, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", t, dt, radius, statsf(py).max, statsf(px).max, statsf(f).sum, Ca, vel0, We, theta_d / pi * 180., 180. * angle_extrapolated / pi, 180. * angle_extracted / pi, xd, topx, Utop, vol, KE, VD, area, curv1, curv2);
    fflush(fp);
}
